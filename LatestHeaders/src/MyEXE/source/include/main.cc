#include "header.hh"

extern "C" {

void NewSimpleAnalyzeCSV(int const argc, char const **argv) {
    if (argc < 2) {
        printf("Not using correctly...\n");
    } else {
        std::string const infilename = argv[1];
        std::string const outfile    = infilename + ".out";

        CPPFileIO::AnalyzeSlave::PrepareFileSchema<',', '\n'>(infilename,
                                                              outfile, 64, 64);
    }
}

void NewSimpleAnalyzeTSV(int const argc, char const **argv) {
    if (argc < 2) {
        printf("Not using correctly...\n");
    } else {
        std::string const infilename = argv[1];
        std::string const outfile    = infilename + ".out";

        CPPFileIO::AnalyzeSlave::PrepareFileSchema<'\t', '\n'>(infilename,
                                                               outfile, 64, 64);
    }
}

void clang_cc(int const argc, char const **argv) {
    std::vector<std::string> args;
    args.push_back("/usr/bin/clang");
    for (int i = 1; i < argc; i++) { args.push_back(argv[i]); }
    args.push_back("-Ofast");
    args.push_back("-mtune=native");
    args.push_back("-march=native");
    CPPFileIO::starter_self(args);
}

void clang_cxx(int const argc, char const **argv) {
    std::vector<std::string> args;
    args.push_back("/usr/bin/clang++");
    for (int i = 1; i < argc; i++) { args.push_back(argv[i]); }
    // args.push_back("-std=c++17");
    args.push_back("-Ofast");
    args.push_back("-mtune=native");
    args.push_back("-march=native");
    CPPFileIO::starter_self(args);
}

void clang_omp_cc(int const argc, char const **argv) {
    std::vector<std::string> args;
    args.push_back("/usr/bin/clang");
    for (int i = 1; i < argc; i++) { args.push_back(argv[i]); }
    args.push_back("-Ofast");
    args.push_back("-mtune=native");
    args.push_back("-march=native");
    args.push_back("-fopenmp");
    args.push_back("-lomp5");
    CPPFileIO::starter_self(args);
}

void clang_omp_cxx(int const argc, char const **argv) {
    std::vector<std::string> args;
    args.push_back("/usr/bin/clang++");
    for (int i = 1; i < argc; i++) { args.push_back(argv[i]); }
    args.push_back("-std=c++17");
    args.push_back("-Ofast");
    args.push_back("-mtune=native");
    args.push_back("-march=native");
    args.push_back("-fopenmp");
    args.push_back("-lomp5");
    CPPFileIO::starter_self(args);
}

void csv_2_null(int const argc, char const **argv) {
    switch (argc) {
        case 0:
        case 1:
            printf("Need atleast 1 argument...\n");
            printf("\tinput file\n");
            printf("\toutput file\n");
            break;

        case 2:
            CPPFileIO::csv_2_null(argv[1], std::string(argv[1]) + ".tsv");
            break;

        default: CPPFileIO::csv_2_null(argv[1], argv[2]); break;
    }
}

void csv_2_tsv(int const argc, char const **argv) {
    switch (argc) {
        case 0:
        case 1:
            printf("Need atleast 1 argument...\n");
            printf("\tinput file\n");
            printf("\toutput file\n");
            break;
        case 2:
            CPPFileIO::csv_2_tsv(argv[1], std::string(argv[1]) + ".tsv");
            break;
        default: CPPFileIO::csv_2_tsv(argv[1], argv[2]); break;
    }
}

void emc(int const argc, char const **argv) {
    if (true) {
        CPPFileIO::find_executable finder;
        std::vector<std::string>   args;
        args.push_back(finder("bin/emacsclient"));
        for (int i = 1; i < argc; i++) { args.push_back(argv[i]); }
        CPPFileIO::ForkMe forker;
        if (forker.InKid()) { CPPFileIO::starter_self(args); }
    }
}

void emd(int const argc, char const **argv) {
    CPPFileIO::find_executable finder;
    CPPFileIO::ExternalStarter<false>::GET(finder("bin/emacsclient"))(
      "--daemon");
}

void emg(int const argc, char const **argv) {
    if (true) {
        CPPFileIO::find_executable finder;
        std::vector<std::string>   args;
        args.push_back(finder("bin/emacsclient"));
        args.push_back("-c");
        for (int i = 1; i < argc; i++) { args.push_back(argv[i]); }
        CPPFileIO::ForkMe forker;
        if (forker.InKid()) { CPPFileIO::starter_self(args); }
    }
}

void fcc(int const argc, char const **argv) {
    CPPFileIO::find_executable finder;
    std::vector<std::string>   args;
    args.push_back(finder("bin/clang-format"));
    args.push_back("-style=file");
    for (int i = 1; i < argc; i++) { args.push_back(argv[i]); }
    CPPFileIO::starter_self(args);
}

void gcc_cc(int const argc, char const **argv) {
    std::vector<std::string> args;
    args.push_back("/usr/bin/gcc");
    for (int i = 1; i < argc; i++) { args.push_back(argv[i]); }
    args.push_back("-Ofast");
    args.push_back("-mtune=native");
    args.push_back("-march=native");
    CPPFileIO::starter_self(args);
}

void gcc_cxx(int const argc, char const **argv) {
    std::vector<std::string> args;
    args.push_back("/usr/bin/g++");
    for (int i = 1; i < argc; i++) { args.push_back(argv[i]); }
    args.push_back("-std=c++17");
    args.push_back("-Ofast");
    args.push_back("-mtune=native");
    args.push_back("-march=native");
    CPPFileIO::starter_self(args);
}

void gcc_omp_cc(int const argc, char const **argv) {
    std::vector<std::string> args;
    args.push_back("/usr/bin/gcc");
    for (int i = 1; i < argc; i++) { args.push_back(argv[i]); }
    args.push_back("-Ofast");
    args.push_back("-mtune=native");
    args.push_back("-march=native");
    args.push_back("-fopenmp");
    CPPFileIO::starter_self(args);
}

void gcc_omp_cxx(int const argc, char const **argv) {
    std::vector<std::string> args;
    args.push_back("/usr/bin/g++");
    for (int i = 1; i < argc; i++) { args.push_back(argv[i]); }
    args.push_back("-std=c++17");
    args.push_back("-Ofast");
    args.push_back("-mtune=native");
    args.push_back("-march=native");
    args.push_back("-fopenmp");
    CPPFileIO::starter_self(args);
}

void generate_class(int const argc, char const **argv) {
    std::string       filename = argv[1];
    std::string const name = CPPFileIO::generate_header_guard_name(filename);
    CPPFileIO::basename(filename, '/');
    CPPFileIO::MyStr<int> tmp(filename);
    auto                  res = tmp('.');
    filename                  = res[0].start();
    printf("#ifndef %s\n", name.c_str());
    printf("#define %s\n", name.c_str());
    printf("#define _MACRO_CLASS_NAME_ %s\n", filename.c_str());
    printf("class _MACRO_CLASS_NAME_ {\n");
    printf("  public:\n");
    printf("    using TYPE_SELF = _MACRO_CLASS_NAME_;\n");
    printf("\n");
    printf("  public:\n");
    printf("    _MACRO_CLASS_NAME_(){}\n");
    printf("    ~_MACRO_CLASS_NAME_(){}\n");
    printf("\n");
    printf("};\n");
    printf("#undef _MACRO_CLASS_NAME_\n");
    printf("#endif\n");
}

void generate_header_guard(int const argc, char const **argv) {
    std::string       filename = argv[1];
    std::string const name = CPPFileIO::generate_header_guard_name(filename);
    printf("#ifndef %s\n", name.c_str());
    printf("#define %s\n", name.c_str());
    printf("#endif\n");
}

void make_searcher(int const argc, char const **argv) {
    if (argc > 2) {
        CPPFileIO::make_searcher(argv[1], argv[2]);
    } else if (argc > 1) {
        CPPFileIO::make_searcher(argv[1]);
    } else {
        printf("Need atleast 1 command line argument...\n");
        printf("    1: The string to search for.\n");
        printf("OR\n");
        printf("    1: The output filename\n");
        printf("    2: The string to search for.\n");
    }
}

void mysync(int const argc, char const **argv) {
    if (true) {
        std::vector<std::string> args;
        args.push_back("/usr/bin/rsync");
        args.push_back("-avh");
        args.push_back("--progress");
        for (int i = 1; i < argc; i++) { args.push_back(argv[i]); }
        CPPFileIO::ForkMe forker;
        if (forker.InKid()) { CPPFileIO::starter_self(args); }
    }
    { CPPFileIO::ExternalStarter<false>::GET("/bin/sync"); }
}

void parallel_run_file_lines(int const c, char const **v) {
    switch (c) {
        case 0:
        case 1:
        case 2:
            if (true) {
                printf("Invalid invocation...\n");
                printf("    (self) (name_file_cmd) (nth)\n");
            }
            break;

        default:
            if (true) {
                std::string const name_file_cmd = v[1];
                size_t            nth;
                sscanf(v[2], "%zu", &nth);
                CPPFileIO::parallel_run_file_lines(
                  /*const std::string name_file_cmd =*/name_file_cmd,
                  /*const size_t nth =*/nth);
            }
            break;
    }
}
}
