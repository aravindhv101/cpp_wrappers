#ifndef _HEADER_GUARD_main_
#define _HEADER_GUARD_main_

extern "C" {
void NewSimpleAnalyzeCSV(int const, char const **);
void NewSimpleAnalyzeTSV(int const, char const **);
void clang_cc(int const, char const **);
void clang_cxx(int const, char const **);
void clang_omp_cc(int const, char const **);
void clang_omp_cxx(int const, char const **);
void csv_2_null(int const, char const **);
void csv_2_tsv(int const, char const **);
void emc(int const, char const **);
void emd(int const, char const **);
void emg(int const, char const **);
void fcc(int const, char const **);
void gcc_cc(int const, char const **);
void gcc_cxx(int const, char const **);
void gcc_omp_cc(int const, char const **);
void gcc_omp_cxx(int const, char const **);
void generate_class(int const, char const **);
void generate_header_guard(int const, char const **);
void make_searcher(int const, char const **);
void mysync(int const, char const **);
void parallel_run_file_lines(int const, char const **);
}

#endif
