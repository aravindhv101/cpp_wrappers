#ifndef _HEADER_GUARD_LLVM_source_functions_hh_
#define _HEADER_GUARD_LLVM_source_functions_hh_

#include "./LatestHeaders/include/CPPFileIO.hh"

inline void download(std::string const url, std::string const destination) {
    CPPFileIO::find_executable finder;
    CPPFileIO::ExternalStarter<true>::GET(
      /*const std::string in =*/finder.find_exe("wget"))("-c")(url)("-O")(
      destination);
}

inline void mkdir_p(std::string in) {
    using type_string  = CPPFileIO::fast_string<4>;
    auto str           = type_string::view_string(in);
    using type_strings = type_string::TYPE_SELVES;
    type_strings cuts;
    str.split(cuts, '/', true);

    std::string res(reinterpret_cast<char *>(cuts[0].get_start()));
    mkdir(res.c_str(), 0755);
    for (size_t i = 1; i < cuts.size() - 1; i++) {

        res = res + std::string("/") +
              std::string(reinterpret_cast<char *>(cuts[i].get_start()));

        mkdir(res.c_str(), 0755);
    }
}

#endif
