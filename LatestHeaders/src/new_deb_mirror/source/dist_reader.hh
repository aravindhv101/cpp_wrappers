#ifndef _HEADER_GUARD_LLVM_source_dist_reader_hh_
#define _HEADER_GUARD_LLVM_source_dist_reader_hh_

#include "./LatestHeaders/include/CPPFileIO.hh"

#define _MACRO_CLASS_NAME_ dist_reader

template <TEMPLATES::TYPE::uint1 INT_SIZE =
            sizeof(TEMPLATES::TYPE::uint_biggest)>

class _MACRO_CLASS_NAME_ {

  public:
    using TYPE_INT        = TEMPLATES::TYPE::type_int<INT_SIZE>;
    using TYPE_UINT       = TEMPLATES::TYPE::type_uint<INT_SIZE>;
    using TYPE_FILE_LINES = CPPFileIO::file_lines<INT_SIZE>;
    using TYPE_SELF       = _MACRO_CLASS_NAME_<INT_SIZE>;
    using TYPE_STRING     = typename TYPE_FILE_LINES::TYPE_STRING;
    using TYPE_STRINGS    = typename TYPE_STRING::TYPE_SELVES;
    using TYPE_BUFFER     = typename TYPE_STRING::TYPE_BUFFER;
    using ATOMIC_UINT     = CPPFileIO::Atomic_Counter<TYPE_UINT>;
    using ATOMIC_INT      = CPPFileIO::Atomic_Counter<TYPE_INT>;

  public:
    static inline void mkdir_p(TYPE_STRING in) {
        if (in.get_size() > 0) {
            TYPE_STRINGS cuts;
            in.split(cuts, '/', true);
            std::string res(
              reinterpret_cast<char const *>(cuts[0].get_start()));
            mkdir(res.c_str(), 0755);
            for (size_t i = 1; i < cuts.size() - 1; i++) {
                res = res + std::string("/") +
                      std::string(
                        reinterpret_cast<char const *>(cuts[i].get_start()));
                mkdir(res.c_str(), 0755);
            }
        }
    }

    static inline void download(TYPE_STRING const &url,
                                TYPE_STRING const &destination) {

        printf("Start download %s %s\n", url.get_start(),
               destination.get_start());
        if ((url.get_size() > 0) && (destination.get_size() > 0)) {
            CPPFileIO::find_executable finder;
            CPPFileIO::ExternalStarter<true>::GET(
              /*const std::string in =*/finder.find_exe("wget"))("-c")(
              reinterpret_cast<char const *>(url.get_start()))("-O")(
              reinterpret_cast<char const *>(destination.get_start()));
        }
        printf("done download\n");
    }

  public:
    TYPE_FILE_LINES base;
    TYPE_FILE_LINES files;
    ATOMIC_UINT     counter;

  public:
    inline TYPE_UINT size_base() const { return base(); }
    inline TYPE_UINT size_file() const { return files(); }

  public:
    inline TYPE_STRING get_base(TYPE_UINT const index) const {
        return base(index);
    }

    inline TYPE_STRING get_file(TYPE_UINT const index) const {
        return files(index);
    }

    inline TYPE_STRING get_url(TYPE_UINT const index_base,
                               TYPE_UINT const index_file) const {

        printf("get_url 1\n");

        TYPE_BUFFER url = TYPE_BUFFER::get_new(base(index_base).get_size() + 1 +
                                               files(index_file).get_size());

        if (base(index_base).get_size() > 0) {
            memcpy(
              /*void *dest =*/reinterpret_cast<void *>(url.get_start()),
              /*const void *src =*/
              reinterpret_cast<const void *>(base(index_base).get_start()),
              /*size_t n =*/size_t(base(index_base).get_size()));
        }

        url(base(index_base).get_size()) = '/';

        if (files(index_file).get_size() > 0) {
            memcpy(
              /*void *dest =*/reinterpret_cast<void *>(
                url.get_stop() - files(index_file).get_size()),
              /*const void *src =*/
              reinterpret_cast<const void *>(files(index_file).get_start()),
              /*size_t n =*/size_t(files(index_file).get_size()));
        }

        TYPE_STRING ret = TYPE_STRING::from_buffer(TYPE_BUFFER::own_this(url));

        return ret;
    }

    inline void mkdir_i(TYPE_UINT const in) const {
        mkdir_p(/*TYPE_STRING in =*/TYPE_STRING::copy_this(files(in)));
    }

    inline void mkdir_i() const {
        for (TYPE_UINT i = 0; i < files(); i++) { mkdir_i(i); }
    }

    inline void download_i(TYPE_UINT const in) const {
        printf("downloading %zu\n", in);
        auto url = get_url(0, in);
        download(url, files(in));
        printf(" done downloading %zu\n", in);
    }

    inline void download_i() {
        counter = 0;
        for (TYPE_UINT i = (counter++); i < files() - 1; i = (counter++)) {
            download_i(i);
        }
    }

    inline void operator()() { download_i(); }

  public:
    _MACRO_CLASS_NAME_()
      : base("list.url_mirrors.txt"), files("list.dist_packages.txt") {}

    ~_MACRO_CLASS_NAME_() {}

  public:
    static inline void mkdir_all() {
        TYPE_SELF slave;
        slave.mkdir_i();
    }

    static inline void download_dists(TYPE_UINT const nth) {
        TYPE_SELF slave;
        slave.mkdir_i();
        std::thread *threads = new std::thread[nth];
        for (TYPE_UINT i = 0; i < nth; i++) {
            threads[i] =
              std::thread(CPPFileIO::thread_slave<TYPE_SELF>, &slave);
        }
        for (size_t i = 0; i < nth; i++) { threads[i].join(); }
        delete[] threads;
    }
};

inline void mkdir_all() { _MACRO_CLASS_NAME_<>::mkdir_all(); }

inline void download_dists(TEMPLATES::TYPE::uint4 const nth) {
    _MACRO_CLASS_NAME_<>::download_dists(nth);
}

#undef _MACRO_CLASS_NAME_

#endif
