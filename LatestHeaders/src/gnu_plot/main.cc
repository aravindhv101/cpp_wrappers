#include "./LatestHeaders/include/CPPFileIO.hh"

#define _MACRO_CLASS_NAME_ gnuplot_wrapper
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;

  private:
    static inline std::string INTERPRETOR_LINE() {
        return "#!/usr/bin/gnuplot\n";
    }

    static inline std::string TERMINAL_TYPE(size_t const size_x = 15,
                                            size_t const size_y = 10) {

        char tmp[512];

        sprintf(tmp,
                "set terminal tikz latex color size %zucm, %zucm scale 1.5,1.5 "
                "fulldoc createstyle\n",
                size_x, size_y);

        return tmp;
    }

    static inline std::string GRID() { return "set grid\n"; }
    static inline std::string LOG_X() { return "set log x\n"; }
    static inline std::string LOG_Y() { return "set log y\n"; }

    static inline std::string OUTPUT(std::string const filename) {
        char tmp[512];
        sprintf(tmp, "set output \"%s\"\n", filename.c_str());
        return tmp;
    }

    static inline std::string PLOT(std::vector<std::string> &filenames) {
        char tmp[512];
        sprintf(tmp, "plot \"out.hist.txt\" with lines notitle\n");
        return tmp;
    }

    static inline std::string EXIT() { return "exit\n"; }

  public:
    _MACRO_CLASS_NAME_(std::string const name_file_script) {}

    ~_MACRO_CLASS_NAME_() {}
};
#undef _MACRO_CLASS_NAME_

int main() { return 0; }
