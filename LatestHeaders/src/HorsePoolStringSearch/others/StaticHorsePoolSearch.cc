#include "./include/CPPFileIO.hh"
#include "./word.hh"

int main() {
    using TYPE_READER = CPPFileIO::FileArray<char>;
    using TYPE_FINDER = CPPFileIO::StaticHorsePoolSearch<TYPE_READER, word_destroy>;
    TYPE_READER reader("./file");
    TYPE_FINDER finder(reader);
    auto        res = finder(0);
    printf("%zu %zu\n", res, reader());
}
