#include "./include/CPPFileIO.hh"

#define _MACRO_CLASS_NAME_ strfind
template <typename T, typename TI = CPPFileIO::TYPE_U64>
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF   = _MACRO_CLASS_NAME_;
    using TYPE_READER = T;
    using TYPE_INT    = TI;
    using CHAR        = CPPFileIO::TYPE_BYTE;

  public:
    static inline TYPE_INT constexpr SIZE() { return 7; }

    static inline TYPE_INT constexpr FAILED_MATCH_SKIP() { return SIZE()-1; }

    static inline CHAR constexpr GET_CHAR(size_t const i) {
        switch (i) {
            case 6: return 'd';
            case 5: return 'e';
            case 4: return 's';
            case 3: return 't';
            case 2: return 'r';
            case 1: return 'o';
            case 0: return 'y';
            default: return 0;
        }
    }

    static inline TYPE_INT constexpr GET_JUMP(CHAR const in) {
        switch (in) {
            case 'd': return 6;
            case 'e': return 5;
            case 's': return 4;
            case 't': return 3;
            case 'r': return 2;
            case 'o': return 1;
            case 'y': return 0;
            default: return SIZE();
        }
    }

  private:
    TYPE_READER &  READER;
    TYPE_INT const BEGIN;
    TYPE_INT const END;
    TYPE_INT       CURRENT;

  private:
    inline bool MATCH() {
        TYPE_INT i = 0;
        TYPE_INT index;

        if ((CURRENT < BEGIN) || (END <= CURRENT)) { return false; }

    LoopStart:

        if (i == SIZE()) { return true; }

        index = CURRENT - i;

        if ((BEGIN <= index) && (index < END) &&
            (READER(index) == GET_CHAR(i))) {
            i++;
            goto LoopStart;
        }

        return false;
    }

    inline TYPE_INT NEXT() {
        CHAR     element;
        TYPE_INT jump;
        while ((BEGIN <= CURRENT) && (CURRENT < END)) {
            element = READER(CURRENT);
            jump    = GET_JUMP(element);
            if (jump != 0) {
                CURRENT += jump;
            } else {
                if (MATCH()) {
                    return CURRENT;
                } else {
                    CURRENT += FAILED_MATCH_SKIP();
                }
            }
        }
        return END;
    }

  public:
    inline TYPE_INT operator()(TYPE_INT const in) {
        CURRENT = in;
        return NEXT();
    }

  public:
    _MACRO_CLASS_NAME_(TYPE_READER &reader, TYPE_INT const begin,
                       TYPE_INT const end)
      : READER(reader), BEGIN(begin), END(end), CURRENT(BEGIN) {}

    _MACRO_CLASS_NAME_(TYPE_READER &reader)
      : READER(reader), BEGIN(0), END(READER()), CURRENT(BEGIN) {}

    _MACRO_CLASS_NAME_(TYPE_READER &reader, TYPE_INT const end)
      : READER(reader), BEGIN(0), END(end), CURRENT(BEGIN) {}

    ~_MACRO_CLASS_NAME_() {}
};
#undef _MACRO_CLASS_NAME_

int main() {
    using TYPE_READER = CPPFileIO::FileArray<char>;
    using TYPE_FINDER = strfind<TYPE_READER, size_t>;
    TYPE_READER reader("./file");
    TYPE_FINDER finder(reader);
    auto res = finder(0);
    printf("%zu %zu\n",res,reader());
}
