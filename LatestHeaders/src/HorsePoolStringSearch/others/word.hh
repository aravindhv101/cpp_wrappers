
#define _MACRO_CLASS_NAME_ word_destroy
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    using TYPE_INT  = size_t;
    using CHAR      = CPPFileIO::TYPE_BYTE;

  public:
    static inline TYPE_INT constexpr SIZE() { return 7; }

    static inline CHAR constexpr GET_CHAR(size_t const i) {
        switch (i) {
            case 6: return 'd';
            case 5: return 'e';
            case 4: return 's';
            case 3: return 't';
            case 2: return 'r';
            case 1: return 'o';
            case 0: return 'y';
            default: return 0;
        }
    }

    static inline TYPE_INT constexpr GET_JUMP(CHAR const in) {
        switch (in) {
            case 'd': return 6;
            case 'e': return 5;
            case 's': return 4;
            case 't': return 3;
            case 'r': return 2;
            case 'o': return 1;
            case 'y': return 0;
            default: return SIZE();
        }
    }
};
#undef _MACRO_CLASS_NAME_
