#include "./include/CPPFileIO.hh"

int main(int const argc, char const **argv) {
    if (argc > 2) {
        CPPFileIO::make_searcher(argv[1], argv[2]);
    } else if (argc > 1) {
        CPPFileIO::make_searcher(argv[1]);
    } else {
	    printf("Need atleast 1 command line argument...\n");
	    printf("    1: The string to search for.\n");
	    printf("OR\n");
	    printf("    1: The output filename\n");
	    printf("    2: The string to search for.\n");
    }
}
