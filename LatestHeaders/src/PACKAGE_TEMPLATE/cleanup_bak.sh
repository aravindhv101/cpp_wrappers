#!/bin/bash
cd "$('dirname' '--' "${0}")"
'find' './' '-type' 'f' | 'grep' '~$\|#$' | 'sed' 's@^@"rm" "-vf" "--" "@g;s@$@"@g' | bash
'rm' '-rf' '--' './bin'
exit '0'
