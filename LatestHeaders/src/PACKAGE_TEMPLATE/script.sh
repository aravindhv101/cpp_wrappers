#!/bin/bash
tail -n +4 "${0}" | 'sed' "s/##NAME##/${1}/g"
exit '0'

* For "##NAME##"
#+BEGIN_SRC c++ :tangle ./source/##NAME##.cc
  #include "./include/main.hh"
  int main(int const argc, char const **argv) {
      ##NAME##(argc, argv);
      return 0;
  }
#+END_SRC

#+BEGIN_SRC makefile :tangle ./Makefile
  ./bin/##NAME##: ./lib/main.o ./source/##NAME##.cc
	  mkdir -pv bin
	  g++ -fopenmp -Ofast -mtune=native -march=native ./source/##NAME##.cc ./lib/main.o -o ./bin/##NAME##
#+END_SRC

