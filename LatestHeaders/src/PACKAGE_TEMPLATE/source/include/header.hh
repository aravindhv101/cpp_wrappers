#ifndef _HEADER_GUARD_header_hh_
#define _HEADER_GUARD_header_hh_

#include "./include/CPPFileIO.hh"
#include "./include/Read_Show_Functions.hh"
#include "./include/Spherical_Geometry_Clustering.hh"
#include "./include/StaticArray.hh"

using TYPE_BYTE = CPPFileIO::TYPE_BYTE;
using TYPE_INT = CPPFileIO::TYPE_I64;
using TYPE_FLOAT = double;
using ATOMIC_INT = CPPFileIO::Atomic_Counter<TYPE_INT>;

using TYPE_BOUNDARY = TYPE_INT;
using TYPE_BOUNDARY_READER = CPPFileIO::FullFileReader<TYPE_BOUNDARY>;
using TYPE_BOUNDARY_WRITER = CPPFileIO::FileWriter<TYPE_BOUNDARY>;

using TYPE_CHAR_BUFFER = CPPFileIO::Dynamic1DArray<char, TYPE_INT>;
using TYPE_CHAR_CONST_BUFFER = CPPFileIO::Dynamic1DArray<char const, TYPE_INT>;

using TYPE_COORDINATE =
    Spherical_Geometry_Clustering::D2GPS_Coordinates<TYPE_FLOAT, TYPE_INT>;

TYPE_BYTE constexpr NTH = 64;

#endif
