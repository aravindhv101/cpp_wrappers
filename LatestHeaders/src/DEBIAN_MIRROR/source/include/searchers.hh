#ifndef _HEADER_GUARD_searchers_hh_
#define _HEADER_GUARD_searchers_hh_

//////////////////////////////
// Headers related BEGIN: { //
//////////////////////////////
#include "./include/Read_Show_Functions.hh"
#include "./definitions.hh"
////////////////////////////
// Headers related END. } //
////////////////////////////

/////////////////////////////////////
// Searcher for debugging BEGIN: { //
/////////////////////////////////////
#define _MACRO_CLASS_NAME_ word_dbg
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    using TYPE_INT  = size_t;
    using CHAR      = CPPFileIO::TYPE_BYTE;

  public:
    static inline TYPE_INT constexpr SIZE() { return 5; }

    static inline CHAR constexpr GET_CHAR(size_t const i) {
        switch (i) {
            case 4: return '-';
            case 3: return 'd';
            case 2: return 'b';
            case 1: return 'g';
            case 0: return '_';
            default: return 0;
        }
    }

    static inline TYPE_INT constexpr GET_JUMP(CHAR const in) {
        switch (in) {
            case '-': return 4;
            case '_': return 0;
            case 'b': return 2;
            case 'd': return 3;
            case 'g': return 1;
            default: return SIZE();
        }
    }
};
#undef _MACRO_CLASS_NAME_
///////////////////////////////////
// Searcher for debugging END. } //
///////////////////////////////////

////////////////////////////////////////
// Searcher for source files BEGIN: { //
////////////////////////////////////////
#define _MACRO_CLASS_NAME_ word_source
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    using TYPE_INT  = size_t;
    using CHAR      = CPPFileIO::TYPE_BYTE;

  public:
    static inline TYPE_INT constexpr SIZE() { return 15; }

    static inline CHAR constexpr GET_CHAR(size_t const i) {
        switch (i) {
            case 14: return '/';
            case 13: return 's';
            case 12: return 'o';
            case 11: return 'u';
            case 10: return 'r';
            case 9: return 'c';
            case 8: return 'e';
            case 7: return '/';
            case 6: return 'S';
            case 5: return 'o';
            case 4: return 'u';
            case 3: return 'r';
            case 2: return 'c';
            case 1: return 'e';
            case 0: return 's';
            default: return 0;
        }
    }

    static inline TYPE_INT constexpr GET_JUMP(CHAR const in) {
        switch (in) {
            case '/': return 7;
            case 'S': return 6;
            case 'c': return 2;
            case 'e': return 1;
            case 'o': return 5;
            case 'r': return 3;
            case 's': return 0;
            case 'u': return 4;
            default: return SIZE();
        }
    }
};
#undef _MACRO_CLASS_NAME_
//////////////////////////////////////
// Searcher for source files END. } //
//////////////////////////////////////

//////////////////////////////////////
// Searcher for i386 files BEGIN: { //
//////////////////////////////////////
#define _MACRO_CLASS_NAME_ word_i386
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    using TYPE_INT  = size_t;
    using CHAR      = CPPFileIO::TYPE_BYTE;

  public:
    static inline TYPE_INT constexpr SIZE() { return 5; }

    static inline CHAR constexpr GET_CHAR(size_t const i) {
        switch (i) {
            case 4: return '-';
            case 3: return 'i';
            case 2: return '3';
            case 1: return '8';
            case 0: return '6';
            default: return 0;
        }
    }

    static inline TYPE_INT constexpr GET_JUMP(CHAR const in) {
        switch (in) {
            case '-': return 4;
            case '3': return 2;
            case '6': return 0;
            case '8': return 1;
            case 'i': return 3;
            default: return SIZE();
        }
    }
};
#undef _MACRO_CLASS_NAME_
////////////////////////////////////
// Searcher for i386 files END. } //
////////////////////////////////////

///////////////////////////////////////
// Searcher for amd64 files BEGIN: { //
///////////////////////////////////////
#define _MACRO_CLASS_NAME_ word_amd64
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    using TYPE_INT  = size_t;
    using CHAR      = CPPFileIO::TYPE_BYTE;

  public:
    static inline TYPE_INT constexpr SIZE() { return 6; }

    static inline CHAR constexpr GET_CHAR(size_t const i) {
        switch (i) {
            case 5: return '-';
            case 4: return 'a';
            case 3: return 'm';
            case 2: return 'd';
            case 1: return '6';
            case 0: return '4';
            default: return 0;
        }
    }

    static inline TYPE_INT constexpr GET_JUMP(CHAR const in) {
        switch (in) {
            case '-': return 5;
            case '4': return 0;
            case '6': return 1;
            case 'a': return 4;
            case 'd': return 2;
            case 'm': return 3;
            default: return SIZE();
        }
    }
};
#undef _MACRO_CLASS_NAME_
/////////////////////////////////////
// Searcher for amd64 files END. } //
/////////////////////////////////////

//////////////////////////////////////////
// Searcher for all arch files BEGIN: { //
//////////////////////////////////////////
#define _MACRO_CLASS_NAME_ word_all
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    using TYPE_INT  = size_t;
    using CHAR      = CPPFileIO::TYPE_BYTE;

  public:
    static inline TYPE_INT constexpr SIZE() { return 4; }

    static inline CHAR constexpr GET_CHAR(size_t const i) {
        switch (i) {
            case 3: return '-';
            case 2: return 'a';
            case 1: return 'l';
            case 0: return 'l';
            default: return 0;
        }
    }

    static inline TYPE_INT constexpr GET_JUMP(CHAR const in) {
        switch (in) {
            case '-': return 3;
            case 'a': return 2;
            case 'l': return 0;
            default: return SIZE();
        }
    }
};
#undef _MACRO_CLASS_NAME_
////////////////////////////////////////
// Searcher for all arch files END. } //
////////////////////////////////////////

/////////////////////////////////////////////
// Searcher for translation files BEGIN: { //
/////////////////////////////////////////////
#define _MACRO_CLASS_NAME_ word_i18n_Translation_en
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    using TYPE_INT  = size_t;
    using CHAR      = CPPFileIO::TYPE_BYTE;

  public:
    static inline TYPE_INT constexpr SIZE() { return 20; }

    static inline CHAR constexpr GET_CHAR(size_t const i) {
        switch (i) {
            case 19: return '/';
            case 18: return 'i';
            case 17: return '1';
            case 16: return '8';
            case 15: return 'n';
            case 14: return '/';
            case 13: return 'T';
            case 12: return 'r';
            case 11: return 'a';
            case 10: return 'n';
            case 9: return 's';
            case 8: return 'l';
            case 7: return 'a';
            case 6: return 't';
            case 5: return 'i';
            case 4: return 'o';
            case 3: return 'n';
            case 2: return '-';
            case 1: return 'e';
            case 0: return 'n';
            default: return 0;
        }
    }

    static inline TYPE_INT constexpr GET_JUMP(CHAR const in) {
        switch (in) {
            case '-': return 2;
            case '/': return 14;
            case '1': return 17;
            case '8': return 16;
            case 'T': return 13;
            case 'a': return 7;
            case 'e': return 1;
            case 'i': return 5;
            case 'l': return 8;
            case 'n': return 0;
            case 'o': return 4;
            case 'r': return 12;
            case 's': return 9;
            case 't': return 6;
            default: return SIZE();
        }
    }
};
#undef _MACRO_CLASS_NAME_
///////////////////////////////////////////
// Searcher for translation files END. } //
///////////////////////////////////////////

//////////////////////////////////////////////////////
// Searcher for llvm style debugging files BEGIN: { //
//////////////////////////////////////////////////////
#define _MACRO_CLASS_NAME_ word_dbgsym
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    using TYPE_INT  = size_t;
    using CHAR      = CPPFileIO::TYPE_BYTE;

  public:
    static inline TYPE_INT constexpr SIZE() { return 8; }

    static inline CHAR constexpr GET_CHAR(size_t const i) {
        switch (i) {
            case 7: return '-';
            case 6: return 'd';
            case 5: return 'b';
            case 4: return 'g';
            case 3: return 's';
            case 2: return 'y';
            case 1: return 'm';
            case 0: return '_';
            default: return 0;
        }
    }

    static inline TYPE_INT constexpr GET_JUMP(CHAR const in) {
        switch (in) {
            case '-': return 7;
            case '_': return 0;
            case 'b': return 5;
            case 'd': return 6;
            case 'g': return 4;
            case 'm': return 1;
            case 's': return 3;
            case 'y': return 2;
            default: return SIZE();
        }
    }
};
#undef _MACRO_CLASS_NAME_
////////////////////////////////////////////////////
// Searcher for llvm style debugging files END. } //
////////////////////////////////////////////////////

////////////////////////////////////
// Important definitions BEGIN: { //
////////////////////////////////////
using find_i386 = CPPFileIO::StaticHorsePoolSearch<char_buffer, word_i386>;

using find_amd64 = CPPFileIO::StaticHorsePoolSearch<char_buffer, word_amd64>;

using find_all = CPPFileIO::StaticHorsePoolSearch<char_buffer, word_all>;

using find_translation =
  CPPFileIO::StaticHorsePoolSearch<char_buffer, word_i18n_Translation_en>;

using find_sources = CPPFileIO::StaticHorsePoolSearch<char_buffer, word_source>;
//////////////////////////////////
// Important definitions END. } //
//////////////////////////////////

#endif
