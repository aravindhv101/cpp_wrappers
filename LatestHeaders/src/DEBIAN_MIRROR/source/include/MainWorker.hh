#ifndef _HEADER_GUARD_MainWorker_hh_
#define _HEADER_GUARD_MainWorker_hh_

//////////////////////////////
// Headers related BEGIN: { //
//////////////////////////////
#include "./include/Read_Show_Functions.hh"
#include "./basic_functions.hh"
#include "./definitions.hh"
#include "./searchers.hh"
#include "./dist_downloader.hh"
#include "./element.hh"
////////////////////////////
// Headers related END. } //
////////////////////////////

#define _MACRO_CLASS_NAME_ MainWorker

/////////////////////////////////
// Main working class BEGIN: { //
/////////////////////////////////

class _MACRO_CLASS_NAME_ {

    //////////////////////////
    // Definitions BEGIN: { //
    //////////////////////////
  public:
    using TYPE_INT   = size_t;
    using TYPE_SELF  = _MACRO_CLASS_NAME_;
    using ATOMIC_INT = CPPFileIO::Atomic_Counter<TYPE_INT>;
    ////////////////////////
    // Definitions END. } //
    ////////////////////////

    ////////////////////////////
    // Data elements BEGIN: { //
    ////////////////////////////
  private:
    elements         STORE;
    dists_downloader SLAVE;
    ATOMIC_INT       COUNTER_FILE, COUNTER_THREAD;
    bool             DOWNLOAD_NON_STOP;
    //////////////////////////
    // Data elements END. } //
    //////////////////////////

    /////////////////////////////////////////////////////////
    // functions to get list of available mirrors BEGIN: { //
    /////////////////////////////////////////////////////////
  private:
    inline string const &BASE_MIRROR_URL() const { return SLAVE.base_urls[0]; }

    inline string const &BASE_MIRROR_URL(size_t const index) const {
        return SLAVE.base_urls[index % SLAVE.base_urls.size()];
    }
    ///////////////////////////////////////////////////////
    // functions to get list of available mirrors END. } //
    ///////////////////////////////////////////////////////

    ///////////////////////////////////////
    // Simple wrapper functions BEGIN: { //
    ///////////////////////////////////////
  private:
    inline string const &GET_PREFIX() const { return SLAVE.prefix; }

    inline strings const &GET_LINK_DISTS() const { return SLAVE.final_urls; }

    inline bool IS_DBG(size_t const i) const { return STORE[i].is_dbg(); }

    inline void DOWNLOAD_DISTS() /* Wrapper function: */ {
        dists_downloader::get_dists();
    }
    /////////////////////////////////////
    // Simple wrapper functions END. } //
    /////////////////////////////////////

    ////////////////////////////////////////////////////
    // Prepare the list of files to download BEGIN: { //
    ////////////////////////////////////////////////////
  private:
    inline void GET_LIST() {
        STORE.clear();

        elements tmpbuf;

        if (true) /* populate the tmp buffer: */ {
            strings in;
            GET_PACKAGE_LIST(in);

            for (size_t id_in = 0; id_in < in.size(); id_in++) {
                strings list_sha, list_filename;

                if (true) /* Get the filenames: */ {
                    CPPFileIO::FileLines<8> lines(in[id_in]);
                    for (size_t i = 0; i < lines(); i++) {
                        if (SHAMATCH(
                              reinterpret_cast<char *>(lines(i).get_start()))) {
                            list_sha.push_back(reinterpret_cast<char *>(
                              lines(i).get_start() + 8));
                        }

                        if (FILENAMEMATCH(
                              reinterpret_cast<char *>(lines(i).get_start()))) {
                            list_filename.push_back(reinterpret_cast<char *>(
                              lines(i).get_start() + 10));
                        }
                    }
                }

                if (true) /* Evaluate the elements: */ {
                    size_t const min =
                      CPPFileIO::mymin(list_sha.size(), list_filename.size());

                    for (size_t i = 0; i < min; i++) {
                        element tmp;
                        tmp.filename = list_filename[i];
                        tmp.sha256   = list_sha[i];
                        tmpbuf.push_back(tmp);
                    }
                }
            }
        }

        if (tmpbuf.size() > 0) {

            if (tmpbuf.size() > 1) { std::sort(tmpbuf.begin(), tmpbuf.end()); }

            STORE.push_back(tmpbuf[0]);
            for (size_t i = 1; i < tmpbuf.size(); i++) {
                if (tmpbuf[i] != tmpbuf[i - 1]) { STORE.push_back(tmpbuf[i]); }
            }
        }
    }
    //////////////////////////////////////////////////
    // Prepare the list of files to download END. } //
    //////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    // Function to clean unnecessary functions BEGIN: { //
    //////////////////////////////////////////////////////
  public:
    inline void CLEAN_SHA() {
        rmdir("./WASTE");
        GET_LIST();

        if (true) /* Move the SHA256 directory: */ {
            CPPFileIO::ExternalStarter<true>::GET("/bin/mv")("-vf")("--")(
              "./SHA256")("./WASTE");
        }

        mkdir("./SHA256", 0755);

#pragma omp parallel for
        for (size_t nth = 0; nth < NTH; nth++) {
            size_t const start = (STORE.size() * nth) / NTH;
            size_t const stop  = (STORE.size() * (nth + 1)) / NTH;

            for (size_t i = start; i < stop; i++) {
                if (!IS_DBG(i)) {
                    string const input = "./WASTE/" + STORE[i].sha256;
                    DO_MOVE(input);
                }
            }
        }

        rmdir("./WASTE");
    }

  public:
    static inline void clean_sha() {
        TYPE_SELF slave;
        slave.CLEAN_SHA();
    }
    ////////////////////////////////////////////////////
    // Function to clean unnecessary functions END. } //
    ////////////////////////////////////////////////////

    //////////////////////////////////////////////////
    // Main function for downloading files BEGIN: { //
    //////////////////////////////////////////////////
  private:
    inline void DOWNLOAD_LIST(TYPE_INT const index_file,
                              TYPE_INT const index_mirror) {

        if (!IS_DBG(index_file)) {
            DOWNLOAD(BASE_MIRROR_URL(index_mirror) + "/" +
                       STORE[index_file].filename,
                     STORE[index_file].sha256);
        }
    }

    inline void DOWNLOAD_LIST(TYPE_INT const index_mirror) {
        if (DOWNLOAD_NON_STOP) {
            for (TYPE_INT i = (COUNTER_FILE++); true; i = (COUNTER_FILE++)) {
                i = i % STORE.size();
                DOWNLOAD_LIST(i, index_mirror);
            }
        } else {
            for (TYPE_INT i = (COUNTER_FILE++); i < STORE.size();
                 i          = (COUNTER_FILE++)) {
                DOWNLOAD_LIST(i, index_mirror);
            }
        }
    }

    inline void DOWNLOAD_LIST() { DOWNLOAD_LIST(COUNTER_THREAD++); }

  public:
    inline void operator()() { DOWNLOAD_LIST(); }

  private:
    inline void DOWNLOAD_LIST_ALL(bool const download_non_stop = false) {
        DOWNLOAD_NON_STOP = download_non_stop;
        string const out_dir("./SHA256/");
        mkdir(out_dir.c_str(), 0755);

        COUNTER_FILE         = 0;
        COUNTER_THREAD       = 0;
        std::thread *threads = new std::thread[NTH];

        GET_LIST();
        for (TYPE_INT i = 0; i < NTH; ++i) {
            threads[i] = std::thread(CPPFileIO::thread_slave<TYPE_SELF>, this);
        }

        for (TYPE_INT i = 0; i < NTH; ++i) { threads[i].join(); }

        delete[] threads;
    }

  public:
    static inline void download_list() {
        TYPE_SELF slave;
        slave.DOWNLOAD_LIST_ALL(false);
    }

    static inline void download_list_non_stop() {
        TYPE_SELF slave;
        slave.DOWNLOAD_LIST_ALL(true);
    }
    ////////////////////////////////////////////////
    // Main function for downloading files END. } //
    ////////////////////////////////////////////////

    ////////////////////////////////////////////
    // Functions to link final files BEGIN: { //
    ////////////////////////////////////////////
  private:
    inline void DO_LINK(std::string const outfile, std::string const infile) {
        std::string           buffer = outfile;
        CPPFileIO::MyStr<int> buf(buffer);
        auto const           &cuts   = buf('/');
        size_t const          limit  = cuts.size();
        std::string           outdir = ".";
        for (size_t i = 0; i < limit - 1; i++) {
            outdir = outdir + "/" + cuts[i].start();
            mkdir(outdir.c_str(), 0755);
        }
        /* int ret = */ symlink(/* const char *target */ infile.c_str(),
                                /* const char *linkpath */ outfile.c_str());
    }

    inline void SETUP_FILES() {
        GET_LIST();
        string const prefix = GET_PREFIX() + "/";
#pragma omp parallel for
        for (size_t i = 0; i < STORE.size(); i++) {
            if (!IS_DBG(i)) {
                string const outfile = prefix + STORE[i].filename;
                size_t       counts  = 0;
                for (char const &i : outfile) {
                    if (i == '/') { counts++; }
                }
                // if (counts > 0) { counts--; }
                string infile = "./SHA256/" + STORE[i].sha256;
                while (counts > 0) {
                    infile = "../" + infile;
                    counts--;
                }
                DO_LINK(outfile, infile);
            }
        }
    }

  public:
    static inline void setup_files() {
        TYPE_SELF slave;
        slave.SETUP_FILES();
    }
    //////////////////////////////////////////
    // Functions to link final files END. } //
    //////////////////////////////////////////
};

///////////////////////////////
// Main working class END. } //
///////////////////////////////

/////////////////////////////////////////////
// Public interface for functions BEGIN: { //
/////////////////////////////////////////////
inline void clean_sha() { _MACRO_CLASS_NAME_::clean_sha(); }
inline void download_list() { _MACRO_CLASS_NAME_::download_list(); }
inline void download_list_non_stop() {
    _MACRO_CLASS_NAME_::download_list_non_stop();
}
inline void setup_files() { _MACRO_CLASS_NAME_::setup_files(); }
///////////////////////////////////////////
// Public interface for functions END. } //
///////////////////////////////////////////

#undef _MACRO_CLASS_NAME_

#endif
