#ifndef _HEADER_GUARD_dist_downloader_hh_
#define _HEADER_GUARD_dist_downloader_hh_

//////////////////////
// Headers BEGIN: { //
//////////////////////
#include "./include/Read_Show_Functions.hh"
#include "./searchers.hh"
#include "./definitions.hh"
#include "./basic_functions.hh"
////////////////////
// Headers END. } //
////////////////////

#define _MACRO_CLASS_NAME_ dists_downloader

/////////////////////////////////
// Main working class BEGIN: { //
/////////////////////////////////
class _MACRO_CLASS_NAME_ {
    ////////////////////////////////////
    // Important definitions BEGIN: { //
    ////////////////////////////////////
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    //////////////////////////////////
    // Important definitions END. } //
    //////////////////////////////////

    ////////////////////////////
    // Data elements BEGIN: { //
    ////////////////////////////
  public:
    strs base_urls;
    strs versions;
    strs release_urls;
    strs components;
    strs final_urls;
    str  prefix;
    //////////////////////////
    // Data elements END. } //
    //////////////////////////

    ////////////////////////////////////////////
    // Configure all URLs and suites BEGIN: { //
    ////////////////////////////////////////////
  private:
    inline void CONFIGURE() {
#include "./config.hh"
    }
    //////////////////////////////////////////
    // Configure all URLs and suites END. } //
    //////////////////////////////////////////

    /////////////////////////////////////////////
    // Download the actual dists file BEGIN: { //
    /////////////////////////////////////////////
  private:
    inline void
    FETCHER(std::string const &sha, std::string const &file,
            std::string const &url) /* Download the actual files: */ {

        //////////////////////////////////////////////////////
        // dont bother if the file is not required BEGIN: { //
        //////////////////////////////////////////////////////
        if (!PASS_PACKAGE(file)) { return; }
        ////////////////////////////////////////////////////
        // dont bother if the file is not required END. } //
        ////////////////////////////////////////////////////

        CPPFileIO::ForkMe forker;

        ///////////////////////////////////////////////////////////////
        // Fork to not mess with parent's working directory BEGIN: { //
        ///////////////////////////////////////////////////////////////
        if (forker.InKid()) {
            if (true) { MKDIR_F(file); }

            if (true) /* Evaluate the symlink path */ {
                std::string            buf = file;
                CPPFileIO::MyStr<long> str(buf);
                auto const            &cut = str('/');
                std::string            out;

                if (true) /* Evaluate the final path: */ {
                    for (size_t i = 1; i < cut.size(); i++) {
                        out = out + "../";
                    }
                    out = out + "SHA256/" + sha;
                }

                if (true) /* Create the symbolic link: */ {
                    std::string tmp = file;

                    /* int const ret = */ symlink(
                      /* const char *target = */ out.c_str(),
                      /* const char *linkpath = */ CPPFileIO::basename(tmp)
                        .c_str());
                }
            }

        } else if (forker.InKid()) {

            if (true) /* Move to the directory: */ {
                mkdir("./SHA256", 0755);
                chdir("./SHA256");
            }

            if (true) /* Download the file: */ {
                CPPFileIO::ExternalStarter<true>::GET("/usr/bin/wget")("-c")(
                  url)("-O")(sha)("--timeout=10");
            }
        }
        /////////////////////////////////////////////////////////////
        // Fork to not mess with parent's working directory END. } //
        /////////////////////////////////////////////////////////////
    }
    ///////////////////////////////////////////
    // Download the actual dists file END. } //
    ///////////////////////////////////////////

    ////////////////////////////////////////////////////////////
    // Work on individual line from the release file BEGIN: { //
    ////////////////////////////////////////////////////////////
    inline void PROCESS_LINES(std::string in, std::string const &base_url) {

        CPPFileIO::MyStr<size_t> buffer(in);
        auto const               res    = buffer(' ');
        size_t                   counts = 0;
        size_t                   i      = 0;
        string                   sha256, file;

    FindLoop:
        if (i < res.size()) /* Gather all the information */ {

            if (res[i].start()[0] != 0) {

                counts++;
                switch (counts) {
                    case 1: sha256 = res[i].start(); break;
                    case 3: file = res[i].start(); goto Process;
                }
            }

            i++;
            goto FindLoop;
        }

    Process:
        if (i < res.size()) /* Work on the information: */ {

            str const full_url = base_url + "/" + res[i].start();
            str const file     = res[i].start();
            FETCHER(sha256, file, full_url);
        }
    }
    //////////////////////////////////////////////////////////
    // Work on individual line from the release file END. } //
    //////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    // Download the release file and process the lines BEGIN: { //
    //////////////////////////////////////////////////////////////
    inline void WORK_ON(str const &base_url, str const &version) {

        CPPFileIO::ForkMe forker;

        if (forker.InKid()) /* Fork to preserve parent's working directory: */ {

            if (true) /* Prepare the directory structure and cd to it: */ {
                mkdir("MIRROR", 0755);
                chdir("MIRROR");
                mkdir("dists", 0755);
                chdir("dists");
                mkdir(version.c_str(), 0755);
                chdir(version.c_str());
            }

            if (true) /* Prepare the file urls: */ {
                str release    = base_url + "/dists/" + version + "/Release";
                str in_release = base_url + "/dists/" + version + "/InRelease";
                str release_gpg =
                  base_url + "/dists/" + version + "/Release.gpg";

                if (true) /* Download the release files: */ {
                    WGET(release);
                    WGET(release_gpg);
                    WGET(in_release);
                }
            }

            CPPFileIO::FileLines<4> lines("Release");

            if (true) /* Process the lines: */ {

                char constexpr SHA256[] = "SHA256:";
                char constexpr SHA512[] = "SHA512:";
                char constexpr MD5SUM[] = "MD5Sum:";
                char constexpr SHA1[]   = "SHA1:";
                std::vector<std::string> interesting_lines;

                CPPFileIO::TYPE_BYTE mode = 0;
                for (size_t i = 0; i < lines(); i++) {
                    if (Read_Show_Functions::Compare(
                          reinterpret_cast<char *>(lines(i).get_start()),
                          SHA256) == 0) {
                        mode = 1;
                    }

                    if (Read_Show_Functions::Compare(
                          reinterpret_cast<char *>(lines(i).get_start()),
                          SHA512) == 0) {
                        mode = 0;
                    }

                    if (Read_Show_Functions::Compare(
                          reinterpret_cast<char *>(lines(i).get_start()),
                          MD5SUM) == 0) {
                        mode = 0;
                    }

                    if (Read_Show_Functions::Compare(
                          reinterpret_cast<char *>(lines(i).get_start()),
                          SHA1) == 0) {
                        mode = 0;
                    }

                    if (mode) {
                        interesting_lines.push_back(
                          reinterpret_cast<char *>(lines(i).get_start()));
                    }
                }

                str const send_url = base_url + "/dists/" + version;
                CPPFileIO::Atomic_Counter<size_t> counter(0);

#pragma omp parallel for
                for (size_t nth = 0; nth < NTH; ++nth) {
                    for (size_t i                        = (counter += 1);
                         i < interesting_lines.size(); i = (counter += 1)) {

                        PROCESS_LINES(interesting_lines[i], send_url);
                    }
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////
    // Download the release file and process the lines END. } //
    ////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    // download the release files for all the versions BEGIN: { //
    //////////////////////////////////////////////////////////////
  private:
    inline void GET_RELEASE() {
        CPPFileIO::ForkMe forker;
        for (size_t i = 0; i < versions.size(); i++)
            if (forker.InKid()) { WORK_ON(base_urls[0], versions[i]); }
    }
    ////////////////////////////////////////////////////////////
    // download the release files for all the versions END. } //
    ////////////////////////////////////////////////////////////

  public:
    _MACRO_CLASS_NAME_() : prefix("OUT") { CONFIGURE(); }

    /////////////////////////////////////
    // Main exposed interface BEGIN: { //
    /////////////////////////////////////
  public:
    static inline void get_dists() {
        TYPE_SELF slave;
        slave.GET_RELEASE();
    }
    ///////////////////////////////////
    // Main exposed interface END. } //
    ///////////////////////////////////
};
///////////////////////////////
// Main working class END. } //
///////////////////////////////

/////////////////////////////////////
// Main exposed interface BEGIN: { //
/////////////////////////////////////
inline void get_dists() { _MACRO_CLASS_NAME_::get_dists(); }
///////////////////////////////////
// Main exposed interface END. } //
///////////////////////////////////

#undef _MACRO_CLASS_NAME_

#endif
