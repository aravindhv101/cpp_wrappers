#ifndef _HEADER_GUARD_element_hh_
#define _HEADER_GUARD_element_hh_

//////////////////////////////
// Headers Related BEGIN: { //
//////////////////////////////
#include "./include/Read_Show_Functions.hh"
#include "./definitions.hh"
#include "./searchers.hh"
////////////////////////////
// Headers Related END. } //
////////////////////////////

#define _MACRO_CLASS_NAME_ element

/////////////////////////////////
// Main working class BEGIN: { //
/////////////////////////////////

class _MACRO_CLASS_NAME_ {

    //////////////////////////
    // Definitions BEGIN: { //
    //////////////////////////
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;

    using TYPE_SEARCH_DBG =
      CPPFileIO::StaticHorsePoolSearch<char_buffer, word_dbg>;

    using TYPE_SEARCH_DBGSYM =
      CPPFileIO::StaticHorsePoolSearch<char_buffer, word_dbgsym>;
    ////////////////////////
    // Definitions END. } //
    ////////////////////////

    /////////////////////////////////
    // Main data elements BEGIN: { //
    /////////////////////////////////
  public:
    string filename, sha256;
    ///////////////////////////////
    // Main data elements END. } //
    ///////////////////////////////

    ////////////////////////////////////////
    // Main comparison operators BEGIN: { //
    ////////////////////////////////////////
  public:
    inline bool operator>(TYPE_SELF const &other) const {
        return sha256 > other.sha256;
    }

    inline bool operator<(TYPE_SELF const &other) const {
        return sha256 < other.sha256;
    }

    inline bool operator==(TYPE_SELF const &other) const {
        return (sha256 == other.sha256);
    }

    inline bool operator!=(TYPE_SELF const &other) const {
        return (sha256 != other.sha256);
    }
    //////////////////////////////////////
    // Main comparison operators END. } //
    //////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    // Function to check if the given file is for debugging symbols BEGIN: { //
    ///////////////////////////////////////////////////////////////////////////
  public:
    inline bool is_dbg() const {
        char_buffer buffer(&(filename[0]), filename.size());

        if (true) /* look for debian style dbg */ {
            TYPE_SEARCH_DBG searcher1(buffer);
            auto const      res1 = searcher1(0);
            if (res1 < buffer()) { return true; }
        }

        if (true) /* look for llvm style dbg */ {
            TYPE_SEARCH_DBGSYM searcher2(buffer);
            auto const         res2 = searcher2(0);
            if (res2 < buffer()) { return true; }
        }

        return false;
    }
    /////////////////////////////////////////////////////////////////////////
    // Function to check if the given file is for debugging symbols END. } //
    /////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////
    // Constructor & Destructor BEGIN: { //
    ///////////////////////////////////////
  public:
    _MACRO_CLASS_NAME_(string FILENAME, string SHA256)
      : filename(FILENAME), sha256(SHA256) {}

    _MACRO_CLASS_NAME_() {}
    ~_MACRO_CLASS_NAME_() {}
    /////////////////////////////////////
    // Constructor & Destructor END. } //
    /////////////////////////////////////
};

///////////////////////////////
// Main working class END. } //
///////////////////////////////

using elements = std::vector<_MACRO_CLASS_NAME_>;

#undef _MACRO_CLASS_NAME_

#endif
