#ifndef _HEADER_GUARD_basic_functions_hh_
#define _HEADER_GUARD_basic_functions_hh_

//////////////////////
// Headers BEGIN: { //
//////////////////////
#include "./include/Read_Show_Functions.hh"
#include "./searchers.hh"
#include "./definitions.hh"
////////////////////
// Headers END. } //
////////////////////

////////////////////////////
// WGET Wrappers BEGIN: { //
////////////////////////////
inline void WGET_MIRROR(string const in) {
    CPPFileIO::ExternalStarter<true>::GET("/usr/bin/wget")("-c")("-m")(
      "--timeout=10")("-np")(in);
}

inline void WGET(string const in, string const out) {
    CPPFileIO::ExternalStarter<true>::GET("/usr/bin/wget")("-c")("-O")(
      out)("--timeout=10")(in);
}

inline void WGET(string const in) {
    CPPFileIO::ExternalStarter<true>::GET("/usr/bin/wget")("-c")(
      "--timeout=10")(in);
}
//////////////////////////
// WGET Wrappers END. } //
//////////////////////////

/////////////////////////////////
// Making directories BEGIN: { //
/////////////////////////////////
inline void MKDIR_P(std::string in) {
    CPPFileIO::MyStr<long> buffer(in);
    auto const            &cuts = buffer('/');
    std::string            res;
    long const             limit = cuts.size() - 1;
    for (long i = 0; i <= limit; i++) {
        res = res + cuts[i].start() + "/";
        mkdir(res.c_str(), 0755);
    }
}

inline void MKDIR_F(std::string in) {
    CPPFileIO::MyStr<long> buffer(in);
    auto const            &cuts = buffer('/');
    std::string            res;
    long const             limit = cuts.size() - 1;
    for (long i = 0; i < limit; i++) {
        mkdir(cuts[i].start(), 0755);
        chdir(cuts[i].start());
    }
}
///////////////////////////////
// Making directories END. } //
///////////////////////////////

//////////////////////////////////////
// Rename file as its hash BEGIN: { //
//////////////////////////////////////
inline std::string const RENAME_2_HASH(std::string const in) {
    std::string const hash = CPPFileIO::SHA256SUM(in);

    rename(
      /*const char *__old =*/in.c_str(),
      /*const char *__new =*/hash.c_str());

    return hash;
}
////////////////////////////////////
// Rename file as its hash END. } //
////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Functions to match if line beginning is SHA or filename BEGIN: { //
//////////////////////////////////////////////////////////////////////
inline bool SHAMATCH(string const &in) {
    if (in.size() < 9) {
        return false;
    } else {
        size_t constexpr buff = 2322228159967217747; // Deadly vodoo !
        size_t const *inn     = reinterpret_cast<size_t const *>(&(in[0]));
        return !(inn[0] ^ buff);
    }
}

inline bool FILENAMEMATCH(string const &in) {
    if (in.size() < 11) {
        return false;
    } else {
        size_t constexpr pos0              = 7308604897068083526;
        CPPFileIO::TYPE_U16 constexpr pos1 = 8250;
        size_t const *part0 = reinterpret_cast<size_t const *>(&(in[0]));

        CPPFileIO::TYPE_U16 const *part1 =
          reinterpret_cast<CPPFileIO::TYPE_U16 const *>(&(in[8]));

        return (!(part0[0] ^ pos0)) & (!(part1[0] ^ pos1));
    }
}
////////////////////////////////////////////////////////////////////
// Functions to match if line beginning is SHA or filename END. } //
////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////
// Get list of package related files BEGIN: { //
////////////////////////////////////////////////
inline void GET_PACKAGE_LIST(strings &list) {
    list.clear();
    auto res = CPPFileIO::find::file("./MIRROR");

    for (size_t i = 0; i < res(); i++) {
        if (res(i).get_size() > 8) {
            char const *tmp = reinterpret_cast<char*>(res(i).get_stop() - 8);
            if (memcmp(tmp, "Packages", 8) == 0) {
                list.push_back(reinterpret_cast<char *>(res(i).get_start()));
            }
        }
    }
}
//////////////////////////////////////////////
// Get list of package related files END. } //
//////////////////////////////////////////////

///////////////////////////////////////////////////
// Move the file into the SHA256 folder BEGIN: { //
///////////////////////////////////////////////////
inline void DO_MOVE(std::string const src) {
    std::string           buffer = src;
    CPPFileIO::MyStr<int> str(buffer);
    auto const           &cuts = str('/');

    std::string const outname =
      std::string("./SHA256/") + cuts[cuts.size() - 1].start();

    int const ret = rename(
      /*const char *oldpath =*/src.c_str(),
      /*const char *newpath =*/outname.c_str());
}
/////////////////////////////////////////////////
// Move the file into the SHA256 folder END. } //
/////////////////////////////////////////////////

//////////////////////////////////////////////////
// Main downloading function for files BEGIN: { //
//////////////////////////////////////////////////
inline void DOWNLOAD(string const in, string const out) {
    std::string const outfile = std::string("./SHA256/") + out;
    int const         ret     = access(outfile.c_str(), F_OK);

    if (ret != 0) {
        CPPFileIO::ForkMe forker;

        if (forker.InKid()) {

            if (true) /* Prepare the directory structure: */ {
                mkdir("./TMP", 0755);
                chdir("./TMP");
                mkdir(out.c_str(), 0755);
                chdir(out.c_str());
            }

            if (true) {
                WGET(in, out);

                std::string const hash =
                  RENAME_2_HASH(/*const std::string in =*/out);

                mkdir("../../SHA256", 0755);

                std::string const final_out =
                  std::string("../../SHA256/") + hash;

                rename(/*const char *__old =*/hash.c_str(),
                       /*const char *__new =*/final_out.c_str());
            }

            if (true) /* Clean the directory structure: */ {
                chdir("../");
                rmdir(out.c_str());
                chdir("../");
                rmdir("./TMP");
            }
        }
    }
}
////////////////////////////////////////////////
// Main downloading function for files END. } //
////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Function to check if the dists file represents packages BEGIN: { //
//////////////////////////////////////////////////////////////////////
inline bool PASS_PACKAGE(
  std::string const
    &filename) /* Filter to decide whether to download the file or not: */ {

    char_buffer buffer(&(filename[0]), filename.size());

    if (true) /* Check for i386: */ {
        find_i386  searcher_i386(buffer);
        auto const ret_i386 = searcher_i386(0);
        if (ret_i386 < buffer()) { return true; }
    }

    if (true) /* Check for amd64: */ {
        find_amd64 searcher_amd64(buffer);
        auto const ret_amd64 = searcher_amd64(0);
        if (ret_amd64 < buffer()) { return true; }
    }

    if (true) /* Check for sources: */ {
        find_sources searcher(buffer);
        auto const   ret = searcher(0);
        if (ret < buffer()) { return true; }
    }

    if (true) /* check for all: */ {
        find_all   searcher_all(buffer);
        auto const ret_all = searcher_all(0);
        if (ret_all < buffer()) { return true; }
    }

    if (true) /* check for english translations: */ {
        find_translation searcher_translation(buffer);
        auto const       ret_translation = searcher_translation(0);
        if (ret_translation < buffer()) { return true; }
    }

    return false;
}
////////////////////////////////////////////////////////////////////
// Function to check if the dists file represents packages END. } //
////////////////////////////////////////////////////////////////////

#endif
