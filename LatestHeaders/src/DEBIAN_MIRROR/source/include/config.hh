base_urls.push_back("http://ftp.us.debian.org/debian");
if(true) {
base_urls.push_back("http://ftp2.de.debian.org/debian");
base_urls.push_back("http://ftp.at.debian.org/debian");
base_urls.push_back("http://ftp.au.debian.org/debian");
base_urls.push_back("http://ftp.be.debian.org/debian");
base_urls.push_back("http://ftp.ca.debian.org/debian");
base_urls.push_back("http://ftp.ch.debian.org/debian");
base_urls.push_back("http://ftp.cn.debian.org/debian");
base_urls.push_back("http://ftp.de.debian.org/debian");
base_urls.push_back("http://ftp.dk.debian.org/debian");
base_urls.push_back("http://ftp.es.debian.org/debian");
base_urls.push_back("http://ftp.fi.debian.org/debian");
base_urls.push_back("http://ftp.fr.debian.org/debian");
base_urls.push_back("http://ftp.hk.debian.org/debian");
base_urls.push_back("http://ftp.is.debian.org/debian");
base_urls.push_back("http://ftp.it.debian.org/debian");
base_urls.push_back("http://ftp.jp.debian.org/debian");
base_urls.push_back("http://ftp.kr.debian.org/debian");
base_urls.push_back("http://ftp.nl.debian.org/debian");
base_urls.push_back("http://ftp.no.debian.org/debian");
base_urls.push_back("http://ftp.nz.debian.org/debian");
base_urls.push_back("http://ftp.se.debian.org/debian");
base_urls.push_back("http://ftp.tw.debian.org/debian");
base_urls.push_back("http://ftp.uk.debian.org/debian");
base_urls.push_back("http://debianmirror.nkn.in/debian");
base_urls.push_back("http://debian.sbnw.in/debian");
base_urls.push_back("http://debmirror.hbcse.tifr.res.in/debian");
base_urls.push_back("http://mirror.cse.iitk.ac.in/debian");
}

versions.push_back("stable");
versions.push_back("stable-backports");
versions.push_back("testing");

components.push_back("main");
components.push_back("contrib");
components.push_back("non-free");
