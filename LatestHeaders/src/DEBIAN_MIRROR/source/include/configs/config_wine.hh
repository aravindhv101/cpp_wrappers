base_urls.push_back("https://dl.winehq.org/wine-builds/debian/");

versions.push_back("stable");
versions.push_back("testing");

components.push_back("main");
components.push_back("contrib");
components.push_back("non-free");
