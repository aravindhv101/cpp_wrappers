base_urls.push_back("https://apt.llvm.org/bullseye/") ;

versions.push_back("llvm-toolchain-bullseye-11");
versions.push_back("llvm-toolchain-bullseye-12");
versions.push_back("llvm-toolchain-bullseye-13");

components.push_back("main");
components.push_back("contrib");
components.push_back("non-free");
