#!/bin/bash
'tail' '-n' '+5' "${0}"
exit '0'

#define _MACRO_CLASS_NAME_ <++>
class _MACRO_CLASS_NAME_ {
	public:
		using TYPE_SELF=_MACRO_CLASS_NAME_;
		<++>

	private:
		<++>

	public:
		inline void operator () (<++>) {<++>}
		inline void operator [] (<++>) {<++>}
		inline void operator > (TYPE_SELF const & other) const {<++>}
		inline void operator < (TYPE_SELF const & other) const {<++>}
		<++>

	public:
		_MACRO_CLASS_NAME_(<++>){<++>}
		~_MACRO_CLASS_NAME_(){<++>}
};
#undef _MACRO_CLASS_NAME_
