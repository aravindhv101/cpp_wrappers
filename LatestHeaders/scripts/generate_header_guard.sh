#!/bin/sh
MACRO="$('echo' "${1}" | 'sed' 's@/@_@g ; s@\.@_@g ; s@^@_HEADER_GUARD_@g ; s@$@_@g ; s@__*@_@g ')"
'echo' "#ifndef ${MACRO}"
'echo' "#define ${MACRO}"
'echo' "#endif"
exit '0'
