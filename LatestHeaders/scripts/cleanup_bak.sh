#!/bin/bash
cd "$('dirname' '--' "${0}")"
'find' './' '-type' 'f' | 'grep' '~$\|#$' | 'sed' 's@^@"rm" "-vf" "--" "@g;s@$@"@g' | '/bin/bash'
'rm' '-rf' './.cache/'
exit '0'
