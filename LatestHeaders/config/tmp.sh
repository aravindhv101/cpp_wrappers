* Snippets:

** Prepare the directory structure:
   #+BEGIN_SRC sh
     mkdir -pv "${HOME}/.emacs.d/snippets/antlr-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/apples-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/applescript-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/bazel-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/bibtex-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/c++-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/c-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/cc-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/chef-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/cider-repl-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/clojure-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/cmake-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/conf-unix-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/coq-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/cperl-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/cpp-omnet-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/crystal-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/csharp-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/css-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/d-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/dart-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/dix-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/dockerfile-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/elixir-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/emacs-lisp-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/enh-ruby-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/ensime-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/erc-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/erlang-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/f90-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/faust-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/fish-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/fundamental-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/git-commit-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/go-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/groovy-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/haskell-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/html-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/hy-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/java-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/js-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/js2-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/js3-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/julia-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/kotlin-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/latex-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/lisp-interaction-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/lisp-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/lua-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/m4-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/makefile-automake-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/makefile-bsdmake-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/makefile-gmake-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/makefile-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/malabar-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/markdown-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/nasm-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/ned-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/nesc-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/nix-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/nsis-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/nxml-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/octave-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/org-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/perl-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/php-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/powershell-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/prog-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/protobuf-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/python-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/racket-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/reason-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/rjsx-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/rst-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/ruby-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/rust-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/rustic-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/scala-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/sh-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/snippet-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/sql-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/swift-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/terraform-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/text-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/tuareg-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/typerex-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/typescript-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/udev-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/vhdl-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/web-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/yaml-mode"
     mkdir -pv "${HOME}/.emacs.d/snippets/gnuplot-mode"
   #+END_SRC

   #+RESULTS:

** GNU Plot:
   #+begin_src conf :tangle ~/.emacs.d/snippets/gnuplot-mode/full
     # -*- mode: snippet -*-
     # name: gnuplt
     # key: gnuplt
     # --

     #!/usr/bin/gnuplot
     set terminal tikz latex color size 15cm, 10cm scale 1.1, 1.1 fulldoc createstyle
     set grid
     set output "${1:out}.tex"
     set label "label" at 100,100 textcolor rgb "#FF0000"
     ${2:#}set log y

     #set xrange [0:1]
     #set yrange [0:1]

     plot \
     "${3:in.txt}"  with lines lt rgb "${4:#FF0000}" title "${5:in}" ,\
     "${6:in2.txt}" with boxes title "${7:title}" fill transparent solid 0.25 lc rgb "${8:#0000FF}"

     exit
   #+end_src

** C++:

*** Easy macros:
    #+begin_src conf :tangle ~/.emacs.d/snippets/c++-mode/def
      # -*- mode: snippet -*-
      # name: def
      # key: def
      # --

      #define ${1:_MACRO_}($2)
      $0
      #undef $1
    #+end_src

*** custom class start:
    #+begin_src conf :tangle ~/.emacs.d/snippets/c++-mode/mycl
      # -*- mode: snippet -*-
      # name: mycl
      # key: mycl
      # --

      #define _MACRO_CLASS_NAME_ ${1:class_name}
      class _MACRO_CLASS_NAME_ {
      public:
      using TYPE_SELF = _MACRO_CLASS_NAME_; $0

      public:
      _MACRO_CLASS_NAME_($2){$3}

      ~_MACRO_CLASS_NAME_(){$4}
      };
      #undef _MACRO_CLASS_NAME_
    #+end_src


* EMACS config

** Config from menu:
   #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
     (custom-set-variables
      ;; custom-set-variables was added by Custom.
      ;; If you edit it by hand, you could mess it up, so be careful.
      ;; Your init file should contain only one such instance.
      ;; If there is more than one, they won't work right.
      '(custom-safe-themes
        '("5eb4b22e97ddb2db9ecce7d983fa45eb8367447f151c7e1b033af27820f43760" "8e7044bfad5a2e70dfc4671337a4f772ee1b41c5677b8318f17f046faa42b16b" "4c8372c68b3eab14516b6ab8233de2f9e0ecac01aaa859e547f902d27310c0c3" "1a094b79734450a146b0c43afb6c669045d7a8a5c28bc0210aba28d36f85d86f" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "95b0bc7b8687101335ebbf770828b641f2befdcf6d3c192243a251ce72ab1692" default))
      '(global-display-line-numbers-mode t)
      '(menu-bar-mode nil)
      '(show-paren-mode t)
      '(tool-bar-mode nil))

     (custom-set-faces
      ;; custom-set-faces was added by Custom.
      ;; If you edit it by hand, you could mess it up, so be careful.
      ;; Your init file should contain only one such instance.
      ;; If there is more than one, they won't work right.
      )
   #+END_SRC

** Custom variable values:

*** LSP mode improve performance:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (setq gc-cons-threshold 100000000)
      (setq read-process-output-max 1048576)
      (setq lsp-idle-delay 0.1)
    #+END_SRC

*** Server config:
**** Produce the server file:
     #+BEGIN_SRC lisp :tangle ~/.emacs.d/server.el
       (setq server-use-tcp 1)
       (setq server-port 7654)
       (server-start)
     #+END_SRC

**** Eval the file:
     #+BEGIN_SRC lisp :tangle ~/.emacs.d/junk.el
       (load-file "~/.emacs.d/server.el")
     #+END_SRC

*** Smart mode line:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (setq sml/theme 'dark)
    #+END_SRC

*** ACE window config:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (setq ace-window-display-mode 1)
    #+END_SRC

*** Disable confirming code execution in org babel:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (setq org-confirm-babel-evaluate nil)
    #+END_SRC

*** LSP mode key bindings:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (setq lsp-keymap-prefix (kbd "C-c l"))
    #+END_SRC

** List of packages:

*** which key:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (use-package which-key
        :ensure nil
        :config
        (setq which-key-idle-delay 0)
        (which-key-mode))
    #+END_SRC

*** Evil mode:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (use-package evil
        :ensure nil
        :config (evil-mode 1))
    #+END_SRC

*** Company mode:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (use-package company
        :ensure nil
        :config
        (setq company-idle-delay 0)
        (setq company-minimum-prefix-length 0)
        (global-set-key (kbd "M-/") 'company-dabbrev-code)
        (global-set-key (kbd "C-c j") 'company-complete)
        (setq company-async-timeout 10))
    #+END_SRC

*** Yasnippet:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (use-package yasnippet
        :ensure nil
        :config (yas-global-mode 1))
    #+END_SRC

*** AG:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (use-package ag
        :ensure nil
        :config
        (setq ag-context-lines 4)
        (setq ag-highlight-search 4)
        (global-set-key (kbd "C-c g") 'ag))
    #+END_SRC

*** Neotree:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (use-package neotree
        :ensure nil
        :config
        (global-set-key (kbd "C-c d") 'neotree-toggle))
    #+END_SRC

*** DIRED

    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (use-package dired
        :ensure nil
        :config (setq dired-listing-switches "-alhF --group-directories-first"))
    #+END_SRC

*** IVY:

**** Main IVY:
     #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
       (use-package ivy
         :ensure nil
         :config
         (ivy-mode))
     #+END_SRC

**** Counsel:

     #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
       (use-package counsel
         :ensure nil
         :config
         (global-set-key (kbd "C-c h") 'counsel-company)
         (global-set-key (kbd "M-x") 'counsel-M-x)
         (global-set-key (kbd "C-c z") 'counsel-fzf)
         (global-set-key (kbd "C-c <tab>") 'counsel-switch-buffer))
     #+END_SRC

**** Swiper:

     #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
       (use-package swiper
         :ensure nil
         :config
         (global-set-key (kbd "C-c f") 'swiper)
         (global-set-key (kbd "C-c a") 'swiper-all)
         (global-set-key (kbd "C-c v") 'swiper-avy))
     #+END_SRC

*** Key Chords:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (defun myfun/bb1 ()
        (interactive)
        (insert "()"))

      (defun myfun/bb2 ()
        (interactive)
        (insert "[]"))

      (defun myfun/bb3 ()
        (interactive)
        (insert "<>"))

      (defun myfun/bb4 ()
        (interactive)
        (insert "{}"))

      (defun myfun/save_and_expand ()
        (interactive)
        (setq mytmpline (line-number-at-pos))
        (shell-command-on-region (point-min) (point-max) "expand" (current-buffer) t "*fcc error*" t)
        (basic-save-buffer)
        (goto-line mytmpline))

      (use-package key-chord
        :ensure nil
        :config
        (setq key-chord-two-keys-delay 0.1)

                                              ; Good bracketing:
        (key-chord-define-global "()" 'myfun/bb1)
        (key-chord-define-global "[]" 'myfun/bb2)
        (key-chord-define-global "<>" 'myfun/bb3)
        (key-chord-define-global "{}" 'myfun/bb4)

                                              ; Push bracketing:
        (key-chord-define-global "(*" "()\C-b")
        (key-chord-define-global "p[" "[]\C-b")
        (key-chord-define-global "M<" "<>\C-b")
        (key-chord-define-global "P{" "{}\C-b")

                                              ; Window specific navigation:
        (key-chord-define-global (kbd "` w") 'neotree-toggle)
        (key-chord-define-global (kbd "1 w") 'ace-window)
        (key-chord-define-global (kbd "2 w") 'counsel-switch-buffer)
        (key-chord-define-global (kbd "3 w") 'counsel-dired)
        (key-chord-define-global (kbd "3 t") 'tear-off-window)

                                              ; Find within file:
        (key-chord-define-global (kbd "1 e") 'swiper-avy)
        (key-chord-define-global (kbd "2 e") 'swiper)
        (key-chord-define-global (kbd "3 e") 'swiper-all)

                                              ; YAS:
        (key-chord-define-global "\\]" 'yas-insert-snippet)

                                              ; Replace tabs with spaces:
        (key-chord-define-global "4t" 'myfun/save_and_expand)

                                              ; Start key chord mode:
        (key-chord-mode 1))
    #+END_SRC

*** Main theme:

**** Simple monokai:
     #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
       (setq modus-themes-org-blocks rainbow)
       ;; (load-theme 'modus-vivendi)
       (load-theme 'monokai)
       (global-set-key (kbd "<f5>") 'modus-themes-toggle)
     #+END_SRC

**** Doom themes:
     #+BEGIN_SRC lisp :tangle ~/.emacs.d/doom_theme.el
       (use-package doom-themes
         :ensure nil
         :load-path "~/.emacs.d/emacs-doom-themes/"
         :config
         ;; Global settings (defaults)
         (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
               doom-themes-enable-italic t) ; if nil, italics is universally disabled
         (load-theme 'doom-gruvbox t)

         ;; Enable flashing mode-line on errors
                                               ; (doom-themes-visual-bell-config)
         ;; Enable custom neotree theme (all-the-icons must be installed!)
                                               ; (doom-themes-neotree-config)
         ;; or for treemacs users
         (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
                                               ; (doom-themes-treemacs-config)
         ;; Corrects (and improves) org-mode's native fontification.
                                               ; (doom-themes-org-config)
         )
     #+END_SRC

** List of global modes:

*** Line number related:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (global-display-line-numbers-mode t)
      (linum-relative-global-mode)
    #+END_SRC

*** Remove UI Junk:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (menu-bar-mode 0)
      (tool-bar-mode 0)
    #+END_SRC

*** Smart mode line:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (sml/setup)
    #+END_SRC

*** Show matching paranthesis:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (show-paren-mode t)
    #+END_SRC

*** IVY:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/junk.el
      (ivy-mode)
    #+END_SRC

*** Dynamic completions mode:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (dynamic-completion-mode)
    #+END_SRC

** General global key bindings:

*** Cancel everything!
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (global-set-key (kbd "<escape>") 'keyboard-escape-quit)
    #+END_SRC

*** Basic Convenience bindings:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (global-set-key (kbd "C-c p") 'find-file-at-point)
      (global-set-key (kbd "C-c o") 'ace-window)
      (global-set-key (kbd "C-c s") 'eshell)
    #+END_SRC

** General text and programming mode setup:

*** HS Minor Mode
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (add-hook 'prog-mode-hook 'hs-minor-mode)
      (add-hook 'text-mode-hook 'hs-minor-mode)
    #+END_SRC
   
*** Rainbow delimiter mode:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
      (add-hook 'text-mode-hook 'rainbow-delimiters-mode)
    #+END_SRC

*** Rainbow identifier mode:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (add-hook 'dired-mode-hook 'rainbow-identifiers-mode)
      (add-hook 'prog-mode-hook 'rainbow-identifiers-mode)
      (add-hook 'text-mode-hook 'rainbow-identifiers-mode)
    #+END_SRC

*** Rainbow mode:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (add-hook 'dired-mode-hook 'rainbow-mode)
      (add-hook 'prog-mode-hook 'rainbow-mode)
      (add-hook 'text-mode-hook 'rainbow-mode)
    #+END_SRC

*** Abbrev mode:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (add-hook 'prog-mode-hook 'abbrev-mode)
      (add-hook 'text-mode-hook 'abbrev-mode)
    #+END_SRC

*** Company mode:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (add-hook 'prog-mode-hook 'company-mode)
      (add-hook 'text-mode-hook 'company-mode)
    #+END_SRC

*** Highlight indentation mode:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (add-hook 'prog-mode-hook 'highlight-indentation-mode)
      (add-hook 'text-mode-hook 'highlight-indentation-mode)
    #+END_SRC

*** Projectile mode:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/projectile.el
      (add-hook 'prog-mode-hook 'projectile-mode)
      (add-hook 'text-mode-hook 'projectile-mode)
    #+END_SRC

** Config for C / C++:

*** LSP:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (add-hook 'c++-mode-hook 'lsp)
      (add-hook 'c-mode-hook 'lsp)
    #+END_SRC

*** Formating:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (defun myfun/save_and_format_c ()
        (interactive)
        (setq mytmpline (line-number-at-pos))
        (shell-command-on-region (point-min) (point-max) "fcc" (current-buffer) t "*fcc error*" t)
        (basic-save-buffer)
        (goto-line mytmpline))

      (add-hook 'c++-mode-hook (lambda () (key-chord-define-local "5t" 'myfun/save_and_format_c)))
      (add-hook 'c-mode-hook (lambda () (key-chord-define-local "5t" 'myfun/save_and_format_c)))

      (add-hook 'c++-mode-hook (lambda () (local-set-key (kbd "C-c t") 'myfun/save_and_format_c)))
      (add-hook 'c-mode-hook (lambda () (local-set-key (kbd "C-c t") 'myfun/save_and_format_c)))
    #+END_SRC

*** Pointer dereferencing:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (add-hook 'c++-mode-hook (lambda () (key-chord-define-local ".;"  "->")))
      (add-hook 'c-mode-hook (lambda () (key-chord-define-local ".;"  "->")))
    #+END_SRC

** Python mode:
   #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
     (defun myfun/save_and_format_py ()
       (interactive)
       (setq mytmpline (line-number-at-pos))
       (shell-command-on-region (point-min) (point-max) "yapf3" (current-buffer) t "*yapf3 error*" t)
       (basic-save-buffer)
       (goto-line mytmpline))

     (add-hook 'python-mode-hook (lambda () (key-chord-define-local "5t" 'myfun/save_and_format_py)))
     (add-hook 'python-mode-hook (lambda () (local-set-key (kbd "C-c t") 'myfun/save_and_format_py)))
   #+END_SRC

** ESHELL Controles:
   #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
     (defhydra myfun/eshells (:color blue)
       "eshell control"
       ("b" eshell-bol "eshell-bol")
       ("z" eshell-z "eshell-z")
       ("c" eshell-kill-input "eshell-kill-input")
       ("x" eshell-kill-process "eshell-kill-process"))

     (add-hook 'eshell-mode-hook (lambda () (key-chord-define-local ";[" 'myfun/eshells/body)))
   #+END_SRC

** Projectile configs:
   #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
     (defhydra myfun/projectiles (:color blue)
       "toggle"
       ("f" projectile-find-file "projectile-find-file")
       ("d" projectile-find-dir "projectile-find-dir")
       ("e" projectile-dired "projectile-dired")
       ("s" projectile-run-eshell "projectile-run-eshell")
       ("g" projectile-ag "projectile-ag"))

     (key-chord-define-global (kbd "; '") 'myfun/projectiles/body)
   #+END_SRC

** ORG mode:

*** ORG mode key bindings for format and save:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (defun myfun/save_and_format_org ()
        (interactive)
        (setq mytmpline (line-number-at-pos))
        (org-indent-region (point-min) (point-max))
        (shell-command-on-region (point-min) (point-max) "expand" (current-buffer) t "*format org error*" t)
        (basic-save-buffer)
        (goto-line mytmpline))

      (add-hook 'org-mode-hook (lambda () (key-chord-define-local "5t" 'myfun/save_and_format_org)))
    #+END_SRC


*** ORG mode key bindings:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (defhydra myfun/org (:color red)
        "org controles"
        ("e" org-babel-execute-src-block "execute source block")
        ("k" org-babel-goto-src-block-head "org-babel-goto-src-block-head")
        ("l" org-babel-next-src-block "org-babel-next-src-block")
        ("h" org-babel-previous-src-block "org-babel-previous-src-block")
        ("v" org-babel-mark-block "org-babel-mark-block")
        ("g" org-set-tags-command "org-set-tags-command" :color blue)
        ("x" org-export-dispatch "org-export-dispatch" :color blue)
        ("q" nil "cancel" :color blue)
        ("t" org-babel-tangle "org babel tangle" :color blue))

      (add-hook 'org-mode-hook (lambda () (key-chord-define-local ";[" 'myfun/org/body)))
    #+END_SRC


*** Languages in org babel:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/init.el
      (org-babel-do-load-languages
       'org-babel-load-languages '(
                                   (emacs-lisp . t)
                                   (python . t)
                                   (R . t)
                                   (eshell . t)
                                   (awk . t)
                                   (sql . t)
                                   (shell . t)
                                   (sqlite . t)
                                   )
       )
    #+END_SRC

** Eval other files:

*** Eval doom theme:                                               :DISABLED:
    #+BEGIN_SRC lisp :tangle ~/.emacs.d/junk.el
      (load-file "~/.emacs.d/doom_theme.el")
    #+END_SRC

* dnsmasq

** The main config file
   #+BEGIN_SRC shell :tangle ~/dnsmasq.conf
     addn-hosts=/etc/dnsmasq/hosts
     expand-hosts
     local-service
     resolv-file=/etc/dnsmasq/resolv.conf
     all-servers
     dhcp-range=192.168.111.101,192.168.111.200,255.255.255.0
   #+END_SRC

** Hosts file related
   #+BEGIN_SRC shell :tangle ~/hosts
     127.0.0.1	localhost
     ::1		localhost ip6-localhost ip6-loopback
     ff02::1		ip6-allnodes
     ff02::2		ip6-allrouters
     127.0.0.1	localhost.localdomain
   #+END_SRC

** resolv.conf related
   #+BEGIN_SRC shell :tangle ~/resolv.conf
     nameserver 4.2.2.1
     nameserver 4.2.2.2
     nameserver 4.2.2.3
     nameserver 4.2.2.4
     nameserver 4.2.2.5
     nameserver 4.2.2.6
     nameserver 8.8.4.4
     nameserver 8.8.8.8
     nameserver 202.88.156.6
     nameserver 202.88.156.8
     nameserver fe80::1%wlp1s0
   #+END_SRC

* FISH

** Get vim bindings:
   #+BEGIN_SRC shell :tangle ~/.config/fish/config.fish
     fish_vi_key_bindings
   #+END_SRC

** Set the path to include home folder:
   #+BEGIN_SRC shell :tangle ~/.config/fish/config.fish
     set PATH {$HOME}/bin {$PATH}
   #+END_SRC

** Define convenience for sudo:
   This might be a *big fat security risk*...
   #+BEGIN_SRC shell :tangle ~/.config/fish/config.fish
     export SUDO_ASKPASS={$HOME}/SUDO_ASKPASS

     function sudo
     /usr/bin/sudo -A $argv
     end
   #+END_SRC

   Write the script which outputs the password:
   #+BEGIN_SRC sh :tangle ~/SUDO_ASKPASS :shebang #!/bin/sh
     echo 'asd' # ofcourse, this is fake!
     exit
   #+END_SRC

** Convenient alias
   #+BEGIN_SRC shell :tangle ~/.config/fish/config.fish
     function top
     /usr/bin/htop $argv
     end

     function puthere
     mysync (cat /tmp/list) ./
     end
   #+END_SRC

** Define mysync wrapper if the actual executable is missing:
   #+BEGIN_SRC shell :tangle ~/.config/fish/config.fish
     function mysync
     rsync '-avh' '--progress' $argv
     sync ; sync
     end
   #+END_SRC
