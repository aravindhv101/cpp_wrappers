set wrap
set number
set foldmethod=syntax
set relativenumber
set cursorline
set cursorcolumn
colorscheme gruvbox
let g:gruvbox_contrast_light='hard'
let g:airline_theme='dark'

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_aggregate_errors = 1

inoremap <c-j> <Esc>/<++><CR><Esc>cf>
inoremap ( ()<++><Esc>F)i
inoremap [ []<++><Esc>F]i
inoremap { {}<++><Esc>F}i
inoremap " ""<++><Esc>F"i
let g:ycm_confirm_extra_conf = 0
let g:ycm_extra_conf_globlist = ['*']
