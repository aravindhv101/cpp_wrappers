(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(monokai))
 '(custom-safe-themes
   '("95b0bc7b8687101335ebbf770828b641f2befdcf6d3c192243a251ce72ab1692" default))
 '(display-line-numbers-type 'visual)
 '(global-display-line-numbers-mode t)
 '(menu-bar-mode nil)
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Bitstream Vera Sans Mono" :foundry "Bits" :slant normal :weight normal :height 143 :width normal)))))

(use-package evil)
(use-package yasnippet)

(setq company-idle-delay 0)
(setq company-minimum-prefix-length 0)

(evil-mode 1)
(yas-global-mode 1)

(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
(add-hook 'foo-mode-hook #'rainbow-delimiters-mode)

(add-hook 'prog-mode-hook #'rainbow-identifiers-mode)
(add-hook 'foo-mode-hook #'rainbow-identifiers-mode)

(add-hook 'prog-mode-hook #'rainbow-mode)
(add-hook 'foo-mode-hook #'rainbow-mode)

(add-hook 'prog-mode-hook #'global-company-mode)
(add-hook 'foo-mode-hook #'global-company-mode)

(add-hook 'prog-mode-hook #'global-linum-mode)
(add-hook 'foo-mode-hook #'global-linum-mode)

(add-hook 'prog-mode-hook #'highlight-indentation-mode)
(add-hook 'foo-mode-hook #'highlight-indentation-mode)


(global-set-key (kbd "C-c h") #'company-complete)

(dynamic-completion-mode)

(ivy-mode)

(global-set-key (kbd "M-x") #'counsel-M-x)
(global-set-key (kbd "M-/") #'company-dabbrev-code)
(global-set-key (kbd "<escape>") #'keyboard-escape-quit)
(global-set-key (kbd "C-c f") #'swiper)
(global-set-key (kbd "C-c a") #'swiper-all)

(which-key-mode)
