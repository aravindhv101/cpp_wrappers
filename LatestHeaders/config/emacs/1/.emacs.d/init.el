(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("5eb4b22e97ddb2db9ecce7d983fa45eb8367447f151c7e1b033af27820f43760" "8e7044bfad5a2e70dfc4671337a4f772ee1b41c5677b8318f17f046faa42b16b" "4c8372c68b3eab14516b6ab8233de2f9e0ecac01aaa859e547f902d27310c0c3" "1a094b79734450a146b0c43afb6c669045d7a8a5c28bc0210aba28d36f85d86f" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "95b0bc7b8687101335ebbf770828b641f2befdcf6d3c192243a251ce72ab1692" default))
 '(global-display-line-numbers-mode t)
 '(menu-bar-mode nil)
 '(show-paren-mode t)
 '(tool-bar-mode nil))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(setq gc-cons-threshold 100000000)
(setq read-process-output-max 1048576)
(setq lsp-idle-delay 0.1)

(load-file "~/.emacs.d/server.el")

(setq sml/theme 'dark)

(setq ace-window-display-mode 1)

(setq org-confirm-babel-evaluate nil)

(setq lsp-keymap-prefix (kbd "C-c l"))

(use-package which-key
  :ensure nil
  :config
  (setq which-key-idle-delay 0)
  (which-key-mode))

(use-package evil
  :ensure nil
  :config (evil-mode 1))

(use-package company
  :ensure nil
  :config
  (setq company-idle-delay 0)
  (setq company-minimum-prefix-length 0)
  (global-set-key (kbd "M-/") 'company-dabbrev-code)
  (global-set-key (kbd "C-c j") 'company-complete)
  (setq company-async-timeout 10))

(use-package yasnippet
  :ensure nil
  :config (yas-global-mode 1))

(use-package ag
  :ensure nil
  :config
  (setq ag-context-lines 4)
  (setq ag-highlight-search 4)
  (global-set-key (kbd "C-c g") 'ag))

(use-package neotree
  :ensure nil
  :config
  (global-set-key (kbd "C-c d") 'neotree-toggle))

(use-package dired
  :ensure nil
  :config (setq dired-listing-switches "-alhF --group-directories-first"))

(use-package ivy
  :ensure nil
  :config
  (ivy-mode))

(use-package counsel
  :ensure nil
  :config
  (global-set-key (kbd "C-c h") 'counsel-company)
  (global-set-key (kbd "M-x") 'counsel-M-x)
  (global-set-key (kbd "C-c z") 'counsel-fzf)
  (global-set-key (kbd "C-c <tab>") 'counsel-switch-buffer))

(use-package swiper
  :ensure nil
  :config
  (global-set-key (kbd "C-c f") 'swiper)
  (global-set-key (kbd "C-c a") 'swiper-all)
  (global-set-key (kbd "C-c v") 'swiper-avy))

(defun myfun/bb1 ()
  (interactive)
  (insert "()"))

(defun myfun/bb2 ()
  (interactive)
  (insert "[]"))

(defun myfun/bb3 ()
  (interactive)
  (insert "<>"))

(defun myfun/bb4 ()
  (interactive)
  (insert "{}"))

(defun myfun/save_and_expand ()
  (interactive)
  (setq mytmpline (line-number-at-pos))
  (shell-command-on-region (point-min) (point-max) "expand" (current-buffer) t "*fcc error*" t)
  (basic-save-buffer)
  (goto-line mytmpline))

(use-package key-chord
  :ensure nil
  :config
  (setq key-chord-two-keys-delay 0.1)

                                        ; Good bracketing:
  (key-chord-define-global "()" 'myfun/bb1)
  (key-chord-define-global "[]" 'myfun/bb2)
  (key-chord-define-global "<>" 'myfun/bb3)
  (key-chord-define-global "{}" 'myfun/bb4)

                                        ; Push bracketing:
  (key-chord-define-global "(*" "()\C-b")
  (key-chord-define-global "p[" "[]\C-b")
  (key-chord-define-global "M<" "<>\C-b")
  (key-chord-define-global "P{" "{}\C-b")

                                        ; Window specific navigation:
  (key-chord-define-global (kbd "` w") 'neotree-toggle)
  (key-chord-define-global (kbd "1 w") 'ace-window)
  (key-chord-define-global (kbd "2 w") 'counsel-switch-buffer)
  (key-chord-define-global (kbd "3 w") 'counsel-dired)
  (key-chord-define-global (kbd "3 t") 'tear-off-window)

                                        ; Find within file:
  (key-chord-define-global (kbd "1 e") 'swiper-avy)
  (key-chord-define-global (kbd "2 e") 'swiper)
  (key-chord-define-global (kbd "3 e") 'swiper-all)

                                        ; YAS:
  (key-chord-define-global "\\]" 'yas-insert-snippet)

                                        ; Replace tabs with spaces:
  (key-chord-define-global "4t" 'myfun/save_and_expand)

                                        ; Start key chord mode:
  (key-chord-mode 1))

(load-theme 'monokai)

(global-display-line-numbers-mode t)
(linum-relative-global-mode)

(menu-bar-mode 0)
(tool-bar-mode 0)

(sml/setup)

(show-paren-mode t)

(dynamic-completion-mode)

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(global-set-key (kbd "C-c p") 'find-file-at-point)
(global-set-key (kbd "C-c o") 'ace-window)
(global-set-key (kbd "C-c s") 'eshell)

(add-hook 'prog-mode-hook 'hs-minor-mode)
(add-hook 'text-mode-hook 'hs-minor-mode)

(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(add-hook 'text-mode-hook 'rainbow-delimiters-mode)

(add-hook 'dired-mode-hook 'rainbow-identifiers-mode)
(add-hook 'prog-mode-hook 'rainbow-identifiers-mode)
(add-hook 'text-mode-hook 'rainbow-identifiers-mode)

(add-hook 'dired-mode-hook 'rainbow-mode)
(add-hook 'prog-mode-hook 'rainbow-mode)
(add-hook 'text-mode-hook 'rainbow-mode)

(add-hook 'prog-mode-hook 'abbrev-mode)
(add-hook 'text-mode-hook 'abbrev-mode)

(add-hook 'prog-mode-hook 'company-mode)
(add-hook 'text-mode-hook 'company-mode)

(add-hook 'prog-mode-hook 'highlight-indentation-mode)
(add-hook 'text-mode-hook 'highlight-indentation-mode)

(add-hook 'c++-mode-hook 'lsp)
(add-hook 'c-mode-hook 'lsp)

(defun myfun/save_and_format_c ()
  (interactive)
  (setq mytmpline (line-number-at-pos))
  (shell-command-on-region (point-min) (point-max) "fcc" (current-buffer) t "*fcc error*" t)
  (basic-save-buffer)
  (goto-line mytmpline))

(add-hook 'c++-mode-hook (lambda () (key-chord-define-local "5t" 'myfun/save_and_format_c)))
(add-hook 'c-mode-hook (lambda () (key-chord-define-local "5t" 'myfun/save_and_format_c)))

(add-hook 'c++-mode-hook (lambda () (local-set-key (kbd "C-c t") 'myfun/save_and_format_c)))
(add-hook 'c-mode-hook (lambda () (local-set-key (kbd "C-c t") 'myfun/save_and_format_c)))

(add-hook 'c++-mode-hook (lambda () (key-chord-define-local ".;"  "->")))
(add-hook 'c-mode-hook (lambda () (key-chord-define-local ".;"  "->")))

(defun myfun/save_and_format_py ()
  (interactive)
  (setq mytmpline (line-number-at-pos))
  (shell-command-on-region (point-min) (point-max) "yapf3" (current-buffer) t "*yapf3 error*" t)
  (basic-save-buffer)
  (goto-line mytmpline))

(add-hook 'python-mode-hook (lambda () (key-chord-define-local "5t" 'myfun/save_and_format_py)))
(add-hook 'python-mode-hook (lambda () (local-set-key (kbd "C-c t") 'myfun/save_and_format_py)))

(defhydra myfun/eshells (:color blue)
  "eshell control"
  ("b" eshell-bol "eshell-bol")
  ("z" eshell-z "eshell-z")
  ("c" eshell-kill-input "eshell-kill-input")
  ("x" eshell-kill-process "eshell-kill-process"))

(add-hook 'eshell-mode-hook (lambda () (key-chord-define-local ";[" 'myfun/eshells/body)))

(defhydra myfun/projectiles (:color blue)
  "toggle"
  ("f" projectile-find-file "projectile-find-file")
  ("d" projectile-find-dir "projectile-find-dir")
  ("e" projectile-dired "projectile-dired")
  ("s" projectile-run-eshell "projectile-run-eshell")
  ("g" projectile-ag "projectile-ag"))

(key-chord-define-global (kbd "; '") 'myfun/projectiles/body)

(defun myfun/save_and_format_org ()
  (interactive)
  (setq mytmpline (line-number-at-pos))
  (org-indent-region (point-min) (point-max))
  (shell-command-on-region (point-min) (point-max) "expand" (current-buffer) t "*format org error*" t)
  (basic-save-buffer)
  (goto-line mytmpline))

(add-hook 'org-mode-hook (lambda () (key-chord-define-local "5t" 'myfun/save_and_format_org)))

(defhydra myfun/org (:color red)
  "org controles"
  ("e" org-babel-execute-src-block "execute source block")
  ("k" org-babel-goto-src-block-head "org-babel-goto-src-block-head")
  ("l" org-babel-next-src-block "org-babel-next-src-block")
  ("h" org-babel-previous-src-block "org-babel-previous-src-block")
  ("v" org-babel-mark-block "org-babel-mark-block")
  ("g" org-set-tags-command "org-set-tags-command" :color blue)
  ("x" org-export-dispatch "org-export-dispatch" :color blue)
  ("q" nil "cancel" :color blue)
  ("t" org-babel-tangle "org babel tangle" :color blue))

(add-hook 'org-mode-hook (lambda () (key-chord-define-local ";[" 'myfun/org/body)))

(org-babel-do-load-languages
 'org-babel-load-languages '(
                             (emacs-lisp . t)
                             (python . t)
                             (R . t)
                             (eshell . t)
                             (awk . t)
                             (sql . t)
                             (shell . t)
                             (sqlite . t)
                             )
 )
