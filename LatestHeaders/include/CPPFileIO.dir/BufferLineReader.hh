#ifndef _HEADER_GUARD_CPPFileIO_dir_BufferLineReader_hh_
#define _HEADER_GUARD_CPPFileIO_dir_BufferLineReader_hh_

#include "./Basic.hh"
#include "./D1.hh"
#include "./MyStr.hh"

////////////////////////////////////////
// BufferLineReaderVectorized BEGIN:{ //
////////////////////////////////////////
#define _MACRO_CLASS_NAME_ BufferLineReaderVectorized
class _MACRO_CLASS_NAME_ {

    /////////////////////////
    // Definitions BEGIN:{ //
    /////////////////////////
  public:
    using TYPE_INT    = size_t;
    using TYPE_BUFFER = char;
    using TYPE_SELF   = _MACRO_CLASS_NAME_;
    using TYPE_WORDS  = std::vector<TYPE_BUFFER const *>;
    using TYPE_STRING = MyStr<TYPE_INT>;
    using TYPE_LINES  = TYPE_STRING::TYPE_SELVES;
    ///////////////////////
    // Definitions END.} //
    ///////////////////////

    ///////////////////////////
    // Data Elements BEGIN:{ //
    ///////////////////////////
  private:
    TYPE_STRING MEM_STR;
    TYPE_LINES  LINES;
    TYPE_WORDS  WORDS;
    TYPE_INT    LINE_LOC;
    char const  SEPERATOR, NEWLINE;
    /////////////////////////
    // Data Elements END.} //
    /////////////////////////

    ////////////////////////////////////
    // Main Working Functions BEGIN:{ //
    ////////////////////////////////////
  private:
    inline TYPE_WORDS &GET_WORDS() {
        if (LINE_LOC < LINES.size()) {
            TYPE_STRING line(LINES[LINE_LOC].start(), LINES[LINE_LOC].stop());
            auto        words = line(SEPERATOR);
            WORDS.resize(words.size());
            for (TYPE_INT i = 0; i < words.size(); i++) {
                WORDS[i] = words[i].start();
            }
            LINE_LOC++;
        } else {
            WORDS.clear();
        }
        return WORDS;
    }
    //////////////////////////////////
    // Main Working Functions END.} //
    //////////////////////////////////

    /////////////////////////////////
    // Convinent interface BEGIN{: //
    /////////////////////////////////
  public:
    inline TYPE_WORDS const &operator()() { return GET_WORDS(); }

    inline void operator()(TYPE_BUFFER *start, TYPE_INT const length) {
        MEM_STR(start, length);
        MEM_STR(LINES, NEWLINE);
        LINE_LOC = 0;
    }

    inline void operator()(TYPE_BUFFER *start, TYPE_BUFFER *end) {
        MEM_STR(start, end);
        MEM_STR(LINES, NEWLINE);
        LINE_LOC = 0;
    }

    inline void operator()(Dynamic1DArray<TYPE_BUFFER> &in) {
        this[0](in.GET_DATA(), in());
    }
    ///////////////////////////////
    // Convinent interface END}. //
    ///////////////////////////////

    //////////////////////////////////////
    // Constructor & Destructor BEGIN:{ //
    //////////////////////////////////////
    _MACRO_CLASS_NAME_(TYPE_BUFFER *buffer, TYPE_INT const mem_size,
                       char const seperator, char const newline)
      : SEPERATOR(seperator), NEWLINE(newline) {
        this[0](buffer, mem_size);
    }

    _MACRO_CLASS_NAME_(TYPE_BUFFER *start, TYPE_BUFFER *end,
                       char const seperator, char const newline)
      : SEPERATOR(seperator), NEWLINE(newline) {
        this[0](start, end);
    }

    _MACRO_CLASS_NAME_(Dynamic1DArray<TYPE_BUFFER> &in, char const seperator,
                       char const newline)
      : SEPERATOR(seperator), NEWLINE(newline) {
        this[0](in);
    }

    ~_MACRO_CLASS_NAME_() {}
    ////////////////////////////////////
    // Constructor & Destructor END.} //
    ////////////////////////////////////
};
#undef _MACRO_CLASS_NAME_

#define _MACRO_CLASS_NAME_ BufferLineReaderVectorizedOld
template <char seperator, char newline> class _MACRO_CLASS_NAME_ {

    /////////////////////////
    // Definitions BEGIN:{ //
    /////////////////////////
  public:
    using TYPE_INT    = size_t;
    using TYPE_BUFFER = char;
    using TYPE_SELF   = _MACRO_CLASS_NAME_<seperator, newline>;
    using TYPE_WORDS  = std::vector<TYPE_BUFFER const *>;
    using TYPE_STRING = MyStr<TYPE_INT>;
    using TYPE_LINES  = TYPE_STRING::TYPE_SELVES;
    ///////////////////////
    // Definitions END.} //
    ///////////////////////

    ///////////////////////////
    // Data Elements BEGIN:{ //
    ///////////////////////////
  private:
    TYPE_STRING MEM_STR;
    TYPE_LINES  LINES;
    TYPE_WORDS  WORDS;
    TYPE_INT    LINE_LOC;
    /////////////////////////
    // Data Elements END.} //
    /////////////////////////

    ////////////////////////////////////
    // Main Working Functions BEGIN:{ //
    ////////////////////////////////////
  private:
    inline TYPE_WORDS &GET_WORDS() {
        if (LINE_LOC < LINES.size()) {
            TYPE_STRING line(LINES[LINE_LOC].start(), LINES[LINE_LOC].stop());
            auto        words = line(seperator);
            WORDS.resize(words.size());
            for (TYPE_INT i = 0; i < words.size(); i++) {
                WORDS[i] = words[i].start();
            }
            LINE_LOC++;
        } else {
            WORDS.clear();
        }
        return WORDS;
    }

    //////////////////////////////////
    // Main Working Functions END.} //
    //////////////////////////////////

    /////////////////////////////////
    // Convinent interface BEGIN{: //
    /////////////////////////////////
  public:
    inline TYPE_WORDS const &operator()() { return GET_WORDS(); }

    inline void operator()(TYPE_BUFFER *start, TYPE_INT const length) {
        MEM_STR(start, length);
        MEM_STR(LINES, newline);
        LINE_LOC = 0;
    }

    inline void operator()(TYPE_BUFFER *start, TYPE_BUFFER *end) {
        MEM_STR(start, end);
        MEM_STR(LINES, newline);
        LINE_LOC = 0;
    }

    inline void operator()(Dynamic1DArray<TYPE_BUFFER> &in) {
        this[0](in.GET_DATA(), in());
    }
    ///////////////////////////////
    // Convinent interface END}. //
    ///////////////////////////////

    //////////////////////////////////////
    // Constructor & Destructor BEGIN:{ //
    //////////////////////////////////////
    _MACRO_CLASS_NAME_(TYPE_BUFFER *buffer, TYPE_INT const mem_size) {
        this[0](buffer, mem_size);
    }

    _MACRO_CLASS_NAME_(TYPE_BUFFER *start, TYPE_BUFFER *end) {
        this[0](start, end);
    }

    _MACRO_CLASS_NAME_(Dynamic1DArray<TYPE_BUFFER> &in) { this[0](in); }

    ~_MACRO_CLASS_NAME_() {}
    ////////////////////////////////////
    // Constructor & Destructor END.} //
    ////////////////////////////////////
};
#undef _MACRO_CLASS_NAME_
//////////////////////////////////////
// BufferLineReaderVectorized END.} //
//////////////////////////////////////

/////////////////////////////////
// BufferLineReaderNew BEGIN:{ //
/////////////////////////////////
#define _MACRO_CLASS_NAME_ BufferLineReaderNew
template <char seperator, char newline> class _MACRO_CLASS_NAME_ {

    /////////////////////////
    // Definitions BEGIN:{ //
    /////////////////////////
  public:
    using TYPE_SELF   = _MACRO_CLASS_NAME_<seperator, newline>;
    using TYPE_BUFFER = char;
    using TYPE_WORDS  = std::vector<TYPE_BUFFER const *>;
    ///////////////////////
    // Definitions END.} //
    ///////////////////////

    ///////////////////////////
    // Data Elements BEGIN:{ //
    ///////////////////////////
  private:
    size_t       MEM_LOC;
    size_t const MEM_SIZE;
    TYPE_BUFFER *BUFFER;
    TYPE_WORDS   WORDS;
    /////////////////////////
    // Data Elements END.} //
    /////////////////////////

    ////////////////////////////////////
    // Main Working Functions BEGIN:{ //
    ////////////////////////////////////
  private:
    inline TYPE_WORDS const &GET_WORDS() {
        WORDS.clear();
        bool allocate = true;

        while (MEM_LOC < MEM_SIZE) {
            if (allocate) {
                WORDS.push_back(&(BUFFER[MEM_LOC]));
                allocate = false;
            }

            switch (BUFFER[MEM_LOC]) {
                case seperator:
                    BUFFER[MEM_LOC] = 0;
                    MEM_LOC++;
                    allocate = true;
                    break;

                case newline:
                    BUFFER[MEM_LOC] = 0;
                    MEM_LOC++;
                    return WORDS;

                default: MEM_LOC++; break;
            }
        }
        BUFFER[MEM_SIZE - 1] = 0;
        return WORDS;
    }

    //////////////////////////////////
    // Main Working Functions END.} //
    //////////////////////////////////

    /////////////////////////////////
    // Convinent interface BEGIN{: //
    /////////////////////////////////
  public:
    inline TYPE_WORDS const &operator()() { return GET_WORDS(); }
    ///////////////////////////////
    // Convinent interface END}. //
    ///////////////////////////////

    //////////////////////////////////////
    // Constructor & Destructor BEGIN:{ //
    //////////////////////////////////////
    _MACRO_CLASS_NAME_(TYPE_BUFFER *buffer, size_t const mem_size)
      : BUFFER(buffer), MEM_SIZE(mem_size), MEM_LOC(0) {}

    _MACRO_CLASS_NAME_(Dynamic1DArray<TYPE_BUFFER> &in)
      : BUFFER(in.GET_DATA()), MEM_SIZE(in()), MEM_LOC(0) {}

    ~_MACRO_CLASS_NAME_() {}
    ////////////////////////////////////
    // Constructor & Destructor END.} //
    ////////////////////////////////////
};
#undef _MACRO_CLASS_NAME_
///////////////////////////////
// BufferLineReaderNew END.} //
///////////////////////////////

#endif
