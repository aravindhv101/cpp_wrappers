#ifndef _HEADER_GUARD_CPPFileIO_dir_redirect_fd_hh_
#define _HEADER_GUARD_CPPFileIO_dir_redirect_fd_hh_

#include "./Basic.hh"

#define _MACRO_CLASS_NAME_ redirect_fd
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;

  private:
    std::string const FILENAME;
    int const         REDIRECTED_FD;
    int               COPIED_FD;
    int               FILE_FD;

  public:
    _MACRO_CLASS_NAME_(std::string const filename, int const fd)
      : FILENAME(filename), REDIRECTED_FD(fd) {
        FILE_FD   = open(FILENAME.c_str(), O_CREAT | O_RDWR, 0755);
        COPIED_FD = dup(REDIRECTED_FD);
        dup2(FILE_FD, REDIRECTED_FD);
    }

    ~_MACRO_CLASS_NAME_() {
        dup2(COPIED_FD, REDIRECTED_FD);
        close(COPIED_FD);
        close(FILE_FD);
    }
};
#undef _MACRO_CLASS_NAME_

#endif
