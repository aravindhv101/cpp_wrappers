﻿#ifndef _HEADER_GUARD_CPPFileIO_FileFD_
#define _HEADER_GUARD_CPPFileIO_FileFD_

/////////////////////
// Includes BEGIN: //
/////////////////////
#include "../Headers.hh"
#include "./Basic.hh"
///////////////////
// Includes END. //
///////////////////

#define _MACRO_CLASS_NAME_ FileFD

class _MACRO_CLASS_NAME_ {

    ////////////////////////
    // Definitions BEGIN: //
    ////////////////////////
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    //////////////////////
    // Definitions END. //
    //////////////////////

    //////////////////////
    // Data part BEGIN: //
    //////////////////////
  private:
    int         fd;
    int         filemode;
    size_t      maplength;
    int         mmapprot;
    int         mapflags;
    struct stat abtme;
    void       *mapped;
    std::string filename;
    ////////////////////
    // Data part END. //
    ////////////////////

    //////////////////////
    // File Mode BEGIN: //
    //////////////////////
  private:
    inline void reset_map() {
        mapped    = MAP_FAILED;
        maplength = 0;
    }

    inline void pre_init() {
        fd       = -1;
        filemode = -1;
        reset_map();
    }

  public:
    inline bool is_mapped() const {
        bool const cond1 = (maplength > 0);
        bool const cond2 = (mmapprot != PROT_NONE);
        bool const cond3 = (mapped != MAP_FAILED);
        bool const cond4 = (mapflags >= 0);
        bool const cond5 = (fd >= 0);
        bool const ret   = cond1 && cond2 && cond3 && cond4 && cond5;
        return ret;
    }

    inline bool mappable() const {
        bool const cond1 = (maplength == 0);
        bool const cond2 = (mmapprot != PROT_NONE);
        bool const cond3 = (mapped == MAP_FAILED);
        bool const cond4 = (mapflags >= 0);
        bool const cond5 = (fd >= 0);
        bool const ret   = cond1 && cond2 && cond3 && cond4 && cond5;
        return ret;
    }

    inline void construct() {
        pre_init();
        if (filename.length() == 0) { filename = std::string("./tmpfile"); }
        mmapprot = PROT_NONE;
        mapflags = MAP_SHARED;
    }

    inline void construct(std::string const Afile) {
        construct();
        filename = Afile;
    }

    inline int unmapfile() {
        int ret = 0;
        if (is_mapped()) {
            ret = munmap(reinterpret_cast<void *>(mapped),
                         reinterpret_cast<size_t>(maplength));

            maplength = 0;
            mapped    = MAP_FAILED;
        }
        return ret;
    }

    inline int closefile() {
        int ret = 0;
        if (fd >= 0) {
            unmapfile();
            ret = close(fd);
        }
        pre_init();
        return ret;
    }

    inline bool check_file_mode(int const newfilemode = -1) {
        bool const status_mode = (filemode == newfilemode);

        if (!status_mode) { closefile(); }
        if (fd < 0) {
            fd = open(static_cast<const char *>(filename.c_str()), newfilemode,
                      static_cast<mode_t>(0755));

            filemode = newfilemode;
        }

        return status_mode;
    }

    inline bool readfile() {
        bool const status = check_file_mode(static_cast<int>(O_RDONLY));
        if (mmapprot == PROT_NONE) { mmapprot = static_cast<int>(PROT_READ); }
        if (mapflags < 0) { mapflags = MAP_SHARED; }
        return status;
    }

    inline bool writefile() {
        bool const status =
          check_file_mode(static_cast<int>(O_WRONLY | O_CREAT | O_TRUNC));

        if (mmapprot == PROT_NONE) { mmapprot = static_cast<int>(PROT_WRITE); }
        if (mapflags < 0) { mapflags = MAP_SHARED; }
        return status;
    }

    inline bool appendfile() {
        bool status = check_file_mode(static_cast<int>(O_RDWR | O_CREAT));
        if (mmapprot == PROT_NONE) {
            mmapprot = static_cast<int>(PROT_READ | PROT_WRITE);
        }
        if (mapflags < 0) { mapflags = MAP_SHARED; }
        return status;
    }

    inline void destroy() { closefile(); }

    inline void reconstruct() {
        destroy();
        construct();
    }

    inline void reconstruct(std::string const Afile) {
        destroy();
        construct(Afile);
    }

    inline FileFD &operator()(std::string const name) {
        reconstruct(name);
        return (*this);
    }

    inline int truncatefile(off_t const length = 0) {
        if (fd >= 0) {
            return static_cast<int>(ftruncate(fd, length));
        } else {
            return -1;
        }
    }
    ////////////////////
    // File Mode END. //
    ////////////////////

    ////////////////////////////////////
    // Writeable map related BEGIN: { //
    ////////////////////////////////////
    inline bool set_map_prot(int const PROT) {
        if (mmapprot == PROT) {
            return true;
        } else {
            mmapprot = PROT;
            unmapfile();
            return false;
        }
    }

    inline bool set_map_read() { return set_map_prot(PROT_READ); }
    inline bool set_map_write() { return set_map_prot(PROT_WRITE); }

    inline bool set_map_append() {
        return set_map_prot(PROT_READ | PROT_WRITE);
    }
    //////////////////////////////////
    // Writeable map related END. } //
    //////////////////////////////////

    //////////////////////////////////
    // Sharing map related BEGIN: { //
    //////////////////////////////////
    inline bool check_mapflags(int const MAPFLAGS) {
        if (mapflags == MAPFLAGS) {
            return true;
        } else {
            mapflags = MAPFLAGS;
            unmapfile();
            return false;
        }
    }

    inline bool set_map_shared() { return check_mapflags(MAP_SHARED); }
    inline bool set_map_private() { return check_mapflags(MAP_PRIVATE); }
    ////////////////////////////////
    // Sharing map related END. } //
    ////////////////////////////////

    /////////////////
    // Seek BEGIN: //
    /////////////////
  public:
    inline off_t seekfile(off_t const offset, int const whence) {
        return static_cast<off_t>(lseek(fd, offset, whence)); //
    }

    inline off_t seekfile_cur(off_t const offset = 0) {
        return static_cast<off_t>(lseek(fd, offset, SEEK_CUR)); //
    }

    inline off_t seekfile_set(off_t const offset = 0) {
        return static_cast<off_t>(lseek(fd, offset, SEEK_SET)); //
    }

    inline off_t seekfile_end(off_t const offset = 0) {
        return static_cast<off_t>(lseek(fd, offset, SEEK_END)); //
    }

    inline off_t operator()(off_t const offset = 0) {
        return seekfile_cur(offset);
    }

    inline FileFD &operator[](off_t const pos) {
        seekfile(static_cast<off_t>(pos), SEEK_SET);
        return (*this);
    }
    ///////////////
    // Seek END. //
    ///////////////

    /////////////////
    // Info BEGIN: //
    /////////////////
  private:
    inline int info() {
        return static_cast<int>(fstat(fd, static_cast<struct stat *>(&abtme)));
    }

  public:
    inline off_t sizefile() {
        info();
        return static_cast<off_t>(abtme.st_size);
    }

    inline std::string const &getfilename() const { return filename; }

    inline int getfd() const { return fd; }
    ///////////////
    // Info END. //
    ///////////////

    ///////////////////////////
    // Memory Mapping BEGIN: //
    ///////////////////////////
  public:
    inline void *mapfile(size_t const length, off_t const offset = 0) {
        if (is_mapped()) { unmapfile(); }

        if (mappable()) {
            off_t const total_len = length + offset;
            if (sizefile() < total_len) { truncatefile(total_len); }

            mapped = reinterpret_cast<void *>(mmap(
              /*void *addr=*/reinterpret_cast<void *>(NULL),
              /*size_t length=*/length,
              /*int prot=*/mmapprot,
              /*int flags=*/mapflags,
              /*int fd=*/fd,
              /*off_t offset=*/offset));

            if (mapped != MAP_FAILED) {
                maplength = length;
            } else {
                printf("MMAP FAILED 1 !!! %s\n", &(filename[0]));
            }
        } else {
            printf("MMAP FAILED 2 !!!\n");
        }
        return mapped;
    }

    inline size_t getmaplength() const { return maplength; }
    /////////////////////////
    // Memory Mapping END. //
    /////////////////////////

    /////////////////////////
    // Read & Write BEGIN: //
    /////////////////////////
  public:
    inline ssize_t read2file(void *buf = NULL, size_t const count = 0) {
        return static_cast<ssize_t>(read(fd, buf, count)); //
    }

    inline ssize_t write2file(const void *buf = NULL, size_t const count = 0) {
        return static_cast<ssize_t>(write(fd, buf, count)); //
    }

    template <typename T>
    inline ssize_t multiread2file(T &buf, size_t const count = 1) {
        return read2file(reinterpret_cast<void *>(&buf),
                         static_cast<size_t>(sizeof(T) * count));
    }

    template <typename T>
    inline ssize_t multiwrite2file(const T &buf, size_t count = 1) {
        return write2file(reinterpret_cast<const void *>(&buf),
                          static_cast<size_t>(sizeof(T) * count)); //
    }

    template <typename T>
    inline ssize_t WriteVector(std::vector<T> const &out) {
        size_t const count       = out.size();
        ssize_t      writtensize = multiwrite2file(count);
        writtensize              = writtensize + multiwrite2file(out[0], count);
        return writtensize;
    }

    template <typename T> inline ssize_t ReadVector(std::vector<T> &out) {
        size_t       count       = 0;
        size_t const oldsize     = out.size();
        ssize_t      writtensize = multiread2file(count);
        out.resize(oldsize + count);
        writtensize = multiread2file(out[oldsize], count);
        return writtensize;
    }

    inline ssize_t WriteString(std::string const &out) {
        size_t const count       = out.size();
        ssize_t      writtensize = multiwrite2file(count);
        writtensize              = writtensize + multiwrite2file(out[0], count);
        return writtensize;
    }

    inline ssize_t ReadString(std::string &out) {
        size_t       count       = 0;
        size_t const oldsize     = out.size();
        ssize_t      writtensize = multiread2file(count);
        out.resize(oldsize + count);
        writtensize = multiread2file(out[oldsize], count);
        return writtensize;
    }

    inline ssize_t WriteStrings(std::vector<std::string> const &out) {
        size_t const count       = out.size();
        ssize_t      writtensize = multiwrite2file(count);
        for (size_t i = 0; i < count; i++) {
            writtensize += WriteString(out[i]);
        }
        return writtensize;
    }

    inline ssize_t ReadStrings(std::vector<std::string> &out) {
        out.clear();
        size_t  count       = 0;
        ssize_t writtensize = multiread2file(count);
        out.resize(count);
        for (size_t i = 0; i < count; i++) {
            writtensize += ReadString(out[i]);
        }
        return writtensize;
    }
    ///////////////////////
    // Read & Write END. //
    ///////////////////////

    ////////////////////////////////
    // Convinence Wrappers BEGIN: //
    ////////////////////////////////
  public:
    inline ssize_t operator>>(char &out) { return multiread2file(out); }
    inline ssize_t operator<<(char const out) { return multiwrite2file(out); }
    inline ssize_t operator>>(int &out) { return multiread2file(out); }
    inline ssize_t operator<<(int const out) { return multiwrite2file(out); }
    inline ssize_t operator>>(float &out) { return multiread2file(out); }
    inline ssize_t operator<<(float const out) { return multiwrite2file(out); }
    inline ssize_t operator>>(double &out) { return multiread2file(out); }
    inline ssize_t operator<<(double const out) { return multiwrite2file(out); }
    inline ssize_t operator>>(long &out) { return multiread2file(out); }
    inline ssize_t operator<<(long const out) { return multiwrite2file(out); }
    inline ssize_t operator>>(size_t &out) { return multiread2file(out); }
    inline ssize_t operator<<(size_t const out) { return multiwrite2file(out); }
    inline ssize_t operator>>(std::string &out) { return ReadString(out); }
    inline ssize_t operator<<(std::string const out) {
        return WriteString(out);
    }
    template <typename T> inline ssize_t operator<<(std::vector<T> const &out) {
        return WriteVector(out);
    }
    template <typename T> inline ssize_t operator>>(std::vector<T> &out) {
        return ReadVector(out);
    }
    //////////////////////////////
    // Convinence Wrappers END. //
    //////////////////////////////

    /////////////////////////////////////
    // Constructor & Destructor BEGIN: //
    /////////////////////////////////////
  public:
    _MACRO_CLASS_NAME_(TYPE_SELF &&in) : filename(std::move(in.filename)) {
        fd        = in.fd;
        abtme     = in.abtme;
        filemode  = in.filemode;
        mapped    = in.mapped;
        maplength = in.maplength;
        mmapprot  = in.mmapprot;
        in.pre_init();
    }

    _MACRO_CLASS_NAME_(TYPE_SELF const &in) : filename(in.filename) {}

    _MACRO_CLASS_NAME_(const char *Afile) { construct(Afile); }
    _MACRO_CLASS_NAME_() { construct(); }
    _MACRO_CLASS_NAME_(std::string Afile) { construct(Afile); }
    ~_MACRO_CLASS_NAME_() { destroy(); }
    ///////////////////////////////////
    // Constructor & Destructor END. //
    ///////////////////////////////////
};

#undef _MACRO_CLASS_NAME_

#endif
