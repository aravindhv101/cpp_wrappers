﻿#ifndef _HEADER_GUARD_CPPFileIO_FileArray_
#define _HEADER_GUARD_CPPFileIO_FileArray_

////////////////////
// Headers BEGIN: //
////////////////////
#include "../Headers.hh"
#include "./Basic.hh"
#include "./FileFD.hh"
#include "./D1.hh"
//////////////////
// Headers END. //
//////////////////

#define _MACRO_CLASS_NAME_ FileArray
template <typename T> class _MACRO_CLASS_NAME_ {
    ////////////////////////
    // Definitions BEGIN: //
    ////////////////////////
  public:
    using TYPE_ELEMENT = T;
    using TYPE_SELF    = _MACRO_CLASS_NAME_<TYPE_ELEMENT>;

  private:
    static inline size_t GET_SIZE_PAGE() { return getpagesize(); }

    static inline size_t constexpr GET_SIZE_ELEMENT() {
        return sizeof(TYPE_ELEMENT);
    }

    static inline size_t GET_SIZE_LCM() {
        return LCM(GET_SIZE_PAGE(), GET_SIZE_ELEMENT());
    }

    static inline size_t GET_NUM_BATCH() {
        return GET_SIZE_LCM() / GET_SIZE_ELEMENT();
    }
    //////////////////////
    // Definitions END. //
    //////////////////////

    //////////////////////////
    // Data Elements BEGIN: //
    //////////////////////////
  private:
    TYPE_ELEMENT *mainptr;
    std::string   filename;
    FileFD        filefd;
    size_t        offset;
    size_t        begin, end, length;
    size_t        act_begin, act_end, act_length;
    size_t        SIZES[4];
    ////////////////////////
    // Data Elements END. //
    ////////////////////////

    //////////////////////
    // File Mode BEGIN: //
    //////////////////////
  public:
    inline void Reset() {
        begin      = 0;
        act_begin  = 0;
        end        = 0;
        act_end    = 0;
        length     = 0;
        act_length = 0;
    }

    inline void construct(std::string const Afilename = std::string("outfile"),
                          size_t const      Aoffset   = 0) {

        filename = Afilename;
        offset   = Aoffset;
        Reset();
        filefd(filename).readfile();
    }

    inline void destroy() {
        Reset();
        filefd.destroy();
    }

    inline void
    reconstruct(std::string const Afilename = std::string("outfile"),
                size_t const      Aoffset   = 0) {

        destroy();
        construct(Afilename, Aoffset);
    }

    inline void writeable(bool const arg = true) {
        if (arg) {
            Reset();
            filefd(filename).appendfile();
        } else {
            Reset();
            filefd(filename).readfile();
        }
    }

    inline void set_file_readonly() { writeable(false); }
    inline void set_file_writeable() { writeable(true); }

    inline bool set_map_shared() {
        Reset();
        return filefd.set_map_shared();
    }
    inline bool set_map_private() {
        Reset();
        return filefd.set_map_private();
    }
    inline bool set_map_read() {
        Reset();
        return filefd.set_map_read();
    }
    inline bool set_map_write() {
        Reset();
        return filefd.set_map_append();
    }
    ////////////////////
    // File Mode END. //
    ////////////////////

    //////////////////////////////////
    // Main Mapping Function BEGIN: //
    //////////////////////////////////
  public:
    inline void map(size_t const t_begin = 0, size_t const t_length = 1) {
        size_t const t_end = t_begin + t_length;
        if ((t_begin < begin) || (t_end > end)) {
            /* Match to sector sizes: */ {
                begin = static_cast<size_t>(static_cast<double>(t_begin) /
                                            static_cast<double>(SIZES[3])) *
                        SIZES[3];

                end = static_cast<size_t>(
                        1 + (static_cast<double>(t_begin + t_length) /
                             static_cast<double>(SIZES[3]))) *
                      SIZES[3];

                length = end - begin;
            }
            /* Reinitiate map: */ {
                filefd.unmapfile();
                mainptr = reinterpret_cast<TYPE_ELEMENT *>(
                  filefd.mapfile((length * SIZES[1]),
                                 (begin * SIZES[1]) + (offset * SIZES[0])));
            }
        }
    }
    ////////////////////////////////
    // Main Mapping Function END. //
    ////////////////////////////////

    ///////////////////////
    // File Sizes BEGIN: //
    ///////////////////////
  public:
    inline off_t filesize() { return filefd.sizefile(); }

    inline off_t size() {
        return filefd.sizefile() / SIZES[1]; //
    }

    inline off_t size(long const num) {
        filefd.unmapfile();
        filefd.truncatefile(num * SIZES[1]);
        return size();
    }
    /////////////////////
    // File Sizes END. //
    /////////////////////

    ///////////////////////
    // Convinence BEGIN: //
    ///////////////////////
  public:
    inline TYPE_ELEMENT &operator()(size_t const A_begin,
                                    size_t const A_length = 1) {
        map(A_begin, A_length);
        TYPE_ELEMENT &ret = mainptr[A_begin - begin];
        return ret;
    }

    template <typename TTI>
    inline void operator()(size_t const                       A_begin,
                           Dynamic1DArray<TYPE_ELEMENT, TTI> &in) {

        TTI        A_length = in();
        auto const filesize = size();
        TTI const  A_end    = mymin(static_cast<TTI>(A_begin + A_length),
                                    static_cast<TTI>(filesize));
        A_length            = A_end - A_begin;
        TYPE_ELEMENT *const       dest = in.GET_DATA();
        TYPE_ELEMENT const *const src  = &(this[0](A_begin, A_length));
        memcpy(dest, src, A_length * sizeof(TYPE_ELEMENT));
    }

    inline off_t operator()() { return size(); }

    inline FileArray &operator()(std::string const Afilename,
                                 size_t const      Aoffset = 0) {
        reconstruct(Afilename, Aoffset);
        return (*this);
    }
    /////////////////////
    // Convinence END. //
    /////////////////////

    /////////////////////////////////////
    // Constructor & Destructor BEGIN: //
    /////////////////////////////////////
  public:
    _MACRO_CLASS_NAME_(std::string const Afilename = std::string("outfile"),
                       size_t const      Aoffset   = 0) {
        SIZES[0] = GET_SIZE_PAGE();
        SIZES[1] = GET_SIZE_ELEMENT();
        SIZES[2] = GET_SIZE_LCM();
        SIZES[3] = GET_NUM_BATCH();
        construct(Afilename, Aoffset);
    }

    _MACRO_CLASS_NAME_(TYPE_SELF const &other)
      : _MACRO_CLASS_NAME_(other.filename, other.offset) {}

    ~_MACRO_CLASS_NAME_() { destroy(); }
    ///////////////////////////////////
    // Constructor & Destructor END. //
    ///////////////////////////////////
};
#undef _MACRO_CLASS_NAME_

#endif
