#ifndef _HEADER_GUARD_CPPFileIO_dir_find_hh_
#define _HEADER_GUARD_CPPFileIO_dir_find_hh_

#include "./redirect_fd.hh"
#include "./ExternalPrograms.hh"
#include "./D1.hh"
#include "./FullFileReader.hh"
#include "./MyStr.hh"
#include "./FileLines.hh"

namespace find {

using TYPE_REDIRECTOR = redirect_fd;

inline std::string NAME_FILE_TXT_FIND() {
    pid_t const pid = getpid();
    std::string ret = std::to_string(pid);
    ret = std::string("/tmp/find_buffer-") + ret + std::string(".txt");
    return ret;
}

inline tmp_file_lines<8> all(std::string const loc) {
    std::string     filename = NAME_FILE_TXT_FIND();
    TYPE_REDIRECTOR redirector(filename, 1);
    { ExternalStarter<true>::GET("/usr/bin/find")(loc); }
    tmp_file_lines<8> ret(filename, '\n');
    return ret;
}

inline tmp_file_lines<8> file(std::string const loc) {
    std::string     filename = NAME_FILE_TXT_FIND();
    TYPE_REDIRECTOR redirector(filename, 1);
    { ExternalStarter<true>::GET("/usr/bin/find")(loc)("-type")("f"); }
    tmp_file_lines<8> ret(filename, '\n');
    unlink(filename.c_str());
    return ret;
}

inline tmp_file_lines<8> link(std::string const loc) {
    std::string     filename = NAME_FILE_TXT_FIND();
    TYPE_REDIRECTOR redirector(filename, 1);
    { ExternalStarter<true>::GET("/usr/bin/find")(loc)("-type")("l"); }
    tmp_file_lines<8> ret(filename, '\n');
    unlink(filename.c_str());
    return ret;
}

inline tmp_file_lines<8> dir(std::string const loc) {
    std::string     filename = NAME_FILE_TXT_FIND();
    TYPE_REDIRECTOR redirector(filename, 1);
    { ExternalStarter<true>::GET("/usr/bin/find")(loc)("-type")("d"); }
    tmp_file_lines<8> ret(filename, '\n');
    unlink(filename.c_str());
    return ret;
}

} // namespace find

#endif
