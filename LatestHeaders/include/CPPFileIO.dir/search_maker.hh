#ifndef _HEADER_GUARD_CPPFileIO_dir_search_maker_hh_
#define _HEADER_GUARD_CPPFileIO_dir_search_maker_hh_

#include "./Basic.hh"

inline void make_searcher_numeric(FILE *f, std::string const in) {
    size_t const                limit = in.size() - 1;
    std::map<TYPE_BYTE, size_t> dict;
    for (size_t i = 0; i <= limit; i++) {
        size_t const index = limit - i;
        dict[in[i]]        = index;
    }
    fprintf(f, "#define _MACRO_CLASS_NAME_ word_%s\n", in.c_str());
    fprintf(f, "class _MACRO_CLASS_NAME_ {\n");
    fprintf(f, "  public:\n");
    fprintf(f, "    using TYPE_SELF = _MACRO_CLASS_NAME_;\n");
    fprintf(f, "    using TYPE_INT  = size_t;\n");
    fprintf(f, "    using CHAR      = CPPFileIO::TYPE_BYTE;\n");
    fprintf(f, "\n");
    fprintf(f, "  public:\n");
    fprintf(f, "    static inline TYPE_INT constexpr SIZE() { return %zu; }\n",
            in.size());
    fprintf(f, "\n");
    fprintf(f, "    static inline CHAR constexpr GET_CHAR(size_t const i) {\n");
    fprintf(f, "        switch (i) {\n");
    for (size_t i = 0; i <= limit; i++) {
        int const val = in[i];
        fprintf(f, "            case %zu: return %d; // %c\n", limit - i, val,
                in[i]);
    }
    fprintf(f, "            default: return 0;\n");
    fprintf(f, "        }\n");
    fprintf(f, "    }\n");
    fprintf(f, "\n");
    fprintf(f,
            "    static inline TYPE_INT constexpr GET_JUMP(CHAR const in) {\n");
    fprintf(f, "        switch (in) {\n");
    for (auto const &i : dict) {
        int const val = i.first;
        fprintf(f, "            case %d: return %zu; // %c\n", val, i.second,
                i.first);
    }
    fprintf(f, "            default: return SIZE();\n");
    fprintf(f, "        }\n");
    fprintf(f, "    }\n");
    fprintf(f, "};\n");
    fprintf(f, "#undef _MACRO_CLASS_NAME_\n");
}

inline void make_searcher_char(FILE *f, std::string const in) {
    size_t const                limit = in.size() - 1;
    std::map<TYPE_BYTE, size_t> dict;
    for (size_t i = 0; i <= limit; i++) {
        size_t const index = limit - i;
        dict[in[i]]        = index;
    }
    fprintf(f, "#define _MACRO_CLASS_NAME_ word_%s\n", in.c_str());
    fprintf(f, "class _MACRO_CLASS_NAME_ {\n");
    fprintf(f, "  public:\n");
    fprintf(f, "    using TYPE_SELF = _MACRO_CLASS_NAME_;\n");
    fprintf(f, "    using TYPE_INT  = size_t;\n");
    fprintf(f, "    using CHAR      = CPPFileIO::TYPE_BYTE;\n");
    fprintf(f, "\n");
    fprintf(f, "  public:\n");
    fprintf(f, "    static inline TYPE_INT constexpr SIZE() { return %zu; }\n",
            in.size());
    fprintf(f, "\n");
    fprintf(f, "    static inline CHAR constexpr GET_CHAR(size_t const i) {\n");
    fprintf(f, "        switch (i) {\n");
    for (size_t i = 0; i <= limit; i++) {
        switch (in[i]) {
            case '\\':
            case '\'':
                fprintf(f, "            case %zu: return '\\%c';\n", limit - i,
                        in[i]);
                break;

            default:
                fprintf(f, "            case %zu: return '%c';\n", limit - i,
                        in[i]);
                break;
        }
    }
    fprintf(f, "            default: return 0;\n");
    fprintf(f, "        }\n");
    fprintf(f, "    }\n");
    fprintf(f, "\n");
    fprintf(f,
            "    static inline TYPE_INT constexpr GET_JUMP(CHAR const in) {\n");
    fprintf(f, "        switch (in) {\n");
    for (auto const &i : dict) {
        switch (i.first) {
            case '\\':
            case '\'':
                fprintf(f, "            case '\\%c': return %zu;\n", i.first,
                        i.second);
                break;

            default:
                fprintf(f, "            case '%c': return %zu;\n", i.first,
                        i.second);
                break;
        }
    }
    fprintf(f, "            default: return SIZE();\n");
    fprintf(f, "        }\n");
    fprintf(f, "    }\n");
    fprintf(f, "};\n");
    fprintf(f, "#undef _MACRO_CLASS_NAME_\n");
}

inline void make_searcher(FILE *f, std::string const in) {
    make_searcher_numeric(f, in);
}

inline void make_searcher(std::string const file, std::string const in) {
    FILE *f = fopen(file.c_str(), "w");
    make_searcher(f, in);
    fclose(f);
}

inline void make_searcher(std::string const in) { make_searcher(stdout, in); }
#endif
