#ifndef _HEADER_GUARD_CPPFileIO_FileSplitter_
#define _HEADER_GUARD_CPPFileIO_FileSplitter_

////////////////////
// Headers BEGIN: //
////////////////////
#include "./FileWriter.hh"
#include "./Atomic_Counter.hh"
//////////////////
// Headers END. //
//////////////////

#define _MACRO_CLASS_NAME_ split_file
template <typename T> class _MACRO_CLASS_NAME_ {

    //////////////////////////
    // Definitions BEGIN: { //
    //////////////////////////
  public:
    using TYPE_SELF    = _MACRO_CLASS_NAME_;
    using TYPE_STRINGS = std::vector<std::string>;
    using TYPE_ELEMENT = T;
    using ATOMIC_INT   = Atomic_Counter<size_t>;
    ////////////////////////
    // Definitions END. } //
    ////////////////////////

    ////////////////////////////
    // Data Elements BEGIN: { //
    ////////////////////////////
  private:
    std::string const   NAME_FILE_BIN_INPUT;
    TYPE_STRINGS const &NAME_FILE_BIN_OUTPUT;
    size_t const        NTH;
    size_t const        LIMIT;
    ATOMIC_INT          COUNTER;
    std::thread *       THREADS;
    //////////////////////////
    // Data Elements END. } //
    //////////////////////////

    /////////////////////////////////////
    // Main working functions BEGIN: { //
    /////////////////////////////////////
  private:
    static inline size_t SIZE_FILE(std::string const &in) {
        FileArray<TYPE_ELEMENT> reader(in);
        size_t const            ret = reader();
        return ret;
    }

  private:
    inline size_t SIZE() const { return NAME_FILE_BIN_OUTPUT.size(); }

    inline void DO_SPLIT(size_t const index) {
        FileArray<T const> reader(NAME_FILE_BIN_INPUT);
        FileWriter<T>      writer(NAME_FILE_BIN_OUTPUT[index]);
        size_t const       begin = (LIMIT * index) / SIZE();
        size_t const       end   = (LIMIT * (index + 1)) / SIZE(); //
        for (size_t j = begin; j < end; j++) { writer(reader(j)); }
    }

    inline void DO_SPLIT() {
        for (size_t i = (COUNTER++); i < SIZE(); i = (COUNTER++)) {
            DO_SPLIT(i);
        }
    }

  private:
    inline void DO_SPLIT_THREADED() {
        COUNTER = 0;
        for (size_t i = 0; i < NTH; i++) {
            THREADS[i] = std::thread(thread_slave<TYPE_SELF>, this);
        }
        for (size_t i = 0; i < NTH; i++) { THREADS[i].join(); }
    }
    ///////////////////////////////////
    // Main working functions END. } //
    ///////////////////////////////////

    //////////////////////////////////////
    // Interface for threading BEGIN: { //
    //////////////////////////////////////
  public:
    inline void operator()() { DO_SPLIT(); }
    ////////////////////////////////////
    // Interface for threading END. } //
    ////////////////////////////////////

    ///////////////////////////////////////
    // Constructor & Destructor BEGIN: { //
    ///////////////////////////////////////
  private:
    _MACRO_CLASS_NAME_(std::string const   name_file_bin_input,
                       TYPE_STRINGS const &name_file_bin_output,
                       size_t const        nth)
      : NAME_FILE_BIN_INPUT(name_file_bin_input),
        NAME_FILE_BIN_OUTPUT(name_file_bin_output), NTH(nth),
        LIMIT(SIZE_FILE(NAME_FILE_BIN_INPUT)), THREADS(new std::thread[NTH]) {}

    ~_MACRO_CLASS_NAME_() { delete[] THREADS; }
    /////////////////////////////////////
    // Constructor & Destructor END. } //
    /////////////////////////////////////

    /////////////////////////////////////
    // Main exposed interface BEGIN: { //
    /////////////////////////////////////
  public:
    static inline void do_split(std::string const   name_file_bin_input,
                                TYPE_STRINGS const &name_file_bin_output,
                                size_t const        nth) {
        TYPE_SELF slave(name_file_bin_input, name_file_bin_output, nth);
        slave.DO_SPLIT_THREADED();
    }
    ///////////////////////////////////
    // Main exposed interface END. } //
    ///////////////////////////////////
};

template <typename T>
inline void SplitFile(std::string const               infilename,
                      std::vector<std::string> const &outnames,
                      size_t const                    NTH = 64) {
    _MACRO_CLASS_NAME_<T>::do_split(infilename, outnames, NTH);
}

#undef _MACRO_CLASS_NAME_

template <typename T>
inline void SplitFile_old(std::string const               infilename,
                          std::vector<std::string> const &outnames) {

    FileArray<T const> reader(infilename);
    size_t const       limit = reader.size();
    for (size_t i = 0; i < outnames.size(); i++) {
        FileWriter<T> writer(outnames[i]);
        size_t const  begin = (limit * i) / outnames.size();       //
        size_t const  end   = (limit * (i + 1)) / outnames.size(); //
        for (size_t j = begin; j < end; j++) { writer(reader(j)); }
    }
}

#endif
