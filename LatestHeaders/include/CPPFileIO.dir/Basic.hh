﻿#ifndef _HEADER_GUARD_CPPFileIO_Basic_
#define _HEADER_GUARD_CPPFileIO_Basic_

////////////////////
// Headers BEGIN: //
////////////////////
#include "../Headers.hh"
#include "../TEMPLATES.hh"
//////////////////
// Headers END. //
//////////////////

using TYPE_U8   = TEMPLATES::TYPE::uint1;
using TYPE_U16  = TEMPLATES::TYPE::uint2;
using TYPE_U32  = TEMPLATES::TYPE::uint4;
using TYPE_U64  = TEMPLATES::TYPE::uint8;
using TYPE_I8   = TEMPLATES::TYPE::int1;
using TYPE_I16  = TEMPLATES::TYPE::int2;
using TYPE_I32  = TEMPLATES::TYPE::int4;
using TYPE_I64  = TEMPLATES::TYPE::int8;
using TYPE_BYTE = TYPE_U8;

inline TYPE_U8 constexpr MAX_U8() { return TEMPLATES::TYPE::max_uint1; }
inline TYPE_U16 constexpr MAX_U16() { return TEMPLATES::TYPE::max_uint2; }
inline TYPE_U32 constexpr MAX_U32() { return TEMPLATES::TYPE::max_uint4; }
inline TYPE_U64 constexpr MAX_U64() { return TEMPLATES::TYPE::max_uint8; }

inline TYPE_I8 constexpr MAX_I8() { return TEMPLATES::TYPE::max_int1; }
inline TYPE_I16 constexpr MAX_I16() { return TEMPLATES::TYPE::max_int2; }
inline TYPE_I32 constexpr MAX_I32() { return TEMPLATES::TYPE::max_int4; }
inline TYPE_I64 constexpr MAX_I64() { return TEMPLATES::TYPE::max_int8; }

inline TYPE_BYTE constexpr NTHREADS() { return 4; }

template <typename T> inline void thread_slave(T *in) { in[0](); }

template <typename T> inline void thread_slave_void(void *in) {
    T *out = reinterpret_cast<T *>(in);
    out[0]();
}

inline std::string &clean_trailing(std::string &in, char const sep) {
    for (size_t i = in.size() - 1; i < in.size(); i--) {
        if (in[i] != sep) {
            in.resize(i + 1);
            return in;
        }
    }
    return in;
}

inline std::string &dirname(std::string &in, char const sep = '/') {
    clean_trailing(in, sep);

    size_t const      limit    = in.size();
    char const *const start    = &(in[0]);
    char const *const stop     = &(in[limit - 1]);
    char const       *current  = start;
    char const       *previous = start;

    while ((start <= current) && (current < stop)) {
        previous = current;
        current  = static_cast<char const *>(memchr(
          /* const void *s = */ static_cast<void const *>(current + 1),
          /* int c = */ sep, /* size_t n = */ stop - current));
    }

    in.resize(previous - start);

    return clean_trailing(in, sep);
}

inline std::string &basename(std::string &in, char const sep = '/') {
    clean_trailing(in, sep);

    size_t const      limit    = in.size();
    char const *const start    = &(in[0]);
    char const *const stop     = &(in[limit - 1]);
    char const       *current  = start;
    char const       *previous = start;

    while ((start <= current) && (current <= stop)) {
        previous      = current;
        size_t length = stop - current;
        if (length > 0) {
            current = static_cast<char const *>(memchr(
              /* const void *s = */ static_cast<void const *>(current + 1),
              /* int c = */ sep, /* size_t n = */ length));

            current++;
        }
    }

    in = std::string(previous);

    return clean_trailing(in, sep);
}

template <typename TypeInt>
inline TypeInt constexpr get_bit(TYPE_BYTE const i) {
    TypeInt ret = (1 << i);
    return ret;
}

template <typename TypeInt>
inline TypeInt constexpr get_bit(TypeInt const num, TYPE_BYTE const i) {
    return ((num >> i) & 1);
}

TYPE_BYTE  junk_address = 0;
const bool DEBUG        = false;

template <typename T> inline T *get_junk() {
    return reinterpret_cast<T *>(&junk_address);
}

template <typename T> inline T *get_junk(T *) { return get_junk<T>(); }

template <typename T> inline void set_junked(T *&inptr) {
    inptr = get_junk<T>();
}

template <typename T> inline bool is_junked(T *inptr) {
    return (inptr == get_junk<T>());
}

template <typename T> inline bool safe_delete(T *&inptr) {
    if (!is_junked(inptr)) {
        delete inptr;
        set_junked(inptr);
        return true;
    } else {
        return false;
    }
}

template <typename T> inline bool safe_delete_array(T *&inptr) {
    if (!is_junked(inptr)) {
        delete[] inptr;
        set_junked(inptr);
        return true;
    } else {
        return false;
    }
}

inline size_t constexpr shifter(size_t in) { return (1 << in); }

inline bool checkbit(size_t const inbits, size_t const checkbits) {
    return ((inbits & checkbits) == checkbits); //
}

template <typename T> inline T mymod(T const a) { return ((a < 0) ? -a : a); }
template <typename T> inline T mysign(T const a) { return ((a < 0) ? -1 : 1); }
template <typename T> inline T mymax(T const a, T const b) {
    return ((a > b) ? a : b);
}
template <typename T> inline T mymin(T const a, T const b) {
    return ((a < b) ? a : b);
}

template <typename T> inline T clamp(T const a, T const b, T const c) {
    return ((a <= b) ? ((b <= c) ? b : c) : b);
}

template <typename T> inline void myswap(T &a, T &b) {
    T tmp = b;
    b     = a;
    a     = tmp;
}

template <typename T> inline T constexpr GCD(T min, T max) {
    T tmp;
    if (min > max) { myswap(min, max); }
    while (min != 0) {
        tmp = min;
        min = max % min;
        max = tmp;
    }
    return max;
}

template <typename T> inline T constexpr LCM(T const a, T const b) {
    T ret = GCD(a, b);
    ret   = a * (b / ret);
    return ret;
}

inline char to_lower(unsigned char const in) {
    return (('A' <= in) && (in <= 'Z')) ? (in | 32) : in;
}

inline char to_upper(unsigned char const in) {
    return (('a' <= in) && (in <= 'z')) ? (in ^ 32) : in;
    // return in - ((('a' <= in) && (in <= 'z')) * ('z' - 'Z'));
}

inline void to_lower(char *begin, char *end) {
    for (char *i = begin; i < end; i++) { *i = to_lower(*i); }
}

inline void to_upper(char *begin, char *end) {
    for (char *i = begin; i < end; i++) { *i = to_upper(*i); }
}

inline void to_lower(std::string &in) {
    for (char &i : in) { i = to_lower(i); }
}

inline void to_upper(std::string &in) {
    for (char &i : in) { i = to_upper(i); }
}

inline void starter_self(std::vector<std::string> &program) {
    std::vector<char *> arrs;
    size_t const        j = program.size();
    if (j > 0) {
        arrs.resize(j + 1);
        for (size_t i = 0; i < j; i++) { arrs[i] = &(program[i][0]); }
        arrs[j] = static_cast<char *>(NULL);
        execvp(static_cast<char const *>(arrs[0]), &(arrs[0]));
    }
}

inline pid_t starter_fork(std::vector<std::string> &program) {
    if (program.size() > 0) {
        pid_t const tmp_pid = fork();
        if (tmp_pid == 0) { starter_self(program); }
        return tmp_pid;
    } else {
        return 0;
    }
}

inline void waitonall(std::vector<pid_t> &childs) {
    for (size_t i = 0; i < childs.size(); i++) {
        waitpid(childs[i], static_cast<int *>(NULL), static_cast<int>(0));
    }
}

inline std::string generate_header_guard_name(std::string in) {
    in = std::string("_") + in + "_";
    std::string ret;
    char        current = 0, previous = 0;

    for (size_t i = 0; i < in.size(); ++i) {
        previous = current;
        current  = in[i];

        switch (current) {
            case '.':
            case '/': current = '_';
            case '_':
                if (current == previous) { goto EndOfSwitch; }

            default: ret.push_back(current); goto EndOfSwitch;
        }
    EndOfSwitch:;
    }

    ret = "_HEADER_GUARD" + ret;

    return ret;
}

#define _MACRO_CLASS_NAME_ get_args
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;

  private:
    std::vector<std::string> args;

  private:
    inline void INIT(int const argc, char const **argv) {
        args.resize(argc);
        for (int i = 0; i < argc; i++) { args[i] = argv[i]; }
    }

  public:
    inline std::string       &operator()(size_t i) { return args[i]; }
    inline std::string const &operator()(size_t i) const { return args[i]; }
    inline size_t             operator()() const { return args.size(); }
    inline void               operator()(int const argc, char const **argv) {
        INIT(argc, argv);
    }

  public:
    _MACRO_CLASS_NAME_(int const argc, char const **argv) { INIT(argc, argv); }
};
#undef _MACRO_CLASS_NAME_

inline void SetCPUAffinity(int const cpunum) {
    cpu_set_t set;
    CPU_ZERO(&set);
    CPU_SET(cpunum, &set);
    if (sched_setaffinity(getpid(), sizeof(set), &set) == -1) {
        printf(" Error setting affinity...\n ");
    }
}

#endif
