#ifndef _HEADER_GUARD_CPPFileIO_dir_find_executables_hh_
#define _HEADER_GUARD_CPPFileIO_dir_find_executables_hh_

#include "./find_stand_alone.hh"

#define _MACRO_CLASS_NAME_ find_executable
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;

  private:
    std::vector<std::string> PATH;

  public:
    inline void add_path(std::string const name) { PATH.push_back(name); }

    inline std::string find_exe(std::string const name_file_exe) {
        find_stand_alone finder;
        for (size_t j = 0; j < PATH.size(); ++j) {
            auto const &res = finder.all(PATH[j]);
            for (size_t i = 0; i < res.size(); ++i) {
                if (match_end(res[i], name_file_exe)) { return res[i]; }
            }
        }
        return "";
    }

  public:
    inline std::string operator()(std::string const name) {
        return find_exe(name);
    }

  public:
    _MACRO_CLASS_NAME_() {
        add_path("/usr/local/bin");
        add_path("/usr/bin");
        add_path("/bin");
    }

    ~_MACRO_CLASS_NAME_() {}
};
#undef _MACRO_CLASS_NAME_

#endif
