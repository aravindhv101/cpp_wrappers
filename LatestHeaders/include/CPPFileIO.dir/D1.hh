﻿#ifndef _HEADER_GUARD_CPPFileIO_D1_
#define _HEADER_GUARD_CPPFileIO_D1_

////////////////////
// Headers BEGIN: //
////////////////////
#include "./Basic.hh"
//////////////////
// Headers END. //
//////////////////

#define _MACRO_CLASS_NAME_ dynamic_1d_array

template <class TD, TEMPLATES::TYPE::uint_smallest SIZE_INT =
                      sizeof(TEMPLATES::TYPE::int_biggest)>
class _MACRO_CLASS_NAME_ {

  public:
    using TYPE_INT  = TEMPLATES::TYPE::type_int<SIZE_INT>;
    using TYPE_UINT = TEMPLATES::TYPE::type_uint<SIZE_INT>;
    using TYPE_DATA = TD;
    using TYPE_SELF = _MACRO_CLASS_NAME_<TYPE_DATA, SIZE_INT>;

  private:
    static inline void *MMAP(size_t const length, int const prot,
                             int const flags, int const fd,
                             off_t const offset) {

        void *ret = mmap(/*void *addr =*/NULL, /*size_t length =*/length,
                         /*int prot =*/prot, /*int flags =*/flags,
                         /*int fd =*/fd, /*off_t offset =*/offset);

        return ret;
    }

    static inline void *MALLOC(size_t const size) {
        if (size > 0) {
            return malloc(/*size_t size =*/size);
        } else {
            return NULL;
        }
    }

    static inline TYPE_DATA *ALLOC(TYPE_UINT const size_data) {
        if (true) {
            return reinterpret_cast<TYPE_DATA *>(
              MALLOC(sizeof(TYPE_DATA) * size_t(size_data)));
        } else {
            return reinterpret_cast<TYPE_DATA *>(MMAP(
              /*size_t length =*/size_t(sizeof(TYPE_DATA) * size_data),
              /*int prot =*/int(PROT_READ | PROT_WRITE),
              /*int flags =*/int(MAP_PRIVATE | MAP_ANONYMOUS),
              /*int fd =*/int(-1), /*off_t offset =*/off_t(0)));
        }
    }

    static inline TYPE_DATA *NEW(TYPE_UINT const size_data) {
        TYPE_DATA *start = NULL;
        if (size_data > 0) {
            start           = ALLOC(size_data);
            TYPE_DATA *stop = start + size_data;
            for (TYPE_DATA *i = start; i < stop; i += 1) {
                new (i) TYPE_DATA();
            }
        }
        return start;
    }

  private:
    static inline int UNMAP(TYPE_DATA *const addr, TYPE_UINT const length) {
        if (length > 0) {
            return munmap(/*void *addr =*/reinterpret_cast<void *>(addr),
                          /*size_t length =*/size_t(length) *
                            sizeof(TYPE_DATA));
        } else {
            return -1;
        }
    }

    static inline void FREE(TYPE_DATA *const in) {
        free(reinterpret_cast<void *>(in));
    }

    static inline void DELETE(TYPE_DATA *const start,
                              TYPE_UINT const  size_data) {

        if (size_data > 0) { FREE(start); }
    }

  private:
    TYPE_INT   SIZE_DATA;
    TYPE_DATA *DATA;

  public:
    inline TYPE_UINT get_size() const {
        return TEMPLATES::TYPE::abs(SIZE_DATA);
    }

    inline TYPE_UINT operator()() const { return get_size(); }

  public:
    inline bool is_own_data() const { return (SIZE_DATA > 0); }

    //////////////
    // UNSAFE { //
    //////////////
  private:
    inline void OWN_DATA() { SIZE_DATA = TEMPLATES::TYPE::abs(SIZE_DATA); }
    inline void DISOWN_DATA() { SIZE_DATA = TEMPLATES::TYPE::neg(SIZE_DATA); }

    inline void DELETE() {
        if (is_own_data()) {
            DELETE(/*TYPE_DATA *const start =*/DATA,
                   /*const TYPE_UINT size_data =*/get_size());
        }
        DATA      = NULL;
        SIZE_DATA = 0;
    }
    //////////////
    // UNSAFE } //
    //////////////

  public:
    inline void copy_from(TYPE_DATA const *const data) {
        if (get_size() > 0) {
            memcpy(
              /*void *dest =*/reinterpret_cast<void *>(DATA),
              /*const void *src =*/reinterpret_cast<const void *>(data),
              /*size_t n =*/
              size_t(TEMPLATES::TYPE::abs(SIZE_DATA) * sizeof(TYPE_DATA)));
        }
    }

    inline void copy_from(TYPE_DATA const *const data, TYPE_UINT const length) {

        length = TEMPLATES::TYPE::min(TYPE_UINT(get_size()), length);

        if (length > 0) {
            memcpy(
              /*void *dest =*/reinterpret_cast<void *>(DATA),
              /*const void *src =*/reinterpret_cast<const void *>(data),
              /*size_t n =*/
              size_t(length * sizeof(TYPE_DATA)));
        }
    }

    inline void copy_from(TYPE_SELF const &in) {
        TYPE_UINT const length =
          TEMPLATES::TYPE::min(get_size(), in.get_size());

        if (length > 0) {
            memcpy(/*void *dest =*/reinterpret_cast<void *>(DATA),
                   /*const void *src =*/reinterpret_cast<const void *>(in.DATA),
                   /*size_t n =*/size_t(length * sizeof(TYPE_DATA)));
        }
    }

  public:
    inline TYPE_DATA       &get_element(TYPE_UINT const i) { return DATA[i]; }
    inline TYPE_DATA const &get_element(TYPE_UINT const i) const {
        return DATA[i];
    }

    inline TYPE_DATA &operator()(TYPE_UINT const i) { return get_element(i); }
    inline TYPE_DATA const &operator()(TYPE_UINT const i) const {
        return get_element(i);
    }

  public:
    inline TYPE_DATA *get_start() { return DATA; }
    inline TYPE_DATA *get_stop() { return DATA + get_size(); }

    inline TYPE_DATA const *get_start() const { return DATA; }
    inline TYPE_DATA const *get_stop() const { return DATA + get_size(); }

  public:
    inline int compare_start(TYPE_SELF const &other,
                             TYPE_UINT const  length) const {
        return memcmp(
          /*const void *s1 =*/static_cast<const void *>(other.get_start()),
          /*const void *s2 =*/static_cast<const void *>(get_start()),
          /*size_t n =*/size_t(length) * sizeof(TYPE_DATA));
    }

    inline int compare_start(TYPE_SELF const &other) const {
        TYPE_UINT length = TEMPLATES::TYPE::min(get_size(), other.get_size());
        return compare_start(other, length);
    }

  public:
    inline int compare_stop(TYPE_SELF const &other,
                            TYPE_UINT const  length) const {
        return memcmp(
          /*const void *s1 =*/static_cast<const void *>(other.get_stop() -
                                                        length),
          /*const void *s2 =*/static_cast<const void *>(get_stop() - length),
          /*size_t n =*/size_t(length) * sizeof(TYPE_DATA));
    }

    inline int compare_stop(TYPE_SELF const &other) const {
        TYPE_UINT length = TEMPLATES::TYPE::min(get_size(), other.get_size());
        return compare_stop(other, length);
    }

  public:
    inline void operator=(TYPE_SELF const &other) {
        DELETE();
        DATA      = other.DATA;
        SIZE_DATA = TEMPLATES::TYPE::neg(other.SIZE_DATA);
    }

    inline void operator=(TYPE_SELF &&other) {
        DELETE();
        DATA      = other.DATA;
        SIZE_DATA = other.SIZE_DATA;
        other.DISOWN_DATA();
    }

  private:
    _MACRO_CLASS_NAME_(TYPE_DATA *const data, TYPE_INT const size_data)
      : SIZE_DATA(size_data), DATA(data) {}

  public:
    _MACRO_CLASS_NAME_() : _MACRO_CLASS_NAME_(NULL, 0) {}

    _MACRO_CLASS_NAME_(TYPE_SELF &&in)
      : _MACRO_CLASS_NAME_(in.DATA, in.SIZE_DATA) {
        in.DISOWN_DATA();
    }

    _MACRO_CLASS_NAME_(TYPE_SELF const &in)
      : _MACRO_CLASS_NAME_(
          /*TYPE_DATA *data =*/in.DATA,
          /*TYPE_INT const size_data =*/TEMPLATES::TYPE::neg(in.SIZE_DATA)) {}

  public:
    ~_MACRO_CLASS_NAME_() { DELETE(); }

  public:
    static inline TYPE_SELF get_new(TYPE_UINT const size_data) {
        TYPE_SELF ret(NEW(size_data), size_data);
        return ret;
    }

  public:
    static inline TYPE_SELF view_this(TYPE_DATA *const data,
                                      TYPE_UINT const  size_data) {
        TYPE_SELF ret(data, TEMPLATES::TYPE::neg(size_data));
        return ret;
    }

    static inline TYPE_SELF view_this(TYPE_SELF const &in) {
        TYPE_SELF ret(in.DATA, TEMPLATES::TYPE::neg(in.SIZE_DATA));
        return ret;
    }

  public:
    static inline TYPE_SELF view_range(TYPE_DATA *const start,
                                       TYPE_DATA *const stop) {
        return view_this(/*TYPE_DATA * data =*/start,
                         /*TYPE_UINT const size_data =*/(stop - start));
    }

  public:
    static inline TYPE_SELF own_this(TYPE_DATA *const data,
                                     TYPE_UINT const  size_data) {
        TYPE_SELF ret(data, size_data);
        return ret;
    }

    static inline TYPE_SELF own_this(TYPE_SELF &in) {
        TYPE_INT tmp = in.SIZE_DATA;
        in.DISOWN_DATA();
        TYPE_SELF ret(in.DATA, tmp);
        return ret;
    }

    static inline TYPE_SELF own_this(TYPE_SELF &&in) {
        TYPE_INT tmp = in.SIZE_DATA;
        in.DISOWN_DATA();
        TYPE_SELF ret(in.DATA, tmp);
        return ret;
    }

    static inline TYPE_SELF own_this(TYPE_SELF const &in) {
        return view_this(in);
    }

  public:
    static inline TYPE_SELF own_range(TYPE_DATA *const start,
                                      TYPE_DATA *const stop) {
        return own_this(/*TYPE_DATA * data =*/start,
                        /*TYPE_UINT const size_data =*/(stop - start));
    }

  public:
    static inline TYPE_SELF copy_this(TYPE_DATA const *const data,
                                      TYPE_UINT const        size_data) {
        TYPE_SELF ret = get_new(size_data);
        ret.copy_from(data);
        return ret;
    }

    static inline TYPE_SELF copy_this(TYPE_SELF const &in) {
        return copy_this(in.DATA, in.get_size());
    }

  public:
    static inline TYPE_SELF copy_range(TYPE_DATA const *const start,
                                       TYPE_DATA const *const stop) {
        return copy_this(/*TYPE_DATA * data =*/start,
                         /*TYPE_UINT const size_data =*/(stop - start));
    }
};

#undef _MACRO_CLASS_NAME_

#define _MACRO_CLASS_NAME_ mapped_array

template <class TD, TEMPLATES::TYPE::uint_smallest SIZE_INT =
                      sizeof(TEMPLATES::TYPE::int_biggest)>
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_INT    = TEMPLATES::TYPE::type_int<SIZE_INT>;
    using TYPE_UINT   = TEMPLATES::TYPE::type_uint<SIZE_INT>;
    using TYPE_DATA   = TD;
    using TYPE_SELF   = _MACRO_CLASS_NAME_<TYPE_DATA, SIZE_INT>;
    using TYPE_BUFFER = dynamic_1d_array<TYPE_DATA, SIZE_INT>;

  private:
    inline void *MMAP(TYPE_UINT const length, TYPE_INT const prot,
                      TYPE_INT const flags, TYPE_INT const fd,
                      TYPE_UINT const offset) {

        return mmap(/*void *addr =*/NULL, /*size_t length =*/size_t(length),
                    /*int prot =*/int(prot), /*int flags =*/int(flags),
                    /*int fd =*/int(fd), /*off_t offset =*/off_t(offset));
    }

  public:
    inline TYPE_INT UNMAP(void *const addr, TYPE_UINT const length) {
        return TYPE_INT(
          munmap(/*void *addr =*/addr, /*size_t length =*/size_t(length)));
    }

  private:
    TYPE_BUFFER BUFFER;
    void       *DATA;
    TYPE_INT    SIZE;

  private:
    inline void OWN_THIS() { SIZE = TEMPLATES::TYPE::abs(SIZE); }
    inline void DISOWN_THIS() { SIZE = TEMPLATES::TYPE::neg(SIZE); }

  public:
    inline void operator=(TYPE_SELF &&other) {
        BUFFER = TYPE_BUFFER::own_this(other.BUFFER);
        DATA   = other.DATA;
        SIZE   = other.SIZE;
        other.DISOWN_THIS();
    }

    inline void operator=(TYPE_SELF const &other) {
        BUFFER = TYPE_BUFFER::view_this(other.BUFFER);
        DATA   = other.DATA;
        SIZE   = TEMPLATES::TYPE::neg(other.SIZE);
        other.DISOWN_THIS();
    }

  private:
    _MACRO_CLASS_NAME_(TYPE_BUFFER &&buffer, void *const data,
                       TYPE_INT const size)
      : BUFFER(TYPE_BUFFER::own_this(buffer)), DATA(data), SIZE(size) {}

    _MACRO_CLASS_NAME_(TYPE_BUFFER const &buffer, void *const data,
                       TYPE_INT const size)
      : BUFFER(TYPE_BUFFER::view_this(buffer)), DATA(data), SIZE(size) {}

  public:
    _MACRO_CLASS_NAME_(TYPE_SELF &&in) { *this = in; }
    _MACRO_CLASS_NAME_(TYPE_SELF const &in) { *this = in; }

  public:
    _MACRO_CLASS_NAME_() {}
    ~_MACRO_CLASS_NAME_() {}
};
#undef _MACRO_CLASS_NAME_

#define _MACRO_CLASS_NAME_ Dynamic1DArray

template <typename TD = double, typename TI = long> class _MACRO_CLASS_NAME_ {

    /////////////////////////
    // DEFINITIONS BEGIN:{ //
    /////////////////////////
  public:
    using TYPE_DATA = TD;
    using TYPE_INT  = TI;
    using TYPE_SELF = _MACRO_CLASS_NAME_<TYPE_DATA, TYPE_INT>;
    ///////////////////////
    // DEFINITIONS END.} //
    ///////////////////////

    //////////////////
    // DATA BEGIN:{ //
    //////////////////
  private:
    TYPE_DATA     *STORE;
    TYPE_INT const SZ;
    TYPE_INT const DS;
    bool           OWNED;
    ////////////////
    // DATA END.} //
    ////////////////

    ///////////////////////////////
    // INTERFACE TO DATA BEGIN:{ //
    ///////////////////////////////
  public:
    inline TYPE_DATA       *GET_DATA() { return STORE; }
    inline TYPE_DATA const *GET_DATA() const { return STORE; }
    inline TYPE_INT         DIST() const { return DS; }
    inline TYPE_INT         SIZE() const { return SZ; }
    inline TYPE_INT         RANGE() const { return SIZE() - 1; }
    inline TYPE_INT         NUM() const { return (DIST() * RANGE()) + 1; }
    inline bool             GET_OWNED() const { return OWNED; }
    inline TYPE_INT GET_INDEX(TYPE_INT const i) const { return i * DIST(); }
    /////////////////////////////
    // INTERFACE TO DATA END.} //
    /////////////////////////////

    ////////////////////////
    // RETRIEVING BEGIN:{ //
    ////////////////////////
  private:
    inline TYPE_DATA &GET_ELEMENT(TYPE_INT const i) {
        return STORE[GET_INDEX(i)];
    }

    inline TYPE_DATA const &GET_ELEMENT(TYPE_INT const i) const {
        return STORE[GET_INDEX(i)];
    }
    //////////////////////
    // RETRIEVING END.} //
    //////////////////////

    /////////////////////
    // SLICING BEGIN:{ //
    /////////////////////
  public:
    inline TYPE_SELF CloneRange(TYPE_INT const begin,
                                TYPE_INT const end) const {

        if (end < begin) {
            return CloneRange(end, begin);
        } else if (end < SIZE()) {
            TYPE_INT const NEW_SIZE = end - begin + 1;
            TYPE_SELF      ret(NEW_SIZE);
            for (TYPE_INT i = 0; i < NEW_SIZE; i++) {
                ret(i) = GET_ELEMENT(i + begin);
            }
            return ret;
        } else if (begin < SIZE()) {
            return CloneRange(begin, SIZE() - 1);
        } else {
            return TYPE_SELF(0);
        }
    }

    inline TYPE_SELF ViewRange(TYPE_INT const begin, TYPE_INT const end) {
        if (end < begin) {
            return ViewRange(end, begin);
        } else if (end < SIZE()) {
            TYPE_INT const NEW_SIZE = end - begin + 1;
            TYPE_SELF      ret(&(STORE[begin]), NEW_SIZE, DIST());
            return ret;
        } else {
            return ViewRange(begin, SIZE() - 1);
        }
    }
    ///////////////////
    // SLICING END.} //
    ///////////////////

    ////////////////////////
    // CONVINENCE BEGIN:{ //
    ////////////////////////
  public:
    inline TYPE_INT operator()() const { return SIZE(); }

    inline TYPE_DATA &operator()(TYPE_INT const i) { return GET_ELEMENT(i); }

    inline TYPE_DATA const &operator()(TYPE_INT const i) const {
        return GET_ELEMENT(i);
    }

    inline void operator=(TYPE_SELF const &other) {
        TYPE_INT I = mymin(SIZE(), other());
        for (TYPE_INT i = 0; i < I; i++) { GET_ELEMENT(i) = other(i); }
    }

    inline void operator=(TYPE_SELF const &&other) {
        TYPE_INT I = mymin(SIZE(), other());
        for (TYPE_INT i = 0; i < I; i++) { GET_ELEMENT(i) = other(i); }
    }

    inline void operator=(TYPE_DATA const *in) {
        for (TYPE_INT i = 0; i < SIZE(); i++) { GET_ELEMENT(i) = in[i]; }
    }

    inline void operator=(TYPE_DATA const in) {
        for (TYPE_INT i = 0; i < SIZE(); i++) { GET_ELEMENT(i) = in; }
    }

    inline TYPE_DATA operator*(TYPE_SELF const &other) const {
        TYPE_DATA ret   = 0;
        TYPE_INT  limit = mymin(SIZE(), other.SIZE());
        for (TYPE_INT i = 0; i < limit; i++) {
            ret += other(i) * GET_ELEMENT(i);
        }
        return ret / static_cast<TYPE_DATA>(limit);
    }
    //////////////////////
    // CONVINENCE END.} //
    //////////////////////

    /////////////////////////
    // CONSTRUCTOR BEGIN:{ //
    /////////////////////////
  public:
    ~_MACRO_CLASS_NAME_() {
        if (OWNED) { delete[] STORE; }
    }

    _MACRO_CLASS_NAME_(TYPE_INT const size)
      : SZ(size), DS(1), OWNED((SZ * DS) > 0) {
        if (OWNED) {
            TYPE_INT const total = NUM();
            STORE                = new TYPE_DATA[total];
        }
    }

    _MACRO_CLASS_NAME_(TYPE_DATA *store, TYPE_INT const size,
                       TYPE_INT const dist = 1, bool const own = false)
      : STORE(store), SZ(size), DS(dist), OWNED(own) {}

    _MACRO_CLASS_NAME_(TYPE_SELF &&in)
      : STORE(in.STORE), SZ(in.SZ), DS(in.DS), OWNED(in.OWNED) {
        in.OWNED = false;
    }

    _MACRO_CLASS_NAME_(TYPE_SELF const &in) : _MACRO_CLASS_NAME_(in()) {
        this[0] = in;
    }
    ///////////////////////
    // CONSTRUCTOR END.} //
    ///////////////////////

    ///////////////////////////////
    // Static interfaces BEGIN:{ //
    ///////////////////////////////
  public:
    static inline TYPE_SELF CLONE(TYPE_SELF const &in) {
        TYPE_SELF ret(in);
        return ret;
    }

    static inline TYPE_SELF CLONE(std::vector<TYPE_DATA> const &in) {
        TYPE_SELF ret(in.size());
        ret = &(in[0]);
        return ret;
    }
    /////////////////////////////
    // Static interfaces END.} //
    /////////////////////////////
};

#undef _MACRO_CLASS_NAME_

#endif
