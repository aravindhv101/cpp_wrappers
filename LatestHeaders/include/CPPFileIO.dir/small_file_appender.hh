#ifndef _HEADER_GUARD_CPPFileIO_dir_small_file_appender_hh_
#define _HEADER_GUARD_CPPFileIO_dir_small_file_appender_hh_

///////////////////////
// Includes BEGIN: { //
///////////////////////
#include "./Basic.hh"
#include "./FileArray.hh"
#include "./FullFileReader.hh"
#include "./D1.hh"
/////////////////////
// Includes END. } //
/////////////////////

#define _MACRO_CLASS_NAME_ small_file_appender

class _MACRO_CLASS_NAME_ {
    //////////////////////////
    // Definitions BEGIN: { //
    //////////////////////////
  public:
    using TYPE_SELF         = _MACRO_CLASS_NAME_;
    using TYPE_WRITER       = FileArray<char>;
    using TYPE_BUFFER       = Dynamic1DArray<char, size_t>;
    using TYPE_BUFFER_CONST = Dynamic1DArray<char const, size_t>;
    ////////////////////////
    // Definitions END. } //
    ////////////////////////

    ////////////////////////////
    // Data elements BEGIN: { //
    ////////////////////////////
  private:
    TYPE_WRITER WRITER;
    size_t      CURRENT;
    //////////////////////////
    // Data elements END. } //
    //////////////////////////

    /////////////////////////////////////
    // Main working functions BEGIN: { //
    /////////////////////////////////////
  private:
    inline void APPEND(char const *in, size_t const length) {
        void *dest = static_cast<void *>(&(WRITER(CURRENT, length)));

        /* void * */ memcpy(
          /* void *dest = */ dest,
          /* const void *src = */ static_cast<void const *>(in),
          /* size_t n = */ length);

        CURRENT += length;
    }

    inline void APPEND(TYPE_BUFFER_CONST &in) { APPEND(&(in(0)), in()); }

    inline void APPEND(std::string const name_file_in) {
        full_file_reader<char> reader(name_file_in);
        APPEND(&(reader(0)), reader());
    }

    inline void APPEND(std::vector<std::string> const &in) {
        for (size_t i = 0; i < in.size(); i++) { APPEND(in[i]); }
    }

  public:
    inline void operator()(char const *in, size_t const length) {
        APPEND(in, length);
    }

    inline void operator()(std::string const name_file_in) {
        APPEND(name_file_in);
    }

    inline void operator()(TYPE_BUFFER_CONST &in) { APPEND(in); }
    inline void operator()(std::vector<std::string> const &in) { APPEND(in); }
    ///////////////////////////////////
    // Main working functions END. } //
    ///////////////////////////////////

    ///////////////////////////////////////
    // Constructor & Destructor BEGIN: { //
    ///////////////////////////////////////
  public:
    _MACRO_CLASS_NAME_(std::string const name_file_out)
      : WRITER(name_file_out), CURRENT(0) {
        WRITER.writeable(true);
    }

    ~_MACRO_CLASS_NAME_() { WRITER.size(CURRENT); }
    /////////////////////////////////////
    // Constructor & Destructor END. } //
    /////////////////////////////////////
};

#undef _MACRO_CLASS_NAME_
#endif
