﻿#ifndef _HEADER_GUARD_CPPFileIO_FullFileReader_
#define _HEADER_GUARD_CPPFileIO_FullFileReader_

////////////////////
// Headers BEGIN: //
////////////////////
#include "./FileArray.hh"
//////////////////
// Headers END. //
//////////////////

#define _MACRO_CLASS_NAME_ full_file_reader

template <class TD, TEMPLATES::TYPE::uint_smallest SIZE_INT =
                      sizeof(TEMPLATES::TYPE::int_biggest)>
class _MACRO_CLASS_NAME_ {

    ////////////////////////
    // Definitions BEGIN: //
    ////////////////////////
  public:
    using TYPE_INT    = TEMPLATES::TYPE::type_int<SIZE_INT>;
    using TYPE_UINT   = TEMPLATES::TYPE::type_uint<SIZE_INT>;
    using TYPE_DATA   = TD;
    using TYPE_SELF   = _MACRO_CLASS_NAME_<TYPE_DATA, SIZE_INT>;
    using TYPE_READER = FileArray<TYPE_DATA>;
    //////////////////////
    // Definitions END. //
    //////////////////////

    /////////////////////////
    // Data Element BEGIN: //
    /////////////////////////
  private:
    TYPE_READER     MAIN_READER;
    TYPE_UINT const SIZE_DATA;
    TYPE_DATA      *DATA;
    ///////////////////////
    // Data Element END. //
    ///////////////////////

  public:
    inline TYPE_DATA       &get_element(TYPE_UINT const i) { return DATA[i]; }
    inline TYPE_DATA const &get_element(TYPE_UINT const i) const {
        return DATA[i];
    }

    inline TYPE_DATA &operator()(TYPE_UINT const i) { return get_element(i); }
    inline TYPE_DATA const &operator()(TYPE_UINT const i) const {
        return get_element(i);
    }

  public:
    inline TYPE_UINT get_size() const {
        return TEMPLATES::TYPE::abs(SIZE_DATA);
    }
    inline TYPE_UINT operator()() const { return get_size(); }

  public:
    inline TYPE_DATA *get_start() { return DATA; }
    inline TYPE_DATA *get_stop() { return DATA + SIZE_DATA; }

    inline TYPE_DATA const *get_start() const { return DATA; }
    inline TYPE_DATA const *get_stop() const { return DATA + SIZE_DATA; }

    /////////////////////////////////////
    // Constructor & Destructor BEGIN: //
    /////////////////////////////////////
  public:
    _MACRO_CLASS_NAME_(std::string const filename)
      : MAIN_READER(filename), SIZE_DATA(MAIN_READER.size()) {
        MAIN_READER.writeable(false);
        MAIN_READER.set_map_write();
        MAIN_READER.set_map_private();
        DATA = (&(MAIN_READER(0, SIZE_DATA)));
    }
    ///////////////////////////////////
    // Constructor & Destructor END. //
    ///////////////////////////////////
};

#undef _MACRO_CLASS_NAME_

#endif
