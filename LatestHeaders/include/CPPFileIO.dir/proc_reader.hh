#ifndef _HEADER_GUARD_CPPFileIO_dir_proc_reader_hh_
#define _HEADER_GUARD_CPPFileIO_dir_proc_reader_hh_

#include "./Basic.hh"
#include "./D1.hh"
#include "./StaticHorsePoolSearch.hh"

//////////////////////////////////////////
// Main class for reading proc BEGIN: { //
//////////////////////////////////////////

#define _MACRO_CLASS_NAME_ proc_reader

template <size_t buf_size_pw = 12> class _MACRO_CLASS_NAME_ {

    ////////////////////////////////////
    // Important definitions BEGIN: { //
    ////////////////////////////////////
  public:
    using TYPE_SELF   = _MACRO_CLASS_NAME_;
    using TYPE_BUFFER = Dynamic1DArray<char>;
    //////////////////////////////////
    // Important definitions END. } //
    //////////////////////////////////

    ////////////////////////////////////////////
    // The horsepool searching class BEGIN: { //
    ////////////////////////////////////////////
  private:
    class word_MemAvailable {
      public:
        using TYPE_SELF = _MACRO_CLASS_NAME_;
        using TYPE_INT  = size_t;
        using CHAR      = TYPE_BYTE;

      public:
        static inline TYPE_INT constexpr SIZE() { return 13; }

        static inline CHAR constexpr GET_CHAR(size_t const i) {
            switch (i) {
                case 12: return 77;  // M
                case 11: return 101; // e
                case 10: return 109; // m
                case 9: return 65;   // A
                case 8: return 118;  // v
                case 7: return 97;   // a
                case 6: return 105;  // i
                case 5: return 108;  // l
                case 4: return 97;   // a
                case 3: return 98;   // b
                case 2: return 108;  // l
                case 1: return 101;  // e
                case 0: return 58;   // :
                default: return 0;
            }
        }

        static inline TYPE_INT constexpr GET_JUMP(CHAR const in) {
            switch (in) {
                case 58: return 0;   // :
                case 65: return 9;   // A
                case 77: return 12;  // M
                case 97: return 4;   // a
                case 98: return 3;   // b
                case 101: return 1;  // e
                case 105: return 6;  // i
                case 108: return 2;  // l
                case 109: return 10; // m
                case 118: return 8;  // v
                default: return SIZE();
            }
        }
    };

    using TYPE_FINDER_MEM_AVAILABLE =
      StaticHorsePoolSearch<TYPE_BUFFER, word_MemAvailable>;
    //////////////////////////////////////////
    // The horsepool searching class END. } //
    //////////////////////////////////////////

    /////////////////////////////////////
    // Important data members BEGIN: { //
    /////////////////////////////////////
  private:
    TYPE_BUFFER               PROC_READER_MEM;
    TYPE_FINDER_MEM_AVAILABLE FIND_MEM_AVAILABLE;
    ///////////////////////////////////
    // Important data members END. } //
    ///////////////////////////////////

    /////////////////////////////////////////
    // The main exposed variables BEGIN: { //
    /////////////////////////////////////////
  public:
    size_t mem_available;
    ///////////////////////////////////////
    // The main exposed variables END. } //
    ///////////////////////////////////////

    //////////////////////////////////////////
    // Important working functions BEGIN: { //
    //////////////////////////////////////////
  private:
    inline void READ_PROC() {
        int const proc_fd = open(
          /*const char *pathname =*/"/proc/meminfo",
          /*int flags =*/O_RDONLY);

        ssize_t sz =
          read(/*int fd =*/proc_fd, /*void *buf =*/&(PROC_READER_MEM(0)),
               /*size_t count =*/4069);

        if (sz > PROC_READER_MEM() - 2) {

            printf("DISASTER!!! Need a bigger buffer to read the full mem info "
                   "file in proc reader...\n");

        } else {
            // printf("Read the file successfully and got %zu bytes\n", sz);
        }

        /*int*/ close(
          /*int fd =*/proc_fd);
    }

    inline void READ_MEM_AVAILABLE() {
        size_t location = FIND_MEM_AVAILABLE(0) + 1;
        if (true) {
        MainLoop1:
            switch (PROC_READER_MEM(location)) {
                default:
                    if (true) {
                        location++;
                        goto MainLoop1;
                    }

                case '0' ... '9': break;
            }
        }

        size_t stop = location;
        if (true) {
        MainLoop2:
            switch (PROC_READER_MEM(stop)) {
                case '0' ... '9':
                    if (true) {
                        stop++;
                        goto MainLoop2;
                    }

                default: break;
            }
            PROC_READER_MEM(stop) = 0;
        }

        sscanf(&(PROC_READER_MEM(location)), "%zu", &mem_available);
        mem_available = mem_available << 10;
    }
    ////////////////////////////////////////
    // Important working functions END. } //
    ////////////////////////////////////////

    //////////////////////////////////////////////////////
    // Function to read and refresh all values BEGIN: { //
    //////////////////////////////////////////////////////
  public:
    inline void refresh() {
        READ_PROC();
        READ_MEM_AVAILABLE();
    }

    inline void mem_wait(size_t const mem_required,
                         size_t const sleep_interval = 1) {

    BEGINNING:
        if (true) {
            refresh();
            if (mem_available < mem_required) {
                printf("Waiting for memory...\n");
                sleep(sleep_interval);
                goto BEGINNING;
            }
        }
    }
    ////////////////////////////////////////////////////
    // Function to read and refresh all values END. } //
    ////////////////////////////////////////////////////

    /////////////////////////////////////////
    // Constructor and destructor BEGIN: { //
    /////////////////////////////////////////
  public:
    _MACRO_CLASS_NAME_()
      : PROC_READER_MEM((1 << buf_size_pw) + 2),
        FIND_MEM_AVAILABLE(PROC_READER_MEM) {
        refresh();
    }

    ~_MACRO_CLASS_NAME_() {}
    ///////////////////////////////////////
    // Constructor and destructor END. } //
    ///////////////////////////////////////
};

#undef _MACRO_CLASS_NAME_

////////////////////////////////////////
// Main class for reading proc END. } //
////////////////////////////////////////

#endif
