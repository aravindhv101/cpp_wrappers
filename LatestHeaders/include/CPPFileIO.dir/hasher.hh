#ifndef _HEADER_GUARD_CPPFileIO_dir_hasher_hh_
#define _HEADER_GUARD_CPPFileIO_dir_hasher_hh_

#include "../xxHash.hh"
#include "./Basic.hh"
#include "./FileArray.hh"
#include "./FullFileReader.hh"
#include "./D1.hh"

inline size_t hash_mem(void const *input, size_t const len) {
    size_t const res = XXH3_64bits(input, len);
    return res;
}

inline size_t hash_mem(char const *input, size_t const len) {
    size_t const res = hash_mem(reinterpret_cast<void const *>(input), len);
    return res;
}

inline size_t hash_mem(char const *input) {
    size_t const len = strlen(input);
    size_t const res = hash_mem(input, len);
    return res;
}

inline size_t hash_mem(std::string const input) {
    size_t const res =
      hash_mem(reinterpret_cast<void const *>(input.c_str()), input.length());

    return res;
}

template <typename T> inline size_t hash_mem(std::vector<T> const input) {
    void const  *buf    = reinterpret_cast<void const *>(&(input[0]));
    size_t const length = sizeof(T) * input.size();
    return hash_mem(buf, length);
}

template <typename T> inline size_t hash_mem(Dynamic1DArray<T> const &input) {
    void const  *buf    = reinterpret_cast<void const *>(input.GET_DATA());
    size_t const length = sizeof(T) * input();
    return hash_mem(buf, length);
}

inline size_t hash_file(std::string const name_file_input) {
    full_file_reader<char> reader(name_file_input);
    void const            *buf = &(reader(0));
    size_t const           res = hash_mem(buf, reader());
    return res;
}

#endif
