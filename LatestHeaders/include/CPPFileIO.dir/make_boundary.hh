#ifndef _HEADER_GUARD_CPPFileIO_dir_make_boundary_hh_
#define _HEADER_GUARD_CPPFileIO_dir_make_boundary_hh_

#include "./FileArray.hh"
#include "./FileWriter.hh"
#include "./Atomic_Counter.hh"

#define _MACRO_CLASS_NAME_ make_boundary_slave

/////////////////////////////////
// Main working class BEGIN: { //
/////////////////////////////////
template <typename T> class _MACRO_CLASS_NAME_ {

    //////////////////////////
    // Definitions BEGIN: { //
    //////////////////////////
  public:
    using TYPE_SELF            = _MACRO_CLASS_NAME_;
    using TYPE_ELEMENT         = T;
    using TYPE_READER          = FileArray<T>;
    using TYPE_BOUNDARY        = std::vector<size_t>;
    using TYPE_BOUNDARIES      = std::vector<TYPE_BOUNDARY>;
    using TYPE_BOUNDARY_WRITER = FileWriter<size_t>;
    using ATOMIC_INT           = Atomic_Counter<size_t>;
    ////////////////////////
    // Definitions END. } //
    ////////////////////////

    ////////////////////////////////////////////
    // Static function for file size BEGIN: { //
    ////////////////////////////////////////////
  private:
    static inline size_t SIZE_FILE(std::string const infile) {
        FileArray<TYPE_ELEMENT> reader(infile);
        return reader();
    }
    //////////////////////////////////////////
    // Static function for file size END. } //
    //////////////////////////////////////////

    ////////////////////////////
    // Data elements BEGIN: { //
    ////////////////////////////
  private:
    std::string const NAME_FILE_BIN_INPUT;
    std::string const NAME_FILE_BOUNDARY_OUTPUT;
    size_t const      NTH;
    size_t const      LIMIT;
    ATOMIC_INT        COUNTER;
    std::thread *     THREADS;
    TYPE_BOUNDARY *   BOUNDARIES;
    //////////////////////////
    // Data elements END. } //
    //////////////////////////

    /////////////////////////////////////
    // Main working functions BEGIN: { //
    /////////////////////////////////////
  private:
    inline size_t THREAD_BONDARY(size_t i) { return (i * LIMIT) / NTH; }

    inline void WORK(size_t i) {
        size_t const start = THREAD_BONDARY(i);
        size_t const stop  = THREAD_BONDARY(i + 1);
        FileArray<T> reader(NAME_FILE_BIN_INPUT);
        TYPE_ELEMENT current = reader(start);
        TYPE_ELEMENT previous;
        for (size_t j = start + 1; j <= stop; ++j) {
            previous = current;
            current  = reader(j);
            if (!current(previous)) { BOUNDARIES[i].push_back(j); }
        }
    }

    inline void WORK() {
        for (size_t i = (COUNTER++); i < NTH; i = (COUNTER++)) { WORK(i); }
    }

    inline void WORK_ALL() {
        COUNTER = 0;
        for (size_t i = 0; i < NTH; ++i) {
            THREADS[i] = std::thread(thread_slave<TYPE_SELF>, this);
        }
        for (size_t i = 0; i < NTH; ++i) { THREADS[i].join(); }
    }

    inline size_t SUM_BOUNDARIES() const {
        size_t sum = 0;
        for (size_t i = 0; i < NTH; ++i) { sum += BOUNDARIES[i].size(); }
        return sum;
    }

    inline void WRITE_OUT() {
        TYPE_BOUNDARY final_boundary;
        final_boundary.reserve(SUM_BOUNDARIES() + 2);
        final_boundary.push_back(0);
        for (size_t i = 0; i < NTH; ++i) {
            for (size_t j = 0; j < BOUNDARIES[i].size(); ++j) {
                final_boundary.push_back(BOUNDARIES[i][j]);
            }
        }
        final_boundary.push_back(LIMIT + 1);
        TYPE_BOUNDARY_WRITER writer(NAME_FILE_BOUNDARY_OUTPUT);
        writer(final_boundary);
    }
    ///////////////////////////////////
    // Main working functions END. } //
    ///////////////////////////////////

    //////////////////////////////////////////////
    // Function to free dynamic memory BEGIN: { //
    //////////////////////////////////////////////
    inline void FREE_ALL() {
        delete[] BOUNDARIES;
        delete[] THREADS;
    }
    ////////////////////////////////////////////
    // Function to free dynamic memory END. } //
    ////////////////////////////////////////////

    //////////////////////////////////////////////
    // Wrapper interface for threading BEGIN: { //
    //////////////////////////////////////////////
  public:
    inline void operator()() { WORK(); }
    ////////////////////////////////////////////
    // Wrapper interface for threading END. } //
    ////////////////////////////////////////////

    ///////////////////////////////////////
    // Constructor & Destructor BEGIN: { //
    ///////////////////////////////////////
  private:
    _MACRO_CLASS_NAME_(std::string const name_file_bin_input,
                       std::string const name_file_boundary_output,
                       size_t const      nth)
      : NAME_FILE_BIN_INPUT(name_file_bin_input),
        NAME_FILE_BOUNDARY_OUTPUT(name_file_boundary_output), NTH(nth),
        LIMIT(SIZE_FILE(NAME_FILE_BIN_INPUT) - 1),
        THREADS(new std::thread[NTH]), BOUNDARIES(new TYPE_BOUNDARY[NTH]) {
        if (LIMIT < 0xFFFFFFFFFFFFFFFF) { WORK_ALL(); }
    }

    ~_MACRO_CLASS_NAME_() {
        WRITE_OUT();
        FREE_ALL();
    }
    /////////////////////////////////////
    // Constructor & Destructor END. } //
    /////////////////////////////////////

    ////////////////////////////////////
    // Main public interface BEGIN: { //
    ////////////////////////////////////
  public:
    static inline size_t work(std::string const name_file_bin_input,
                              std::string const name_file_boundary_output,
                              size_t const      nth) {
        TYPE_SELF slave(name_file_bin_input, name_file_boundary_output, nth);
        return slave.LIMIT + 1;
    }
    //////////////////////////////////
    // Main public interface END. } //
    //////////////////////////////////
};
///////////////////////////////
// Main working class END. } //
///////////////////////////////

////////////////////////////////////////////
// Convenient function interface BEGIN: { //
////////////////////////////////////////////
template <typename T>
inline size_t make_boundary(std::string const name_file_bin,
                            std::string const name_file_boundary,
                            size_t const      nth = 1) {

    return _MACRO_CLASS_NAME_<T>::work(name_file_bin, name_file_boundary, nth);
}
//////////////////////////////////////////
// Convenient function interface END. } //
//////////////////////////////////////////

#undef _MACRO_CLASS_NAME_

#endif
