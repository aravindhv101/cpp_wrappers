#ifndef _HEADER_GUARD_CPPFileIO_dir_AnalyzeLines_hh_
#define _HEADER_GUARD_CPPFileIO_dir_AnalyzeLines_hh_

/////////////////////
// Includes BEGIN: //
/////////////////////
#include "./Basic.hh"
#include "./FastTXT2BIN_NEW.hh"
#include "./FileFD.hh"
#include "./number_checker.hh"
///////////////////
// Includes END. //
///////////////////

///////////////////////////
// Main Analyzer BEGIN:{ //
///////////////////////////
#define _MACRO_CLASS_NAME_ AnalyzeLines
class _MACRO_CLASS_NAME_ {

    /////////////////////////////
    // Main Definitions BEGIN: //
    /////////////////////////////
  public:
    using TYPE_SELF                        = _MACRO_CLASS_NAME_;
    static char constexpr CODE_TYPE_I64    = -8;
    static char constexpr CODE_TYPE_I32    = -4;
    static char constexpr CODE_TYPE_I16    = -2;
    static char constexpr CODE_TYPE_I8     = -1;
    static char constexpr CODE_TYPE_U64    = 8;
    static char constexpr CODE_TYPE_U32    = 4;
    static char constexpr CODE_TYPE_U16    = 2;
    static char constexpr CODE_TYPE_U8     = 1;
    static char constexpr CODE_TYPE_FLOAT  = 0;
    static char constexpr CODE_TYPE_STRING = 64;
    ///////////////////////////
    // Main Definitions END. //
    ///////////////////////////

    //////////////////////////
    // Data Elements BEGIN: //
    //////////////////////////
  private:
    std::vector<size_t>      sizes;
    std::vector<size_t>      status_codes;
    std::vector<std::string> labels;
    ////////////////////////
    // Data Elements END. //
    ////////////////////////

    ////////////////////
    // Merging BEGIN: //
    ////////////////////
  private:
    inline void MERGE(TYPE_SELF const &other) {
        if (sizes.size() < other.sizes.size()) /* Part for sizes */ {
            sizes.resize(other.sizes.size());
        }

        for (size_t i = 0; i < sizes.size(); ++i) {
            sizes[i] = mymax(sizes[i], other.sizes[i]);
        }

        if (true) /* Part for status codes: */ {

            while (status_codes.size() < other.status_codes.size()) {
                status_codes.push_back(CODE_TYPE_U8);
            }

            size_t i = 0;
            while (i < other.status_codes.size()) {

                status_codes[i] =
                  merge_codes(status_codes[i], other.status_codes[i]);

                i++;
            }
        }

        if (true) /* Part for labels */ {
            size_t size_a = labels.size();
            size_t size_b = other.labels.size();
            if (size_b > size_a) {
                labels.resize(size_b);
                for (size_t i = size_a; i < size_b; i++) {
                    labels[i] = other.labels[i];
                }
            }
        }
    }

  public:
    inline void operator()(TYPE_SELF const &other) { MERGE(other); }
    //////////////////
    // Merging END. //
    //////////////////

    //////////////////////////////
    // Writing to file BEGIN: { //
    //////////////////////////////
  private:
    inline ssize_t WRITE_OUT(FileFD &in) const {
        ssize_t ret = (in << sizes);
        ret += (in << status_codes);
        ret += in.WriteStrings(labels);
        return ret;
    }

    inline ssize_t WRITE_OUT(std::string const outfilename) const {
        FileFD file(outfilename);
        file.writefile();
        return WRITE_OUT(file);
    }

  public:
    inline ssize_t operator>>(std::string const filename) const {
        return WRITE_OUT(filename);
    }
    ////////////////////////////
    // Writing to file END. } //
    ////////////////////////////

    //////////////////////////////////
    // Reading from a file BEGIN: { //
    //////////////////////////////////
  private:
    inline ssize_t READ_IN(FileFD &in) {
        ssize_t ret = (in >> sizes);
        ret += (in >> status_codes);
        ret += in.ReadStrings(labels);
        return ret;
    }

    inline ssize_t READ_IN(std::string const infilename) {
        FileFD file(infilename);
        file.readfile();
        return READ_IN(file);
    }

  public:
    inline ssize_t operator<<(std::string const filename) {
        return READ_IN(filename);
    }
    ////////////////////////////////
    // Reading from a file END. } //
    ////////////////////////////////

    //////////////////////////
    // Infer Codes BEGIN: { //
    //////////////////////////
  private:
    static inline std::string InferCodes(char const in, size_t const sz) {
        switch (in) {
            case CODE_TYPE_I64:
                if (true) { return std::string("CPPFileIO::TYPE_I64"); }
                break;

            case CODE_TYPE_I32:
                if (true) { return std::string("CPPFileIO::TYPE_I32"); }
                break;

            case CODE_TYPE_I16:
                if (true) { return std::string("CPPFileIO::TYPE_I16"); }
                break;

            case CODE_TYPE_I8:
                if (true) { return std::string("CPPFileIO::TYPE_I8"); }
                break;

            case CODE_TYPE_U64:
                if (true) { return std::string("CPPFileIO::TYPE_U64"); }
                break;

            case CODE_TYPE_U32:
                if (true) { return std::string("CPPFileIO::TYPE_U32"); }
                break;

            case CODE_TYPE_U16:
                if (true) { return std::string("CPPFileIO::TYPE_U16"); }
                break;

            case CODE_TYPE_U8:
                if (true) { return std::string("CPPFileIO::TYPE_U8"); }
                break;

            case CODE_TYPE_FLOAT:
                if (true) { return std::string("double"); }
                break;

            case CODE_TYPE_STRING:
                if (true) {
                    char tmp[128];
                    sprintf(tmp, "StaticArray::ND_ARRAY <%ld,char>", (sz + 1));
                    return std::string(tmp);
                }
                break;
        }

        if (true) {
            char tmp[128];
            sprintf(tmp, "StaticArray::ND_ARRAY <%ld,char>", (sz + 1));
            return std::string(tmp);
        }
    }

    inline std::string InferCodes(size_t const i) const {
        return InferCodes(status_codes[i], sizes[i]);
    }
    ////////////////////////
    // Infer Codes END. } //
    ////////////////////////

    /////////////////////////////
    // Analyze status BEGIN: { //
    /////////////////////////////
  private:
    inline char AnalyzeStatus(std::string const &in) {
        return number_type_and_size(in.c_str());
    }

    inline char AnalyzeStatus(char const *in) {
        return number_type_and_size(in);
    }

    inline void AnalyzeStatus(std::vector<std::string> const &in) {
        status_codes.reserve(in.size());
        while (status_codes.size() < in.size()) {
            status_codes.push_back(CODE_TYPE_U8);
        }
        for (size_t i = 0; i < in.size(); i++) {
            auto const rettype = AnalyzeStatus(in[i]);
            status_codes[i]    = merge_codes(status_codes[i], rettype);
        }
    }

    inline void AnalyzeStatus(std::vector<char const *> const &in) {
        status_codes.reserve(in.size());
        while (status_codes.size() < in.size()) {
            status_codes.push_back(CODE_TYPE_U8);
        }
        for (size_t i = 0; i < in.size(); i++) {
            auto const rettype = AnalyzeStatus(in[i]);
            status_codes[i]    = merge_codes(status_codes[i], rettype);
        }
    }
    ///////////////////////////
    // Analyze status END. } //
    ///////////////////////////

    ///////////////////////////////////
    // Main Working Functions BEGIN: //
    ///////////////////////////////////
  private:
    inline void ReadLine(std::vector<std::string> const &in) {
        sizes.reserve(in.size());
        while (in.size() > sizes.size()) { sizes.push_back(0); }
        for (size_t i = 0; i < in.size(); i++) {
            sizes[i] = mymax(sizes[i], in[i].size());
        }
    }

    inline void ReadLine(std::vector<char const *> const &in) {
        sizes.reserve(in.size());
        while (in.size() > sizes.size()) { sizes.push_back(0); }
        for (size_t i = 0; i < in.size(); i++) {
            size_t const length = strlen(in[i]);
            sizes[i]            = mymax(sizes[i], length);
        }
    }
    /////////////////////////////////
    // Main Working Functions END. //
    /////////////////////////////////

    ////////////////////////////////////////
    // Header Generation Functions BEGIN: //
    ////////////////////////////////////////
  public:
    inline void show_read_write_wrappers(FILE *f = stdout) const {
        fprintf(f, "\n");

        fprintf(f, "    inline bool is_valid()const{\n");
        fprintf(f, "        return true;\n");
        fprintf(f, "    }\n");

        fprintf(f, "\n");

        fprintf(f, "    inline bool operator () (TYPE_LINES const & in ) {\n");
        fprintf(f, "        this->Read_All(in);\n");
        fprintf(f, "        return this->is_valid();\n");
        fprintf(f, "    }\n");

        fprintf(f, "\n");

        fprintf(f, "    static inline void tsv_2_bin(std::string const "
                   "name_file_tsv, std::string const name_file_bin) {\n");
        fprintf(f, "        TYPE_SLAVE::tsv_2_bin(name_file_tsv, "
                   "name_file_bin, 64, 64);\n");
        fprintf(f, "    }\n");

        fprintf(f, "\n");

        fprintf(f, "    static inline CPPFileIO::FullFileReader <TYPE_SELF>\n");
        fprintf(f, "    Get_Reader_Full (std::string const filename) {\n");
        fprintf(f, "        return\n");
        fprintf(f, "            CPPFileIO::FullFileReader <TYPE_SELF>\n");
        fprintf(f, "                (filename)\n");
        fprintf(f, "        ;\n");
        fprintf(f, "    }\n");

        fprintf(f, "\n");

        fprintf(f, "    static inline CPPFileIO::FileArray <TYPE_SELF>\n");
        fprintf(f, "    Get_Reader (std::string const filename) {\n");
        fprintf(f, "        return\n");
        fprintf(f, "            CPPFileIO::FileArray <TYPE_SELF>\n");
        fprintf(f, "                (filename)\n");
        fprintf(f, "        ;\n");
        fprintf(f, "    }\n");

        fprintf(f, "\n");

        fprintf(f, "    static inline CPPFileIO::FileWriter <TYPE_SELF>\n");
        fprintf(f, "    Get_Writer (std::string const filename) {\n");
        fprintf(f, "        return\n");
        fprintf(f, "            CPPFileIO::FileWriter <TYPE_SELF>\n");
        fprintf(f, "                (filename)\n");
        fprintf(f, "        ;\n");
        fprintf(f, "    }\n");

        fprintf(f, "\n");
    }

    inline void show_labels(FILE *f = stdout) const {
        printf("\n");
        for (size_t i = 0; i < labels.size(); i++) {
            fprintf(f, "#define L%zu_ %s\n", i, labels[i].c_str());
        }
        printf("\n");
        for (size_t i = 0; i < labels.size(); i++) {
            fprintf(f, "#undef L%zu_\n", i);
        }
        printf("\n");
    }

    inline void show_header(FILE *f = stdout) const {
        fprintf(f, "\n");

        fprintf(f, "#define _MACRO_CLASS_NAME_ StorageElement\n");
        fprintf(f, "\n");
        fprintf(f, "class _MACRO_CLASS_NAME_ {\n");
        fprintf(f, "\n");
        fprintf(f, "public:\n");

        fprintf(f, "    using TYPE_SELF = _MACRO_CLASS_NAME_;\n");
        fprintf(f, "    using TYPE_LINES = std::vector <char const *>;\n");

        fprintf(f, "\n");

        fprintf(f, "    using TYPE_SLAVE = CPPFileIO::SampleSlave<\n");
        fprintf(f, "      /* typename sortable = */ TYPE_SELF,\n");
        fprintf(f, "      /* bool dosort = */ false,\n");
        fprintf(f, "      /* bool dodup = */ true,\n");
        fprintf(f, "      /* bool has_header = */ true>;\n");

        fprintf(f, "\n");
    }

    inline void show_compare(FILE *f = stdout) const {
        fprintf(f, "  public:\n");

        fprintf(f, "    inline char compare(TYPE_SELF const &in) const {\n");
        fprintf(f, "\n");
        fprintf(f, "#define CMP(name)                                          "
                   "                 \\\n");
        fprintf(f, "    if (true) {                                            "
                   "                 \\\n");
        fprintf(f, "        char const val = "
                   "Read_Show_Functions::Compare(this->name, in.name); \\\n");
        fprintf(f, "        if (val != 0) { return val; }                      "
                   "                 \\\n");
        fprintf(f, "    }\n");

        fprintf(f, "\n");

        for (size_t i = 0; i < labels.size(); i++) {
            if (status_codes[i] != 0) {
                fprintf(f, "        CMP(%s)\n", labels[i].c_str());
            }
        }

        fprintf(f, "\n");
        fprintf(f, "#undef CMP\n");
        fprintf(f, "        return 0;\n");
        fprintf(f, "\n");
        fprintf(f, "    }\n");

        fprintf(f, "\n");

        fprintf(
          f, "#define CMP(name)                                          \\\n");
        fprintf(
          f, "    inline bool operator name(TYPE_SELF const &in) const { \\\n");
        fprintf(
          f, "        return compare(in) name 0;                         \\\n");
        fprintf(f, "    }\n");
        fprintf(f, "\n");

        fprintf(f, "    CMP(>)\n");
        fprintf(f, "    CMP(<)\n");
        fprintf(f, "    CMP(==)\n");
        fprintf(f, "    CMP(!=)\n");
        fprintf(f, "    CMP(>=)\n");
        fprintf(f, "    CMP(<=)\n");

        fprintf(f, "\n");
        fprintf(f, "#undef CMP\n");
    }

    inline void show_data(FILE *f = stdout) const {
        for (size_t i = 0; i < sizes.size(); i++) {
            char tmp[8];
            sprintf(tmp, "L%zu_", i);
            fprintf(f, "    %s %s; // %zu = %zu\n", InferCodes(i).c_str(), tmp,
                    i, sizes[i]);
        }
        fprintf(f, "\n");
    }

    inline void show_read(FILE *f = stdout) const {
        fprintf(f, "\n");

        fprintf(f, "    inline void\n");
        fprintf(f, "    Read_All (\n");
        fprintf(f, "        std::vector <std::string> const &\n");
        fprintf(f, "            in\n");
        fprintf(f, "    ) {\n");
        fprintf(f, "        using namespace Read_Show_Functions;\n");
        fprintf(f, "        size_t i=0 ;\n");

        fprintf(f, "\n");

        fprintf(f, "#define SA(name)           \\\n");
        fprintf(f, "    if (i < in.size()) {   \\\n");
        fprintf(f, "        Read(name, in[i]); \\\n");
        fprintf(f, "        i++;               \\\n");
        fprintf(f, "    }\n");

        fprintf(f, "\n");

        fprintf(f, "        _MACRO_SA_\n");
        fprintf(f, "        #undef SA\n");
        fprintf(f, "    }");

        fprintf(f, "\n");

        fprintf(f, "    inline void\n");
        fprintf(f, "    Read_All (\n");
        fprintf(f, "        std::vector <char const *> const &\n");
        fprintf(f, "            in\n");
        fprintf(f, "    ) {\n");
        fprintf(f, "        using namespace Read_Show_Functions;\n");
        fprintf(f, "        size_t i=0 ;\n");

        fprintf(f, "\n");

        fprintf(f, "#define SA(name)           \\\n");
        fprintf(f, "    if (i < in.size()) {   \\\n");
        fprintf(f, "        Read(name, in[i]); \\\n");
        fprintf(f, "        i++;               \\\n");
        fprintf(f, "    }\n");

        fprintf(f, "\n");

        fprintf(f, "        _MACRO_SA_\n");
        fprintf(f, "        #undef SA\n");
        fprintf(f, "    }\n");
    }

    inline void show_show(FILE *f = stdout) const {
        fprintf(f, "\n");

        fprintf(f, "    inline void\n");
        fprintf(f, "    Show_All (\n");
        fprintf(f, "        FILE *\n");
        fprintf(f, "            f = stdout\n");
        fprintf(f, "    ) const {\n");
        fprintf(f, "        using namespace Read_Show_Functions;\n");
        fprintf(f, "        #define SA(name) Show(name,f); Show(f);\n");
        fprintf(f, "        _MACRO_SA_\n");
        fprintf(f, "        #undef SA\n");
        fprintf(f, "        Show_Next(f);\n");
        fprintf(f, "    }\n");
    }

    inline void show_macros(FILE *f = stdout) const {
        fprintf(f, "#define _MACRO_SA_ \\\n");
        for (size_t i = 0; i < sizes.size(); i++) {
            fprintf(f, "    SA(L%ld_)", i);
            if (i != (sizes.size() - 1)) { fprintf(f, " \\"); }
            fprintf(f, "\n");
        }
        show_read(f);
        show_show(f);
        fprintf(f, "\n#undef _MACRO_SA_\n");
        fprintf(f, "\n");
    }

    inline void show_tail(FILE *f = stdout) const {
        fprintf(f, "};\n");
        fprintf(f, "\n");
        fprintf(f, "#undef _MACRO_CLASS_NAME_\n");
        fprintf(f, "\n");
    }

    inline void show(FILE *f = stdout) const {
        show_header(f);
        show_labels(f);
        show_data(f);
        show_compare(f);
        show_macros(f);
        show_read_write_wrappers(f);
        show_tail(f);
    }
    //////////////////////////////////////
    // Header Generation Functions END. //
    //////////////////////////////////////

    ///////////////////////
    // Interfaces BEGIN: //
    ///////////////////////
  public:
    inline void Read_Labels(std::vector<std::string> const &in) {
        labels.resize(in.size());
        for (size_t i = 0; i < in.size(); i++) { labels[i] = in[i]; }
    }

    inline void Read_Labels(std::vector<char const *> const &in) {
        labels.resize(in.size());
        for (size_t i = 0; i < in.size(); i++) { labels[i] = in[i]; }
    }

    inline size_t operator()() const { return sizes.size(); }

    inline size_t operator[](size_t const in) const { return sizes[in]; }

    inline void operator()(std::vector<std::string> const &in) {
        ReadLine(in);
        AnalyzeStatus(in);
    }

    inline void operator()(std::vector<char const *> const &in) {
        ReadLine(in);
        AnalyzeStatus(in);
    }
    /////////////////////
    // Interfaces END. //
    /////////////////////

    /////////////////////////////////////
    // Constructor & Destructor BEGIN: //
    /////////////////////////////////////
  public:
    _MACRO_CLASS_NAME_() {}
    ~_MACRO_CLASS_NAME_() {}
    ///////////////////////////////////
    // Constructor & Destructor END. //
    ///////////////////////////////////
};
#undef _MACRO_CLASS_NAME_
/////////////////////////
// Main Analyzer END.} //
/////////////////////////

///////////////////////////////////////////////////
// Slave for multi threaded analysis new BEGIN:{ //
///////////////////////////////////////////////////
#define _MACRO_CLASS_NAME_ AnalyzeSlave
class _MACRO_CLASS_NAME_ {

    ////////////////////////
    // Definitions BEGIN: //
    ////////////////////////
  public:
    using TYPE_SELF     = _MACRO_CLASS_NAME_;
    using TYPE_WORD     = std::string;
    using TYPE_LINE     = std::vector<char const *>;
    using TYPE_ANALYZER = AnalyzeLines;
    //////////////////////
    // Definitions END. //
    //////////////////////

    //////////////////////////
    // Data Elements BEGIN: //
    //////////////////////////
  private:
    TYPE_WORD const FILENAME;
    TYPE_ANALYZER   ANALYZER;
    size_t          COUNT;
    size_t const    INDEX;
    ////////////////////////
    // Data Elements END. //
    ////////////////////////

    ////////////////////////////////
    // Required interfaces BEGIN: //
    ////////////////////////////////
  public:
    static inline void SORT(TYPE_WORD const) {}

    static inline void MERGE(TYPE_WORD const in1, TYPE_WORD const in2,
                             TYPE_WORD const out) {

        TYPE_ANALYZER A1;
        TYPE_ANALYZER A2;
        A1 << in1;
        A2 << in2;
        A1(A2);
        A1 >> out;
    }

    inline void operator()(TYPE_LINE const &in) {
        bool const outcome = (COUNT == 0) && (INDEX == 0);
        if (outcome) {
            ANALYZER.Read_Labels(in);
        } else {
            ANALYZER(in);
        }
        COUNT++;
    }

    _MACRO_CLASS_NAME_(std::string const filename, size_t const index)
      : FILENAME(filename), INDEX(index) {
        COUNT = 0;
    }

    ~_MACRO_CLASS_NAME_() { ANALYZER >> FILENAME; }
    //////////////////////////////
    // Required interfaces END. //
    //////////////////////////////

    template <char seperator, char newline>
    static inline void PrepareFileSchema(std::string const infilename,
                                         std::string const outfilename,
                                         size_t const      n_splits  = 16,
                                         size_t const      n_threads = 4) {

        FastTXT2BIN_NEW<TYPE_SELF, seperator, newline>::Do_All(
          infilename, outfilename, n_splits, n_threads);

        TYPE_ANALYZER analyze;
        analyze << outfilename;
        std::string const headername = outfilename + ".hh";
        FILE *            f          = fopen(headername.c_str(), "w");
        analyze.show(f);
        fclose(f);
    }
};
#undef _MACRO_CLASS_NAME_
/////////////////////////////////////////////////
// Slave for multi threaded analysis new END.} //
/////////////////////////////////////////////////

#endif
