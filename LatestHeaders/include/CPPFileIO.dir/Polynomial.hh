﻿#ifndef _HEADER_GUARD_CPPFileIO_Polynomial_
#define _HEADER_GUARD_CPPFileIO_Polynomial_

#include "./D1.hh"
#include "./BinarySearch.hh"

#define _MACRO_CLASS_NAME_ polynomial

template <typename TF = double> class _MACRO_CLASS_NAME_ {
    /////////////////////////
    // Definitions BEGIN:{ //
    /////////////////////////
  public:
    using TYPE_FLOAT        = TF;
    using TYPE_INT          = size_t;
    using TYPE_SELF         = _MACRO_CLASS_NAME_<TYPE_FLOAT>;
    using TYPE_COEFFICIENTS = std::vector<TYPE_FLOAT>;
    ///////////////////////
    // Definitions END.} //
    ///////////////////////

    ///////////////////////////
    // Data Elements BEGIN:{ //
    ///////////////////////////
  private:
    TYPE_COEFFICIENTS COEFFICIENTS;
    /////////////////////////
    // Data Elements END.} //
    /////////////////////////

    /////////////////////////////
    // Access elements BEGIN:{ //
    /////////////////////////////
  public:
    inline TYPE_FLOAT &coefficients(TYPE_INT const i) {
        return COEFFICIENTS[i];
    }

    inline TYPE_FLOAT &operator()(TYPE_INT const i) { return coefficients(i); }

    inline TYPE_FLOAT coefficients(TYPE_INT const i) const {
        return COEFFICIENTS[i];
    }

    inline TYPE_FLOAT operator()(TYPE_INT const i) const {
        return coefficients(i);
    }

    inline TYPE_COEFFICIENTS &coefficients() { return COEFFICIENTS; }

    inline TYPE_COEFFICIENTS const &coefficients() const {
        return COEFFICIENTS;
    }
    ///////////////////////////
    // Access elements END.} //
    ///////////////////////////

    /////////////////////////////////
    // Order of polynomial BEGIN:{ //
    /////////////////////////////////
  public:
    inline TYPE_INT order() const { return coefficients().size() - 1; }
    inline TYPE_INT operator()() const { return order(); }
    ///////////////////////////////
    // Order of polynomial END.} //
    ///////////////////////////////

    //////////////////////////////////////////
    // Change order of coefficients BEGIN:{ //
    //////////////////////////////////////////
  public:
    inline void reorder(TYPE_INT const i) {
        TYPE_INT const old_size = order();
        coefficients().resize(i + 1);
        for (TYPE_INT i = old_size + 1; i <= order(); ++i) {
            coefficients(i) = 0;
        }
    }
    inline void clear() { reorder(0); }
    ////////////////////////////////////////
    // Change order of coefficients END.} //
    ////////////////////////////////////////

    ///////////////////////////
    // Debug related BEGIN:{ //
    ///////////////////////////
  public:
    inline void show() {
        std::string res;
        for (TYPE_INT i = 0; i <= order(); ++i) {
            char tmp[64];
            sprintf(tmp, "+ ( %lf * x ^ %zu ) ", coefficients(i), i);
            res = res + tmp;
        }
        printf("%s\n", res.c_str());
    }
    /////////////////////////
    // Debug related END.} //
    /////////////////////////

    /////////////////////////////////////
    // Construction interfaces BEGIN:{ //
    /////////////////////////////////////
  public:
    _MACRO_CLASS_NAME_() {}
    _MACRO_CLASS_NAME_(TYPE_INT const i) : COEFFICIENTS(i + 1) {}
    _MACRO_CLASS_NAME_(TYPE_SELF const &i) : COEFFICIENTS(i.coefficients()) {}

  public:
    static inline TYPE_SELF p_get(TYPE_INT const i) {
        TYPE_SELF ret(i);
        return ret;
    }

    static inline TYPE_SELF p_0(TYPE_INT const i) {
        TYPE_SELF ret = p_get(i);
        for (TYPE_INT i = 0; i <= ret(); i++) { ret(i) = 0; }
        return ret;
    }

    static inline TYPE_SELF p_0() { return p_0(0); }

    static inline TYPE_SELF p_1(TYPE_INT const i) {
        TYPE_SELF ret = p_0(i);
        ret(0)        = 1;
        return ret;
    }

    static inline TYPE_SELF p_1() { return p_1(0); }

    static inline TYPE_SELF p_x(TYPE_INT const i) {
        TYPE_SELF ret = p_0(i);
        ret(1)        = 1;
        return ret;
    }

    static inline TYPE_SELF p_x() { return p_x(1); }
    ///////////////////////////////////
    // Construction interfaces END.} //
    ///////////////////////////////////

    ////////////////////////////////
    // Assignment related BEGIN:{ //
    ////////////////////////////////
  public:
    inline void assign(TYPE_SELF const &other) {
        coefficients() = other.coefficients();
    }

    inline void operator=(TYPE_SELF const &other) { assign(other); }

    inline void assign(TYPE_FLOAT const other) {
        reorder(0);
        coefficients(0) = other;
    }

    inline void operator=(TYPE_FLOAT const other) { assign(other); }
    //////////////////////////////
    // Assignment related END.} //
    //////////////////////////////

    //////////////////////////////
    // Multiply related BEGIN:{ //
    //////////////////////////////
  public:
    inline TYPE_SELF multiply(TYPE_SELF const &other) const {
        TYPE_SELF ret = p_0(order() + other.order());
        for (TYPE_INT i = 0; i <= order(); i++) {
            for (TYPE_INT j = 0; j <= other.order(); j++) {
                ret(i + j) += coefficients(i) * other.coefficients(j);
            }
        }
        return ret;
    }

    inline void multiply(TYPE_FLOAT const other) {
        for (TYPE_INT i = 0; i <= order(); i++) { coefficients(i) *= other; }
    }

    inline TYPE_SELF operator*(TYPE_SELF const &other) const {
        return multiply(other);
    }

    inline void operator*=(TYPE_SELF const &other) {
        coefficients() = multiply(other).coefficients();
    }

    inline void operator*=(TYPE_FLOAT const other) { multiply(other); }
    ////////////////////////////
    // Multiply related END.} //
    ////////////////////////////

    ////////////////////////////
    // Divide related BEGIN:{ //
    ////////////////////////////
  public:
    inline void divide(TYPE_FLOAT const other) {
        for (TYPE_INT i = 0; i <= order(); i++) { coefficients(i) /= other; }
    }
    inline void operator/=(TYPE_FLOAT const other) { divide(other); }
    //////////////////////////
    // Divide related END.} //
    //////////////////////////

    //////////////////////////////
    // Subtract related BEGIN:{ //
    //////////////////////////////
  public:
    inline TYPE_SELF subtract(TYPE_SELF const &other) const {
        TYPE_INT  min_order = mymin(order(), other.order());
        TYPE_INT  max_order = mymax(order(), other.order());
        TYPE_SELF ret       = p_0(max_order);
        TYPE_INT  index     = 0;

        while (index <= min_order) {
            ret(index) = coefficients(index) - other.coefficients(index);
            index++;
        }

        while (index <= order()) {
            ret(index) = coefficients(index);
            index++;
        }

        while (index <= other.order()) {
            ret(index) = -other.coefficients(index);
            index++;
        }

        return ret;
    }

    inline void subtract_self(TYPE_SELF const &other) {
        if (order() < other.order()) { reorder(other.order()); }
        for (TYPE_INT i = 0; i <= other.order(); ++i) {
            coefficients(i) -= other.coefficients(i);
        }
    }

    inline void subtract_self(TYPE_FLOAT const other) {
        coefficients(0) -= other;
    }

    inline TYPE_SELF operator-(TYPE_SELF const &other) const {
        return subtract(other);
    }

    inline void operator-=(TYPE_SELF const &other) { subtract_self(other); }
    inline void operator-=(TYPE_FLOAT const other) { subtract_self(other); }
    ////////////////////////////
    // Subtract related END.} //
    ////////////////////////////

    /////////////////////////
    // Add related BEGIN:{ //
    /////////////////////////
  public:
    inline TYPE_SELF add(TYPE_SELF const &other) const {

        TYPE_INT  min_order = mymin(order(), other.order());
        TYPE_INT  max_order = mymax(order(), other.order());
        TYPE_SELF ret       = p_0(max_order);
        TYPE_INT  index     = 0;

        while (index <= min_order) {
            ret(index) = coefficients(index) + other(index);
            index++;
        }

        while (index <= order()) {
            ret(index) = coefficients(index);
            index++;
        }

        while (index <= other.order()) {
            ret(index) = other(index);
            index++;
        }

        return ret;
    }

    inline void add_self(TYPE_SELF const &other) {
        if (order() < other.order()) { reorder(other.order()); }
        for (TYPE_INT i = 0; i <= other.order(); ++i) {
            coefficients(i) += other.coefficients(i);
        }
    }

    inline void add_self(TYPE_FLOAT const other) { coefficients(0) += other; }

    inline TYPE_SELF operator+(TYPE_SELF const &other) const {
        return add(other);
    }
    inline void operator+=(TYPE_SELF const &other) { add_self(other); }
    inline void operator+=(TYPE_FLOAT const other) { add_self(other); }
    ///////////////////////
    // Add related END.} //
    ///////////////////////

    //////////////////////
    // Calculus BEGIN:{ //
    //////////////////////
  public:
    inline TYPE_SELF differentiate() const {
        if (order() == 0) { return p_0(); }

        TYPE_SELF ret = p_0(order() - 1);

        for (TYPE_INT i = 0; i <= ret.order(); i++) {
            ret(i) = (i + 1) * coefficients(i + 1);
        }

        return ret;
    }

    inline TYPE_SELF integrate() const {
        TYPE_SELF ret = p_0(order() + 1);
        for (TYPE_INT i = 1; i <= ret.order(); ++i) {
            ret(i) = i * coefficients(i - 1);
        }
        return ret;
    }
    ////////////////////
    // Calculus END.} //
    ////////////////////

    ////////////////////////////////
    // Evaluation related BEGIN:{ //
    ////////////////////////////////
    inline TYPE_FLOAT evaluate(TYPE_FLOAT const in) const {
        TYPE_FLOAT ret = 0;
        TYPE_FLOAT xpw = 1;
        for (TYPE_INT i = 0; i <= order(); i++) {
            ret += coefficients(i) * xpw;
            xpw *= in;
        }
        return ret;
    }
    //////////////////////////////
    // Evaluation related END.} //
    //////////////////////////////
};

#undef _MACRO_CLASS_NAME_

#define _MACRO_CLASS_NAME_ interpolate
template <typename TF> class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_FLOAT      = TF;
    using TYPE_INT        = size_t;
    using TYPE_SELF       = _MACRO_CLASS_NAME_<TYPE_FLOAT>;
    using TYPE_POLYNOMIAL = polynomial<TYPE_FLOAT>;

    struct TYPE_ELEMENT {
        TYPE_FLOAT x, y;

        inline bool operator>(TYPE_ELEMENT const &other) const {
            return x > other.x;
        }
        inline bool operator<(TYPE_ELEMENT const &other) const {
            return x < other.x;
        }
        inline bool operator==(TYPE_ELEMENT const &other) const {
            return x == other.x;
        }
        inline bool operator!=(TYPE_ELEMENT const &other) const {
            return x != other.x;
        }
    };

    using TYPE_ELEMENTS = std::vector<TYPE_ELEMENT>;
    using TYPE_BUFFER   = Dynamic1DArray<TYPE_ELEMENT, TYPE_INT>;
    using TYPE_FINDER   = BinarySearch<TYPE_BUFFER>;

    struct TYPE_COMPARE {
        inline char operator()(TYPE_ELEMENT const &a,
                               TYPE_ELEMENT const &b) const {
            return (a.x > b.x) - (a.x < b.x);
        }

        inline char operator()(TYPE_ELEMENT const &a,
                               TYPE_FLOAT const    b) const {
            return (a.x > b) - (a.x < b);
        }

        inline char operator()(TYPE_FLOAT const & a,
                               TYPE_ELEMENT const b) const {
            return (a > b.x) - (a < b.x);
        }

        inline char operator()(TYPE_FLOAT const &a, TYPE_FLOAT const b) const {
            return (a > b) - (a < b);
        }
    };

  public:
    static inline TYPE_ELEMENT get_point(TYPE_FLOAT const x,
                                         TYPE_FLOAT const y) {

        TYPE_ELEMENT ret;
        ret.x = x;
        ret.y = y;
        return ret;
    }

  private:
    TYPE_ELEMENTS   ELEMENTS;
    bool            PREPARED;
    TYPE_POLYNOMIAL POLYNOMIAL;

  private:
    inline void DO_PREPARE() {
        if (PREPARED || (ELEMENTS.size() < 2)) { return; }

        std::sort(ELEMENTS.begin(), ELEMENTS.end());

        TYPE_ELEMENTS tmp;
        tmp.push_back(ELEMENTS[0]);

        for (size_t i = 1; i < ELEMENTS.size(); i++) {
            if (ELEMENTS[i] != ELEMENTS[i - 1]) { tmp.push_back(ELEMENTS[i]); }
        }

        ELEMENTS = tmp;

        POLYNOMIAL.reorder(0);
        POLYNOMIAL(0) = 0;

        PREPARED = true;
    }

    inline TYPE_POLYNOMIAL &GET_RANGE(TYPE_INT begin, TYPE_INT end) {
        if (true) /* Check validity: */ {
            bool const good =
              (0 <= begin) && (begin <= end) && (end < size()) && PREPARED;

            if (!good) { return POLYNOMIAL; }
        }

        for (TYPE_INT i = begin; i <= end; ++i) {

            TYPE_POLYNOMIAL product     = TYPE_POLYNOMIAL::p_1();
            TYPE_FLOAT      denominator = 1;

            for (TYPE_INT j = begin; j <= end; ++j) {
                if (j != i) {
                    TYPE_POLYNOMIAL tmp = TYPE_POLYNOMIAL::p_x(1);
                    tmp(0)              = -ELEMENTS[j].x;
                    denominator *= (ELEMENTS[i].x - ELEMENTS[j].x);
                    product *= tmp;
                }
            }

            product *= ELEMENTS[i].y / denominator;
            POLYNOMIAL += product;
        }

        return POLYNOMIAL;
    }

  public:
    inline TYPE_INT size() const { return ELEMENTS.size(); }

    inline void append(TYPE_FLOAT const x, TYPE_FLOAT const y) {
        TYPE_ELEMENT tmp = {x, y};
        ELEMENTS.push_back(tmp);
        PREPARED = false;
    }

    inline void clear() {
        ELEMENTS.clear();
        PREPARED = true;
    }

  public:
    inline TYPE_POLYNOMIAL &get() {
        DO_PREPARE();
        return GET_RANGE(0, size() - 1);
    }

    inline TYPE_POLYNOMIAL &get(TYPE_INT const x, TYPE_INT const order) {
        DO_PREPARE();

        TYPE_COMPARE compare;
        TYPE_BUFFER  buffer(&(ELEMENTS[0]), ELEMENTS.size());
        TYPE_FINDER  finder(buffer);

        auto const res = finder(x, compare);

        if (res.status == -1) { return POLYNOMIAL; }

        TYPE_INT begin;
        if (res.begin >= order) {
            begin = res.begin - order;
        } else {
            begin = 0;
        }

        TYPE_INT end = res.end + order;
        if (end >= size()) { end = size() - 1; }

        return GET_RANGE(begin, end);
    }

  public:
    _MACRO_CLASS_NAME_() : PREPARED(true) {}
    ~_MACRO_CLASS_NAME_() {}
};
#undef _MACRO_CLASS_NAME_

#endif
