#ifndef _HEADER_GUARD_CPPFileIO_FastTXT2BIN_NEW_
#define _HEADER_GUARD_CPPFileIO_FastTXT2BIN_NEW_

////////////////////
// Headers BEGIN: //
////////////////////
#include "./D1.hh"
#include "./ExternalPrograms.hh"
#include "./FileArray.hh"
#include "./FileWriter.hh"
#include "./Sorter.hh"
#include "./FileDivider.hh"
#include "./MyStr.hh"
#include "./FullFileReader.hh"
#include "./find_executables.hh"
#include "./Atomic_Counter.hh"
#include "./BufferLineReader.hh"
//////////////////
// Headers END. //
//////////////////

//////////////////////////////////////
// Main Common definitions BEGIN: { //
//////////////////////////////////////
#define _MACRO_COMMON_DEF_                                                     \
  public:                                                                      \
    using TYPE_SLAVE      = T;                                                 \
    using TYPE_DIVIDER    = FileDivider<newline>;                              \
    using TYPE_BOUNDARIES = typename TYPE_DIVIDER::TYPE_BOUNDARIES;            \
    using TYPE_BUFFER     = typename TYPE_DIVIDER::TYPE_BUFFER;                \
    using TYPE_STRINGS    = std::vector<std::string>;                          \
    using ATOMIC_INT      = Atomic_Counter<size_t>;                            \
    using TYPE_READER     = BufferLineReaderVectorized;
////////////////////////////////////
// Main Common definitions END. } //
////////////////////////////////////

////////////////////////////////////////
// Main common data elements BEGIN: { //
////////////////////////////////////////
#define _MACRO_COMMON_DATA_ELEMENTS_                                           \
  private:                                                                     \
    size_t const      N_SPLITS;                                                \
    size_t const      NTH;                                                     \
    std::string const FILENAME;                                                \
    std::string const OUTPUTNAME;                                              \
    TYPE_DIVIDER      DIVIDER;                                                 \
    TYPE_BOUNDARIES   BOUNDARIES;                                              \
    ATOMIC_INT        COUNTER;                                                 \
    std::thread      *THREADS;
//////////////////////////////////////
// Main common data elements END. } //
//////////////////////////////////////

/////////////////////////////////////////
// Common filename generation BEGIN: { //
/////////////////////////////////////////
#define _MACRO_COMMON_FILENAME_GENERATION_                                     \
  private:                                                                     \
    inline std::string const DIRNAME() const {                                 \
        std::string const ret = OUTPUTNAME + ".dir/";                          \
        mkdir(ret.c_str(), 0755);                                              \
        return ret;                                                            \
    }                                                                          \
    inline std::string const GET_OUT_FILENAME(size_t const i) {                \
        std::string const ret =                                                \
          DIRNAME() + "out." + std::to_string(i) + ".bin";                     \
        return ret;                                                            \
    }                                                                          \
    inline std::string const GET_OUT_FILENAME(size_t const i,                  \
                                              size_t const j) {                \
        if (i == j) {                                                          \
            return GET_OUT_FILENAME(i);                                        \
        } else {                                                               \
            std::string const ret = DIRNAME() + "out." + std::to_string(i) +   \
                                    "-" + std::to_string(j) + ".bin";          \
            return ret;                                                        \
        }                                                                      \
    }
///////////////////////////////////////
// Common filename generation END. } //
///////////////////////////////////////

///////////////////////////////////////
// Common working functions BEGIN: { //
///////////////////////////////////////
#define _MACRO_COMMON_WORKING_FUNCTIONS_                                       \
  private:                                                                     \
    inline void WORK() {                                                       \
        for (size_t i = (COUNTER++); i < N_SPLITS; i = (COUNTER++)) {          \
            WORK(i);                                                           \
        }                                                                      \
    }                                                                          \
                                                                               \
  public:                                                                      \
    inline void operator()() { WORK(); }                                       \
                                                                               \
  private:                                                                     \
    inline void WORK_ALL() {                                                   \
        COUNTER = 0;                                                           \
        for (size_t i = 0; i < NTH; i++) {                                     \
            THREADS[i] = std::thread(thread_slave<TYPE_SELF>, this);           \
        }                                                                      \
        for (size_t i = 0; i < NTH; i++) { THREADS[i].join(); }                \
    }                                                                          \
                                                                               \
  private:                                                                     \
    inline void MERGE_ALL() {                                                  \
        std::vector<std::string> tmpfilenames;                                 \
        for (size_t i = 0; i < N_SPLITS; i++) {                                \
            tmpfilenames.push_back(GET_OUT_FILENAME(i));                       \
        }                                                                      \
        sort_and_merge<TYPE_SLAVE>::WORK(tmpfilenames, OUTPUTNAME, NTH, true); \
        rmdir(DIRNAME().c_str());                                              \
    }                                                                          \
                                                                               \
  private:                                                                     \
    inline void do_all() {                                                     \
        WORK_ALL();                                                            \
        MERGE_ALL();                                                           \
    }                                                                          \
                                                                               \
  private:                                                                     \
    _MACRO_CLASS_NAME_(size_t const n_splits, size_t const nth,                \
                       std::string const filename,                             \
                       std::string const outputname)                           \
      : N_SPLITS(n_splits), NTH(nth), FILENAME(filename),                      \
        OUTPUTNAME(outputname), DIVIDER(FILENAME),                             \
        THREADS(new std::thread[NTH]) {                                        \
        BOUNDARIES = DIVIDER(N_SPLITS);                                        \
    }                                                                          \
    ~_MACRO_CLASS_NAME_() { delete[] THREADS; }                                \
                                                                               \
  public:                                                                      \
    static inline void Do_All(std::string const inputname,                     \
                              std::string const outputname,                    \
                              size_t const n_splits, size_t const n_threads) { \
        TYPE_SELF slave(n_splits, n_threads, inputname, outputname);           \
        slave.do_all();                                                        \
    }

/////////////////////////////////////
// Common working functions END. } //
/////////////////////////////////////

/////////////////////////////////////
// Main Class Line reading BEGIN:{ //
/////////////////////////////////////
#define _MACRO_CLASS_NAME_ FastLine2BIN_NEW
template <typename T, char newline> class _MACRO_CLASS_NAME_ {

    ///////////////////////////
    // common defs1 BEGIN: { //
    ///////////////////////////
    _MACRO_COMMON_DEF_
    using TYPE_SELF = _MACRO_CLASS_NAME_<TYPE_SLAVE, newline>;
    _MACRO_COMMON_DATA_ELEMENTS_
    _MACRO_COMMON_FILENAME_GENERATION_
    /////////////////////////
    // Common defs1 END. } //
    /////////////////////////

    /////////////////////////
    // Main TXT2BIN BEGIN: //
    /////////////////////////
  private:
    inline std::string const WORK(size_t const index) {
        std::string const OUT_FILENAME = GET_OUT_FILENAME(index);
        TYPE_BUFFER       buffer(0);
        DIVIDER(index, buffer);
        MyStr<typename TYPE_BUFFER::TYPE_INT> str(buffer);
        auto                                  cuts = str(newline);
        TYPE_SLAVE tmpbuf(OUT_FILENAME, index, cuts.size());
        for (size_t i = 0; i < cuts.size(); i++) {
            tmpbuf(cuts[i].start(), cuts[i].stop());
        }
        return GET_OUT_FILENAME(index);
    }
    ///////////////////////
    // Main TXT2BIN END. //
    ///////////////////////

    ///////////////////////////
    // common defs2 BEGIN: { //
    ///////////////////////////
    _MACRO_COMMON_WORKING_FUNCTIONS_
    /////////////////////////
    // Common defs2 END. } //
    /////////////////////////
};
#undef _MACRO_CLASS_NAME_
///////////////////////////////////
// Main Class line reading END.} //
///////////////////////////////////

/////////////////////////////////////
// Main Class Line reading BEGIN:{ //
/////////////////////////////////////
#define _MACRO_CLASS_NAME_ FastTXT2BIN_NEW
template <typename T, char seperator, char newline> class _MACRO_CLASS_NAME_ {

    ///////////////////////////
    // common defs1 BEGIN: { //
    ///////////////////////////
    _MACRO_COMMON_DEF_
    using TYPE_SELF = _MACRO_CLASS_NAME_<TYPE_SLAVE, seperator, newline>;
    _MACRO_COMMON_DATA_ELEMENTS_
    _MACRO_COMMON_FILENAME_GENERATION_
    /////////////////////////
    // Common defs1 END. } //
    /////////////////////////

    /////////////////////////
    // Main TXT2BIN BEGIN: //
    /////////////////////////
  private:
    inline std::string const WORK(size_t const index) {

        std::string const OUT_FILENAME = GET_OUT_FILENAME(index);

        TYPE_BUFFER buffer(0);
        DIVIDER(index, buffer);
        TYPE_READER reader(buffer.GET_DATA(), buffer(), seperator, newline);

        TYPE_SLAVE tmpbuf(OUT_FILENAME, index);
    MainLoop:
        /* The main working loop: */ {
            auto const &lines = reader();
            if (lines.size() > 0) {
                tmpbuf(lines);
                goto MainLoop;
            }
        }

        return GET_OUT_FILENAME(index);
    }
    ///////////////////////
    // Main TXT2BIN END. //
    ///////////////////////

    ///////////////////////////
    // common defs2 BEGIN: { //
    ///////////////////////////
    _MACRO_COMMON_WORKING_FUNCTIONS_
    /////////////////////////
    // Common defs2 END. } //
    /////////////////////////
};
#undef _MACRO_CLASS_NAME_
///////////////////////////////////
// Main Class line reading END.} //
///////////////////////////////////

///////////////////////////
// Undef macros BEGIN: { //
///////////////////////////
#undef _MACRO_COMMON_WORKING_FUNCTIONS_
#undef _MACRO_COMMON_FILENAME_GENERATION_
#undef _MACRO_COMMON_DATA_ELEMENTS_
#undef _MACRO_COMMON_DEF_
/////////////////////////
// Undef macros END. } //
/////////////////////////

///////////////////////////////////
// Example slave to read BEGIN:{ //
///////////////////////////////////
#define _MACRO_CLASS_NAME_ SampleSlave
template <typename sortable, bool dosort = false, bool dodup = true,
          bool has_header = true>
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_ELEMENT     = sortable;
    using TYPE_LINES       = std::vector<char const *>;
    using TYPE_STRING      = std::string;
    using TYPE_READER      = FileArray<TYPE_ELEMENT>;
    using TYPE_FULL_READER = full_file_reader<TYPE_ELEMENT>;
    using TYPE_WRITER      = FileWriter<TYPE_ELEMENT, TYPE_U64>;

    using TYPE_SELF =
      _MACRO_CLASS_NAME_<TYPE_ELEMENT, dosort, dodup, has_header>;

    using TYPE_TSV_READER = FastTXT2BIN_NEW<TYPE_SELF, '\t', '\n'>;

  public:
    static inline TYPE_STRING const NAME_FILE_TSV() {
        return TYPE_ELEMENT::NAME_FILE_TSV();
    }

    static inline TYPE_STRING const NAME_FILE_BIN() {
        return TYPE_ELEMENT::NAME_FILE_BIN();
    }

  private:
    std::string const         OUTFILENAME;
    size_t const              INDEX;
    size_t                    COUNTS;
    std::vector<TYPE_ELEMENT> DATA;

  public:
    inline void operator()(TYPE_LINES const &in) {
        bool outcome = has_header && (INDEX == 0) && (COUNTS == 0);
        if (!outcome) {
            TYPE_ELEMENT tmp;
            if (tmp(in)) { DATA.push_back(tmp); }
        }
        COUNTS++;
    }

    static inline void SORT(TYPE_STRING const filename) {
        if (dosort) { SortFile<TYPE_ELEMENT>(filename); }
    }

    static inline void MERGE(std::string const a1, std::string const a2,
                             std::string const out) {
        if (dodup) {
            MergeFileDeDup<TYPE_ELEMENT>(a1, a2, out);
        } else {
            MergeFile<TYPE_ELEMENT>(a1, a2, out);
        }
    }

  public:
    _MACRO_CLASS_NAME_(std::string const outfilename, size_t const index)
      : OUTFILENAME(outfilename), INDEX(index), COUNTS(0) {}

    ~_MACRO_CLASS_NAME_() {
        FileWriter<TYPE_ELEMENT> writer(OUTFILENAME);
        switch (DATA.size()) {
            default: std::sort(DATA.begin(), DATA.end());
            case 1: writer(DATA);
            case 0: break;
        }
    }

  public:
    static inline void tsv_2_bin(size_t const n_pieces,
                                 size_t const n_threads) {

        TYPE_TSV_READER::Do_All(NAME_FILE_TSV(), NAME_FILE_BIN(), n_pieces,
                                n_threads);
    }

    static inline void tsv_2_bin(std::string const infilename,
                                 std::string const outfilename,
                                 size_t const      n_pieces,
                                 size_t const      n_threads) {

        TYPE_TSV_READER::Do_All(infilename, outfilename, n_pieces, n_threads);
    }
};
#undef _MACRO_CLASS_NAME_
/////////////////////////////////
// Example slave to read END.} //
/////////////////////////////////

/////////////////////////////////////////////////
// Example slave to read lines by line BEGIN:{ //
/////////////////////////////////////////////////
#define _MACRO_CLASS_NAME_ SampleSlaveLineReader
template <typename sortable, bool dosort = false, bool dodup = true,
          bool has_header = true>
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_ELEMENT     = sortable;
    using TYPE_STRING      = std::string;
    using TYPE_READER      = FileArray<TYPE_ELEMENT>;
    using TYPE_FULL_READER = full_file_reader<TYPE_ELEMENT>;
    using TYPE_WRITER      = FileWriter<TYPE_ELEMENT, TYPE_U64>;

    using TYPE_SELF =
      _MACRO_CLASS_NAME_<TYPE_ELEMENT, dosort, dodup, has_header>;

    using TYPE_TSV_READER = FastLine2BIN_NEW<TYPE_SELF, '\n'>;

  public:
    static inline TYPE_STRING const NAME_FILE_ASCII() {
        return TYPE_ELEMENT::NAME_FILE_ASCII();
    }

    static inline TYPE_STRING const NAME_FILE_BIN() {
        return TYPE_ELEMENT::NAME_FILE_BIN();
    }

  private:
    std::string const         OUTFILENAME;
    size_t const              INDEX;
    size_t const              NUM_LINES;
    size_t                    COUNTS;
    std::vector<TYPE_ELEMENT> DATA;

  public:
    inline void operator()(char *begin, char *end) {
        bool outcome = has_header && (INDEX == 0) && (COUNTS == 0);
        if (!outcome) {
            TYPE_ELEMENT tmp;
            if (tmp(begin, end)) { DATA.push_back(tmp); }
        }
        COUNTS++;
    }

    static inline void SORT(TYPE_STRING const filename) {
        if (dosort) { SortFile<TYPE_ELEMENT>(filename); }
    }

    static inline void MERGE(std::string const a1, std::string const a2,
                             std::string const out) {

        if (dodup) {
            MergeFileDeDup<TYPE_ELEMENT>(a1, a2, out);
        } else {
            MergeFile<TYPE_ELEMENT>(a1, a2, out);
        }
    }

  public:
    _MACRO_CLASS_NAME_(std::string const outfilename, size_t const index,
                       size_t const num_lines)
      : OUTFILENAME(outfilename), INDEX(index), NUM_LINES(num_lines),
        COUNTS(0) {}

    ~_MACRO_CLASS_NAME_() {
        FileWriter<TYPE_ELEMENT> writer(OUTFILENAME);
        switch (DATA.size()) {
            default: std::sort(DATA.begin(), DATA.end());
            case 1: writer(DATA);
            case 0: break;
        }
    }

  public:
    static inline void tsv_2_bin(size_t const n_pieces,
                                 size_t const n_threads) {

        TYPE_TSV_READER::Do_All(NAME_FILE_ASCII(), NAME_FILE_BIN(), n_pieces,
                                n_threads);
    }

    static inline void tsv_2_bin(std::string const infilename,
                                 std::string const outfilename,
                                 size_t const      n_pieces,
                                 size_t const      n_threads) {

        TYPE_TSV_READER::Do_All(infilename, outfilename, n_pieces, n_threads);
    }
};
#undef _MACRO_CLASS_NAME_
///////////////////////////////////////////////
// Example slave to read lines by line END.} //
///////////////////////////////////////////////

#endif
