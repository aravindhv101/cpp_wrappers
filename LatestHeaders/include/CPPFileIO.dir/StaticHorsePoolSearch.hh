#ifndef _HEADER_GUARD_CPPFileIO_dir_StaticHorsePoolSearch_hh_
#define _HEADER_GUARD_CPPFileIO_dir_StaticHorsePoolSearch_hh_

#include "./Basic.hh"

#define _MACRO_CLASS_NAME_ StaticHorsePoolSearch
template <typename T, typename W> class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF   = _MACRO_CLASS_NAME_;
    using TYPE_READER = T;
    using THE_WORD    = W;
    using TYPE_INT    = typename W::TYPE_INT;
    using CHAR        = typename W::CHAR;

  public:
    static inline TYPE_INT constexpr SIZE() { return THE_WORD::SIZE(); }

    static inline CHAR constexpr GET_CHAR(TYPE_INT const i) {
        return THE_WORD::GET_CHAR(i);
    }

    static inline TYPE_INT constexpr GET_JUMP(CHAR const in) {
        return THE_WORD::GET_JUMP(in);
    }

    static inline TYPE_INT constexpr EVAL_FAILED_MATCH_SKIP() {
        CHAR constexpr last = GET_CHAR(0);
        for (TYPE_INT i = 1; i < SIZE(); i++) {
            if (GET_CHAR(i) == last) { return i; }
        }
        return SIZE() - 1;
    }

  private:
    static TYPE_INT constexpr FAILED_MATCH_SKIP = EVAL_FAILED_MATCH_SKIP();
    TYPE_READER &  READER;
    TYPE_INT const BEGIN;
    TYPE_INT const END;
    TYPE_INT       CURRENT;

  private:
    inline bool MATCH() {
        TYPE_INT i = 0;
        TYPE_INT index;

        if ((CURRENT < BEGIN) || (END <= CURRENT)) { return false; }

    LoopStart:

        if (i == SIZE()) { return true; }

        index = CURRENT - i;

        if ((BEGIN <= index) && (index < END) &&
            (READER(index) == GET_CHAR(i))) {
            i++;
            goto LoopStart;
        }

        return false;
    }

    inline TYPE_INT NEXT() {
        CHAR     element;
        TYPE_INT jump;
        while ((BEGIN <= CURRENT) && (CURRENT < END)) {
            element = READER(CURRENT);
            jump    = GET_JUMP(element);
            if (jump != 0) {
                CURRENT += jump;
            } else {
                if (MATCH()) {
                    return CURRENT;
                } else {
                    CURRENT += FAILED_MATCH_SKIP;
                }
            }
        }
        return END;
    }

  public:
    inline TYPE_INT operator()(TYPE_INT const in) {
        CURRENT = in;
        return NEXT();
    }

  public:
    _MACRO_CLASS_NAME_(TYPE_READER &reader, TYPE_INT const begin,
                       TYPE_INT const end)
      : READER(reader), BEGIN(begin), END(end), CURRENT(BEGIN) {}

    _MACRO_CLASS_NAME_(TYPE_READER &reader)
      : READER(reader), BEGIN(0), END(READER()), CURRENT(BEGIN) {}

    _MACRO_CLASS_NAME_(TYPE_READER &reader, TYPE_INT const end)
      : READER(reader), BEGIN(0), END(end), CURRENT(BEGIN) {}

    ~_MACRO_CLASS_NAME_() {}
};
#undef _MACRO_CLASS_NAME_

#endif
