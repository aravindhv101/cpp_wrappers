#ifndef _HEADER_GUARD_CPPFileIO_dir_parallel_run_file_lines_hh_
#define _HEADER_GUARD_CPPFileIO_dir_parallel_run_file_lines_hh_

//////////////////////////////
// Headers related BEGIN: { //
//////////////////////////////
#include "./Atomic_Counter.hh"
#include "./Basic.hh"
#include "./FileLines.hh"
#include "./MyStr.hh"
////////////////////////////
// Headers related END. } //
////////////////////////////

#define _MACRO_CLASS_NAME_ parallel_run_file_lines_slave

/////////////////////////////////
// Main working class BEGIN: { //
/////////////////////////////////

class _MACRO_CLASS_NAME_ {

    //////////////////////////
    // Definitions BEGIN: { //
    //////////////////////////
  public:
    using TYPE_SELF        = _MACRO_CLASS_NAME_;
    using TYPE_INT         = size_t;
    using TYPE_LINE_READER = FileLines<8>;
    using TYPE_STRING      = fast_string<8>;
    using TYPE_CHAR        = typename TYPE_STRING::TYPE_CHAR;
    using ATOMIC_INT       = Atomic_Counter<TYPE_INT>;
    ////////////////////////
    // Definitions END. } //
    ////////////////////////

    /////////////////////////////////
    // Main data elements BEGIN: { //
    /////////////////////////////////
  private:
    std::string const NAME_FILE_CMD;
    TYPE_LINE_READER  LINE_READER;
    ATOMIC_INT        COUNTER;
    ///////////////////////////////
    // Main data elements END. } //
    ///////////////////////////////

    /////////////////////////////////////
    // Main working functions BEGIN: { //
    /////////////////////////////////////
  private:
    inline void DO_RUN(TYPE_CHAR const *in) const {
        system(reinterpret_cast<char const *>(in));
    }

    inline void DO_RUN(TYPE_STRING const in) const {
        DO_RUN(/*const char *in =*/in.get_start());
    }

    inline void DO_RUN() {
        for (TYPE_INT i = (COUNTER++); i < LINE_READER(); i = (COUNTER++)) {
            DO_RUN(/*const TYPE_STRING in =*/LINE_READER(i));
        }
    }

  public:
    inline void operator()() { DO_RUN(); }

  private:
    inline void DO_RUN(TYPE_INT nth) {
        COUNTER = 0;
        std::thread *threads(new std::thread[nth]);
        for (TYPE_INT i = 0; i < nth; i++) {
            threads[i] = std::thread(thread_slave<TYPE_SELF>, this);
        }
        for (TYPE_INT i = 0; i < nth; i++) { threads[i].join(); }
        delete[] threads;
    }
    ///////////////////////////////////
    // Main working functions END. } //
    ///////////////////////////////////

    //////////////////////////
    // Constructor BEGIN: { //
    //////////////////////////
  private:
    _MACRO_CLASS_NAME_(std::string const name_file_cmd)
      : NAME_FILE_CMD(name_file_cmd), LINE_READER(NAME_FILE_CMD), COUNTER(0) {}
    ////////////////////////
    // Constructor END. } //
    ////////////////////////

    //////////////////////////////////////
    // Main exposed interfaces BEGIN: { //
    //////////////////////////////////////
  public:
    static inline void do_run(std::string const name_file_cmd,
                              TYPE_INT const    nth) {
        TYPE_SELF slave(name_file_cmd);
        slave.DO_RUN(/*TYPE_INT nth =*/nth);
    }
    ////////////////////////////////////
    // Main exposed interfaces END. } //
    ////////////////////////////////////
};

///////////////////////////////
// Main working class END. } //
///////////////////////////////

///////////////////////////////////////
// Exposed public interface BEGIN: { //
///////////////////////////////////////
inline void parallel_run_file_lines(std::string const name_file_cmd,
                                    size_t const      nth) {

    _MACRO_CLASS_NAME_::do_run(
      /*const std::string name_file_cmd =*/name_file_cmd,
      /*const TYPE_INT nth =*/nth);
}
/////////////////////////////////////
// Exposed public interface END. } //
/////////////////////////////////////

#undef _MACRO_CLASS_NAME_

#endif
