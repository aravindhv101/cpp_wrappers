#ifndef _HEADER_GUARD_CPPFileIO_dir_interpolation_polynomial_hh_
#define _HEADER_GUARD_CPPFileIO_dir_interpolation_polynomial_hh_

#include "./Basic.hh"

#define _MACRO_CLASS_NAME_ interpolation_polynomial
template <typename TD> class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF  = _MACRO_CLASS_NAME_;
    using TYPE_FLOAT = TD;
    using TYPE_INT   = size_t;

  private:
    std::vector<TYPE_FLOAT> X;
    std::vector<TYPE_FLOAT> Y;
    std::vector<TYPE_FLOAT> D_DELTA;

  private:
    inline size_t LIMIT() const { return mymin(X.size(), Y.size()); }

  private:
    inline TYPE_FLOAT GET_N_DELTA(TYPE_FLOAT const x, TYPE_INT const i) const {
        size_t const limit   = LIMIT();
        TYPE_FLOAT   product = 1;
        for (size_t j = 0; j < limit; j++) {
            if (j != i) { product *= (x - X[j]); }
        }
        return product;
    }

    inline TYPE_FLOAT GET_N_DELTA(TYPE_FLOAT const x, TYPE_INT const i,
                                  TYPE_INT const k) const {

        size_t const limit   = LIMIT();
        TYPE_FLOAT   product = 1;
        for (size_t j = 0; j < limit; j++) {
            if ((j != i) && (j != k)) { product *= (x - X[j]); }
        }
        return product;
    }

    inline TYPE_FLOAT GET_N_DELTA(TYPE_FLOAT const x, TYPE_INT const i,
                                  TYPE_INT const k, TYPE_INT const l) const {

        size_t const limit   = LIMIT();
        TYPE_FLOAT   product = 1;
        for (size_t j = 0; j < limit; j++) {
            if ((j != i) && (j != k) && (j != l)) { product *= (x - X[j]); }
        }
        return product;
    }

    inline TYPE_FLOAT GET_N_DELTA_D(TYPE_FLOAT const x,
                                    TYPE_INT const   i) const {

        size_t const limit = LIMIT();
        TYPE_FLOAT   ret   = 0;
        for (size_t k = 0; k < limit; k++) {
            if (k != i) { ret += GET_N_DELTA(x, i, k); }
        }
        return ret;
    }

    inline TYPE_FLOAT GET_N_DELTA_D2(TYPE_FLOAT const x,
                                     TYPE_INT const   i) const {

        size_t const limit = LIMIT();
        TYPE_FLOAT   ret   = 0;
        for (size_t l = 0; l < limit; l++) {
            if (l != i) {
                for (size_t k = 0; k < limit; k++) {
                    if ((k != i) && (k != l)) {
                        ret += GET_N_DELTA(x, i, k, l);
                    }
                }
            }
        }
        return ret;
    }

  private:
    inline TYPE_FLOAT GET_D_DELTA(TYPE_INT const i) const {
        size_t const limit   = LIMIT();
        TYPE_FLOAT   product = 1;
        for (size_t j = 0; j < limit; j++) {
            if (j != i) { product *= (X[i] - X[j]); }
        }
        return product;
    }

    inline void PREPARE_D_DELTA() {
        size_t const limit = LIMIT();
        D_DELTA.resize(limit);
        for (size_t i = 0; i < limit; i++) { D_DELTA[i] = GET_D_DELTA(i); }
    }

  public:
    inline void append(TYPE_FLOAT const x, TYPE_FLOAT const y) {
        X.push_back(x);
        Y.push_back(y);
        D_DELTA.clear();
    }

    inline void append(TYPE_FLOAT const *const x, TYPE_FLOAT const *const y,
                       size_t const length) {

        for (size_t i = 0; i < length; i++) { append(x[i], y[i]); }
        D_DELTA.clear();
    }

  public:
    inline void prepare() {
        if (D_DELTA.size() == 0) { PREPARE_D_DELTA(); }
    }

  public:
    inline TYPE_FLOAT evaluate(TYPE_FLOAT const x) {
        prepare();
        TYPE_FLOAT   ret   = 0;
        size_t const limit = LIMIT();
        for (size_t i = 0; i < limit; i++) {
            ret += (Y[i] * GET_N_DELTA(x, i)) / D_DELTA[i];
        }
        return ret;
    }

    inline TYPE_FLOAT evaluate_D(TYPE_FLOAT const x) {
        prepare();
        TYPE_FLOAT   ret   = 0;
        size_t const limit = LIMIT();
        for (size_t i = 0; i < limit; i++) {
            ret += (Y[i] * GET_N_DELTA_D(x, i)) / D_DELTA[i];
        }
        return ret;
    }

    inline TYPE_FLOAT evaluate_D2(TYPE_FLOAT const x) {
        prepare();
        TYPE_FLOAT   ret   = 0;
        size_t const limit = LIMIT();
        for (size_t i = 0; i < limit; i++) {
            ret += (Y[i] * GET_N_DELTA_D2(x, i)) / D_DELTA[i];
        }
        return ret;
    }

  public:
    _MACRO_CLASS_NAME_() {}
    ~_MACRO_CLASS_NAME_() {}
};
#undef _MACRO_CLASS_NAME_
#endif
