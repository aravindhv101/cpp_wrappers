#ifndef _HEADER_GUARD_CPPFileIO_MyStr_
#define _HEADER_GUARD_CPPFileIO_MyStr_

#include "../Headers.hh"
#include "./D1.hh"

#define _MACRO_CLASS_NAME_ fast_string

template <TEMPLATES::TYPE::uint1 SIZE_INT =
            sizeof(TEMPLATES::TYPE::int_biggest)>
class _MACRO_CLASS_NAME_ {

    ////////////////////////////////////
    // Important definitions BEGIN: { //
    ////////////////////////////////////
  public:
    using TYPE_INT          = TEMPLATES::TYPE::type_int<SIZE_INT>;
    using TYPE_UINT         = TEMPLATES::TYPE::type_uint<SIZE_INT>;
    using TYPE_CHAR         = TEMPLATES::TYPE::uint1;
    using TYPE_SELF         = _MACRO_CLASS_NAME_<SIZE_INT>;
    using TYPE_SELVES       = std::vector<TYPE_SELF>;
    using TYPE_BUFFER       = dynamic_1d_array<TYPE_CHAR, SIZE_INT>;
    using TYPE_BUFFER_CONST = dynamic_1d_array<TYPE_CHAR const, SIZE_INT>;
    //////////////////////////////////
    // Important definitions END. } //
    //////////////////////////////////

    ////////////////////////////
    // Data elements BEGIN: { //
    ////////////////////////////
  private:
    TYPE_BUFFER BUFFER;
    //////////////////////////
    // Data elements END. } //
    //////////////////////////

    ///////////////////////////////
    // Basic properties BEGIN: { //
    ///////////////////////////////
  public:
    inline TYPE_CHAR       *get_start() { return BUFFER.get_start(); }
    inline TYPE_CHAR       *get_stop() { return BUFFER.get_stop(); }
    inline TYPE_CHAR const *get_start() const { return BUFFER.get_start(); }
    inline TYPE_CHAR const *get_stop() const { return BUFFER.get_stop(); }
    inline TYPE_INT         get_size() const { return BUFFER.get_size(); }
    /////////////////////////////
    // Basic properties END. } //
    /////////////////////////////

    //////////////////////////////
    // Finding related BEGIN: { //
    //////////////////////////////
  public:
    inline TYPE_CHAR const *
    find_char_forward(TYPE_CHAR const ch, TYPE_CHAR const *const start) const {
        if (start < get_stop()) {
            return static_cast<TYPE_CHAR const *>(
              memchr(/*const void *s =*/static_cast<const void *>(start),
                     /*int c =*/int(ch),
                     /*size_t n =*/size_t(get_stop() - start)));
        } else {
            return NULL;
        }
    }

    inline TYPE_CHAR *find_char_forward(TYPE_CHAR const  ch,
                                        TYPE_CHAR *const start) {
        if (start < get_stop()) {
            return static_cast<TYPE_CHAR *>(
              memchr(/*const void *s =*/static_cast<void *>(start),
                     /*int c =*/int(ch),
                     /*size_t n =*/size_t(get_stop() - start)));
        } else {
            return NULL;
        }
    }

  public:
    inline TYPE_CHAR const *
    find_char_backward(TYPE_CHAR const ch, TYPE_CHAR const *const start) const {
        if (start < get_stop()) {
            return static_cast<TYPE_CHAR const *>(
              memchr(start, ch, get_stop() - start));
        } else {
            return NULL;
        }
    }

    inline TYPE_CHAR *find_char_backward(TYPE_CHAR const  ch,
                                         TYPE_CHAR *const start) {
        if (start < get_stop()) {
            return static_cast<TYPE_CHAR *>(
              memchr(start, ch, get_stop() - start));
        } else {
            return NULL;
        }
    }
    ////////////////////////////
    // Finding related END. } //
    ////////////////////////////

    /////////////////////////////////////////
    // Defining equality operator BEGIN: { //
    /////////////////////////////////////////
  public:
    inline void operator=(TYPE_SELF const &other) {
        BUFFER = TYPE_BUFFER::view_this(other.BUFFER);
    }
    inline void operator=(TYPE_SELF &&other) {
        BUFFER = TYPE_BUFFER::own_this(other.BUFFER);
    }
    ///////////////////////////////////////
    // Defining equality operator END. } //
    ///////////////////////////////////////

    ///////////////////////////
    // Constructors BEGIN: { //
    ///////////////////////////
  private:
    _MACRO_CLASS_NAME_(TYPE_BUFFER &&in) : BUFFER(TYPE_BUFFER::own_this(in)) {}
    _MACRO_CLASS_NAME_(TYPE_BUFFER const &in)
      : BUFFER(TYPE_BUFFER::view_this(in)) {}

  public:
    static inline TYPE_SELF from_buffer(TYPE_BUFFER &&in) {
        return TYPE_SELF(TYPE_BUFFER::own_this(in));
    }
    static inline TYPE_SELF from_buffer(TYPE_BUFFER const &in) {
        return TYPE_SELF(TYPE_BUFFER::view_this(in));
    }

  public:
    _MACRO_CLASS_NAME_() {}

    _MACRO_CLASS_NAME_(TYPE_SELF &&in)
      : _MACRO_CLASS_NAME_(TYPE_BUFFER::own_this(in.BUFFER)) {}

    _MACRO_CLASS_NAME_(TYPE_SELF const &in)
      : _MACRO_CLASS_NAME_(TYPE_BUFFER::view_this(in.BUFFER)) {}

  public:
    static inline TYPE_SELF get_new(TYPE_UINT const in) {
        return TYPE_SELF::from_buffer(
          /*TYPE_BUFFER &&in =*/TYPE_BUFFER::get_new(in));
    }

  public:
    static inline TYPE_SELF view_this(TYPE_CHAR *const data,
                                      TYPE_UINT const  size_data) {
        return from_buffer(TYPE_BUFFER::view_this(data, size_data));
    }

    static inline TYPE_SELF view_this(TYPE_SELF const &in) {
        return from_buffer(TYPE_BUFFER::view_this(in.BUFFER));
    }

  public:
    static inline TYPE_SELF view_range(TYPE_CHAR *const start,
                                       TYPE_CHAR *const stop) {
        return from_buffer(TYPE_BUFFER::view_range(start, stop));
    }

  public:
    static inline TYPE_SELF own_this(TYPE_CHAR *const data,
                                     TYPE_UINT const  size_data) {
        return from_buffer(TYPE_BUFFER::own_this(data, size_data));
    }

    static inline TYPE_SELF own_this(TYPE_SELF &in) {
        return from_buffer(TYPE_BUFFER::own_this(in.BUFFER));
    }

    static inline TYPE_SELF own_this(TYPE_SELF &&in) {
        return from_buffer(TYPE_BUFFER::own_this(in.BUFFER));
    }

    static inline TYPE_SELF own_this(TYPE_SELF const &in) {
        return view_this(in);
    }

  public:
    static inline TYPE_SELF own_range(TYPE_CHAR *const start,
                                      TYPE_CHAR *const stop) {
        return own_this(/*TYPE_DATA * data =*/start,
                        /*TYPE_UINT const size_data =*/(stop - start));
    }

  public:
    static inline TYPE_SELF copy_this(TYPE_CHAR const *const data,
                                      TYPE_UINT const        size_data) {
        return from_buffer(TYPE_BUFFER::copy_this(data, size_data));
    }

    static inline TYPE_SELF copy_this(TYPE_SELF const &in) {
        return from_buffer(TYPE_BUFFER::copy_this(in.BUFFER));
    }

  public:
    static inline TYPE_SELF copy_range(TYPE_CHAR const *const start,
                                       TYPE_CHAR const *const stop) {
        return copy_this(/*TYPE_DATA * data =*/start,
                         /*TYPE_UINT const size_data =*/(stop - start));
    }

  public:
    static inline TYPE_SELF view_string(std::string &in) {
        TYPE_CHAR *start = reinterpret_cast<TYPE_CHAR *>(&(in[0]));
        return view_this(start, in.size());
    }
    /////////////////////////
    // Constructors END. } //
    /////////////////////////

    //////////////////////////////////
    // Comparing functions BEGIN: { //
    //////////////////////////////////
  public:
    inline int compare_start(TYPE_BUFFER const &in) const {
        return BUFFER.compare_start(in);
    }
    inline int compare_stop(TYPE_BUFFER const &in) const {
        return BUFFER.compare_stop(in);
    }
    ////////////////////////////////
    // Comparing functions END. } //
    ////////////////////////////////

  public:
    inline void split(TYPE_SELVES &lines, TYPE_CHAR const d,
                      bool const terminate_by_null = false) {

        TYPE_CHAR *const start = get_start();
        TYPE_CHAR *const stop  = get_stop();

        std::vector<TYPE_CHAR *> boundaries;

        for (TYPE_CHAR *current =
               find_char_forward(/*const TYPE_CHAR ch =*/d,
                                 /*TYPE_CHAR *const start =*/start);

             ((current != NULL) && (current < stop));

             current =
               find_char_forward(/*const TYPE_CHAR ch =*/d,
                                 /*TYPE_CHAR *const start =*/current + 1)) {

            if (terminate_by_null) { *current = 0; }

            boundaries.push_back(current);
        }

        boundaries.push_back(stop);

        lines.resize(boundaries.size());

        lines[0] = view_range(/*TYPE_CHAR *const start =*/start,
                              /*TYPE_CHAR *const stop =*/boundaries[0]);

        for (TYPE_UINT i = 1; i < boundaries.size(); i++) {
            lines[i] = view_range(
              /*TYPE_CHAR *const start =*/boundaries[i - 1] + 1,
              /*TYPE_CHAR *const stop =*/boundaries[i]);
        }
    }
};

#undef _MACRO_CLASS_NAME_

#define _MACRO_CLASS_NAME_ MyStr
template <typename TI = long> class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_INT          = TI;
    using TYPE_SELF         = _MACRO_CLASS_NAME_<TYPE_INT>;
    using TYPE_SELVES       = std::vector<TYPE_SELF>;
    using TYPE_BUFFER       = Dynamic1DArray<char, TYPE_INT>;
    using TYPE_BUFFER_CONST = Dynamic1DArray<char const, TYPE_INT>;

  private:
    char *START;     // point to 1st address in the array.

    char *STOP;      // Point to the final null character in the array.
                     // = START + LENGTH.

    TYPE_INT LENGTH; // length of the string,
                     // excluding the null terminating byte.
                     // = END - START.

  public:
    inline char       *start() { return START; }
    inline char       *stop() { return STOP; }
    inline char const *start() const { return START; }
    inline char const *stop() const { return STOP; }
    inline TYPE_INT    length() const { return LENGTH; }

  private:
    inline char const *FIND_CHAR(char const ch, char const *start) const {
        return static_cast<char const *>(memchr(start, ch, STOP - start));
    }

    inline void CUT(TYPE_SELVES &ret, char const in) {
        char *start = START;
        char *end;

    MainLoop:
        if (true) /* The main cutting loop: */ {

            end = static_cast<char *>(memchr(start, in, STOP - start));

            if (end) {
                (*end) = 0;
                ret.push_back(TYPE_SELF(start, end));
                start = end + 1;
                if (start < STOP) { goto MainLoop; }
            } else if (start < STOP) {
                ret.push_back(TYPE_SELF(start, STOP));
            }
        }
    }

  public:
    inline void tr(char const from, char const to) {
        char *current  = START;
        bool  loopover = (START <= current) && (current < STOP);

        if (true) /* Main Loop Part: */ {
        LoopStart:
            current =
              static_cast<char *>(memchr(current, from, STOP - current));

            loopover =
              (START <= current) && (current < STOP) && (current != NULL);

            if (loopover) {
                *current = to;
                current++;
                goto LoopStart;
            }
        }
    }

    inline TYPE_INT find_char(char const ch, TYPE_INT const start) const {
        char const *res = FIND_CHAR(ch, START + start);
        if (res != NULL) {
            return res - START;
        } else {
            return LENGTH + 1;
        }
    }

    inline TYPE_BUFFER clone_range(TYPE_INT const begin,
                                   TYPE_INT const size) const {

        TYPE_INT const end = mymin(begin + size, STOP - START);
        TYPE_BUFFER    ret(size + 1);
        size_t         i = begin;

        while (i < end) {
            ret(i - begin) = START[i];
            i++;
        }

        i -= begin;

        while (i < size) {
            ret(i) = 0;
            i++;
        }

        return ret;
    }

  public:
    inline TYPE_BUFFER operator()() {
        TYPE_BUFFER ret(START, LENGTH);
        return ret;
    }

    inline TYPE_BUFFER_CONST operator()() const {
        TYPE_BUFFER_CONST const ret(START, LENGTH);
        return ret;
    }

    inline TYPE_SELVES operator()(char const in) {
        TYPE_SELVES ret;
        CUT(ret, in);
        return ret;
    }

    inline TYPE_SELVES cut(char const in) {
        TYPE_SELVES ret;
        CUT(ret, in);
        return ret;
    }

    inline void operator()(TYPE_SELVES &ret, char const in) {
        ret.clear();
        CUT(ret, in);
    }

    inline void operator()(char *const buffer, TYPE_INT const length) {
        START  = buffer;
        LENGTH = length;
        STOP   = START + LENGTH;
    }

    inline void operator()(char *const start, char *const end) {
        START  = start;
        STOP   = end;
        LENGTH = end - start;
    }

    inline void operator()(char *const buffer) {
        this[0](buffer, strlen(buffer));
    }

  public:
    _MACRO_CLASS_NAME_(char *const buffer) { this[0](buffer); }

    _MACRO_CLASS_NAME_(char *const start, char *const stop) {
        this[0](start, stop);
    }

    _MACRO_CLASS_NAME_(char *const buffer, TYPE_INT const length) {
        this[0](buffer, length);
    }

    _MACRO_CLASS_NAME_(std::string &in) { this[0](&(in[0]), in.size()); }

    _MACRO_CLASS_NAME_(TYPE_BUFFER &in) { this[0](in.GET_DATA(), in()); }

    _MACRO_CLASS_NAME_() {}

    ~_MACRO_CLASS_NAME_() {}
};
#undef _MACRO_CLASS_NAME_

#endif
