#ifndef _HEADER_GUARD_CPPFileIO_dir_find_stand_alone_hh_
#define _HEADER_GUARD_CPPFileIO_dir_find_stand_alone_hh_

#include "./Basic.hh"

/////////////////////////////////////////////////////////////////
// Function to check if end of a string matches input BEGIN: { //
/////////////////////////////////////////////////////////////////
inline bool match_end(std::string const inname, std::string const inpattern) {
    if (inname.size() < inpattern.size()) { return false; }
    char const *buf = &(inname[inname.size() - inpattern.size()]);
    return (strcmp(buf, inpattern.c_str()) == 0);
}
///////////////////////////////////////////////////////////////
// Function to check if end of a string matches input END. } //
///////////////////////////////////////////////////////////////

#define _MACRO_CLASS_NAME_ find_stand_alone

/////////////////////////////////
// Main working class BEGIN: { //
/////////////////////////////////

class _MACRO_CLASS_NAME_ {

    //////////////////////////
    // Definitions BEGIN: { //
    //////////////////////////
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    using string    = std::string;
    using strings   = std::vector<string>;
    ////////////////////////
    // Definitions END. } //
    ////////////////////////

    /////////////////////////////////////////
    // Main data to hold the list BEGIN: { //
    /////////////////////////////////////////
  private:
    strings contents;
    ///////////////////////////////////////
    // Main data to hold the list END. } //
    ///////////////////////////////////////

    ////////////////////////////////
    // Main ls functions BEGIN: { //
    ////////////////////////////////
  public:
    inline strings const &ls_all(std::string dirname) {

        contents.clear();

        std::string d_name;
        std::string final_name;

        DIR *indir = opendir(/*const char *name = */ dirname.c_str());

        /* struct */ dirent *res;

    LoopLabel:
        if (true) {
            res = readdir(/* DIR *dirp = */ indir);
            if (res != NULL) {
                contents.push_back(res->d_name);
                goto LoopLabel;
            }
        }

        /* int const ret = */ closedir(/* DIR *dirp = */ indir);

        return contents;
    }

    inline strings const &ls_file(std::string dirname) {

        contents.clear();

        std::string d_name;
        std::string final_name;

        DIR *indir = opendir(/*const char *name = */ dirname.c_str());

        /* struct */ dirent *res;

    LoopLabel:
        if (true) {
            res = readdir(/* DIR *dirp = */ indir);
            if ((res != NULL) && (res->d_type == DT_REG)) {
                contents.push_back(res->d_name);
                goto LoopLabel;
            }
        }

        /* int const ret = */ closedir(/* DIR *dirp = */ indir);

        return contents;
    }

    inline strings const &ls_link(std::string dirname) {

        contents.clear();

        std::string d_name;
        std::string final_name;

        DIR *indir = opendir(/*const char *name = */ dirname.c_str());

        /* struct */ dirent *res;

    LoopLabel:
        if (true) {
            res = readdir(/* DIR *dirp = */ indir);
            if ((res != NULL) && (res->d_type == DT_LNK)) {
                contents.push_back(res->d_name);
                goto LoopLabel;
            }
        }

        /* int const ret = */ closedir(/* DIR *dirp = */ indir);

        return contents;
    }

    inline strings const &ls_dir(std::string dirname) {

        contents.clear();

        std::string d_name;
        std::string final_name;

        DIR *indir = opendir(/*const char *name = */ dirname.c_str());

        /* struct */ dirent *res;

    LoopLabel:
        if (true) {
            res = readdir(/* DIR *dirp = */ indir);
            if ((res != NULL) && (res->d_type == DT_DIR)) {
                contents.push_back(res->d_name);
                goto LoopLabel;
            }
        }

        /* int const ret = */ closedir(/* DIR *dirp = */ indir);

        return contents;
    }
    //////////////////////////////
    // Main ls functions END. } //
    //////////////////////////////

    /////////////////////////////////////
    // Main finding functions BEGIN: { //
    /////////////////////////////////////
  private:
    inline void FIND_ALL(std::string dirname) {

        contents.clear();

        std::stack<std::string> dirs;
        dirs.push(dirname);
        /* struct */ dirent *res;
        DIR *                indir;
        std::string          d_name;
        std::string          final_name;

        while (dirs.size() > 0) {

            dirname = dirs.top();
            dirs.pop();
            indir = opendir(/*const char *name = */ dirname.c_str());

        LoopLabel:
            if (true) {

                res = readdir(/* DIR *dirp = */ indir);

                if (res != NULL) {

                    d_name     = res->d_name;
                    final_name = dirname + "/" + res->d_name;

                    if (res->d_type == DT_DIR) {
                        if ((d_name != ".") && (d_name != "..")) {
                            contents.push_back(final_name);
                            dirs.push(final_name);
                        }
                    } else {
                        contents.push_back(final_name);
                    }

                    goto LoopLabel;
                }
            }

            /* int const ret = */ closedir(/* DIR *dirp = */ indir);
        }
    }

    inline void FIND_FILE(std::string dirname) {

        contents.clear();

        std::stack<std::string> dirs;
        dirs.push(dirname);
        /* struct */ dirent *res;
        DIR *                indir;
        std::string          d_name;
        std::string          final_name;

        while (dirs.size() > 0) {

            dirname = dirs.top();
            dirs.pop();
            indir = opendir(/*const char *name = */ dirname.c_str());

        LoopLabel:
            if (true) {

                res = readdir(/* DIR *dirp = */ indir);

                if (res != NULL) {

                    d_name     = res->d_name;
                    final_name = dirname + "/" + res->d_name;

                    if (res->d_type == DT_DIR) {
                        if ((d_name != ".") && (d_name != "..")) {
                            dirs.push(final_name);
                        }
                    } else if (res->d_type == DT_REG) {
                        contents.push_back(final_name);
                    }

                    goto LoopLabel;
                }
            }

            /* int const ret = */ closedir(/* DIR *dirp = */ indir);
        }
    }

    inline void FIND_LINK(std::string dirname) {

        contents.clear();

        std::stack<std::string> dirs;
        dirs.push(dirname);
        /* struct */ dirent *res;
        DIR *                indir;
        std::string          d_name;
        std::string          final_name;

        while (dirs.size() > 0) {

            dirname = dirs.top();
            dirs.pop();
            indir = opendir(/*const char *name = */ dirname.c_str());

        LoopLabel:
            if (true) {

                res = readdir(/* DIR *dirp = */ indir);

                if (res != NULL) {

                    d_name     = res->d_name;
                    final_name = dirname + "/" + res->d_name;

                    if (res->d_type == DT_DIR) {
                        if ((d_name != ".") && (d_name != "..")) {
                            dirs.push(final_name);
                        }
                    } else if (res->d_type == DT_LNK) {
                        contents.push_back(final_name);
                    }

                    goto LoopLabel;
                }
            }

            /* int const ret = */ closedir(/* DIR *dirp = */ indir);
        }
    }

    inline void FIND_DIR(std::string dirname) {

        contents.clear();

        std::stack<std::string> dirs;
        dirs.push(dirname);
        /* struct */ dirent *res;
        DIR *                indir;
        std::string          d_name;
        std::string          final_name;

        while (dirs.size() > 0) {

            dirname = dirs.top();
            dirs.pop();
            indir = opendir(/*const char *name = */ dirname.c_str());

        LoopLabel:
            if (true) {

                res = readdir(/* DIR *dirp = */ indir);

                if (res != NULL) {

                    d_name     = res->d_name;
                    final_name = dirname + "/" + res->d_name;

                    if (res->d_type == DT_DIR) {
                        if ((d_name != ".") && (d_name != "..")) {
                            contents.push_back(final_name);
                            dirs.push(final_name);
                        }
                    }

                    goto LoopLabel;
                }
            }

            /* int const ret = */ closedir(/* DIR *dirp = */ indir);
        }
    }
    ///////////////////////////////////
    // Main finding functions END. } //
    ///////////////////////////////////

    /////////////////////////////
    // Main interface BEGIN: { //
    /////////////////////////////
  public:
    inline strings const &all(std::string const name_dir_input) {
        FIND_ALL(name_dir_input);
        return contents;
    }

    inline strings const &file(std::string const name_dir_input) {
        FIND_FILE(name_dir_input);
        return contents;
    }

    inline strings const &link(std::string const name_dir_input) {
        FIND_LINK(name_dir_input);
        return contents;
    }

    inline strings const &dir(std::string const name_dir_input) {
        FIND_DIR(name_dir_input);
        return contents;
    }
    ///////////////////////////
    // Main interface END. } //
    ///////////////////////////
};

///////////////////////////////
// Main working class END. } //
///////////////////////////////

#undef _MACRO_CLASS_NAME_

#endif
