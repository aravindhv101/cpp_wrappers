﻿#ifndef _HEADER_GUARD_CPPFileIO_FileLines_
#define _HEADER_GUARD_CPPFileIO_FileLines_

/////////////////////
// Includes BEGIN: //
/////////////////////
#include "./FileFD.hh"
#include "./FullFileReader.hh"
#include "./MyStr.hh"
///////////////////
// Includes END. //
///////////////////

#define _MACRO_CLASS_NAME_ file_lines

template <TEMPLATES::TYPE::uint_smallest SIZE_INT =
            sizeof(TEMPLATES::TYPE::uint_biggest)>
class _MACRO_CLASS_NAME_ {

  public:
    using TYPE_UINT   = TEMPLATES::TYPE::type_uint<SIZE_INT>;
    using TYPE_INT    = TEMPLATES::TYPE::type_int<SIZE_INT>;
    using TYPE_CHAR   = TEMPLATES::TYPE::uint_smallest;
    using TYPE_READER = full_file_reader<TYPE_CHAR>;
    using TYPE_SELF   = _MACRO_CLASS_NAME_<SIZE_INT>;
    using TYPE_STRING = fast_string<SIZE_INT>;
    using TYPE_LINES  = typename TYPE_STRING::TYPE_SELVES;

  private:
    TYPE_CHAR const SEPERATOR;
    TYPE_READER     READER;
    TYPE_STRING     STRING;
    TYPE_LINES      LINES;

  public:
    inline TYPE_INT     operator()() const { return LINES.size(); }
    inline TYPE_STRING &operator()(TYPE_UINT const in) { return LINES[in]; }
    inline TYPE_STRING const &operator()(TYPE_UINT const in) const {
        return LINES[in];
    }

  public:
    _MACRO_CLASS_NAME_(std::string const filename, char const seperator = '\n')
      : SEPERATOR(seperator), READER(filename),
        STRING(
          TYPE_STRING::view_range(/*TYPE_CHAR *start =*/READER.get_start(),
                                  /*TYPE_CHAR *stop =*/READER.get_stop())) {

        STRING.split(LINES, SEPERATOR, true);
    }

    ~_MACRO_CLASS_NAME_() {}
};

#undef _MACRO_CLASS_NAME_

template <TEMPLATES::TYPE::uint_smallest SIZE>
using FileLines = file_lines<SIZE>;

template <TEMPLATES::TYPE::uint_smallest SIZE>
using tmp_file_lines = FileLines<SIZE>;

#endif
