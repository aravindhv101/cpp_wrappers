﻿#ifndef _HEADER_GUARD_CPPFileIO_Sorter_
#define _HEADER_GUARD_CPPFileIO_Sorter_

////////////////////
// Headers BEGIN: //
////////////////////
#include "../Headers.hh"
#include "./Basic.hh"
#include "./ExternalPrograms.hh"
#include "./FileArray.hh"
#include "./FileFD.hh"
#include "./FileWriter.hh"
#include "./find_executables.hh"
#include "./Atomic_Counter.hh"
#include "./FileSplitter.hh"
#include "./find_executables.hh"
//////////////////
// Headers END. //
//////////////////

/////////////////////////////////////
// Simple sorting of file BEGIN: { //
/////////////////////////////////////
#define _MACRO_CLASS_NAME_ SortFileSlave

template <typename T> class _MACRO_CLASS_NAME_ {

    //////////////////////////
    // Definitions BEGIN: { //
    //////////////////////////
  public:
    using TYPE_SELF    = _MACRO_CLASS_NAME_;
    using TYPE_ELEMENT = T;
    using TYPE_ARRAY   = FileArray<TYPE_ELEMENT>;
    ////////////////////////
    // Definitions END. } //
    ////////////////////////

    ////////////////////////////
    // Data elements BEGIN: { //
    ////////////////////////////
  private:
    std::string const NAME_FILE_BIN;
    TYPE_ARRAY        ARRAY;
    size_t const      LIMIT;
    TYPE_ELEMENT *    ELEMENTS;
    //////////////////////////
    // Data elements END. } //
    //////////////////////////

    ////////////////////////////////
    // WRAPPER functions BEGIN: { //
    ////////////////////////////////
  private:
    inline void MAP_DATA() {
        ARRAY.writeable(true);
        ELEMENTS = &(ARRAY(0, LIMIT));
    }

    inline void SORT_DATA() { std::sort(ELEMENTS, ELEMENTS + LIMIT); }

    inline void WORK() {
        MAP_DATA();
        SORT_DATA();
    }

    inline void DESTROY() {
        ARRAY.size(LIMIT);
        ARRAY.writeable(false);
    }
    //////////////////////////////
    // WRAPPER functions END. } //
    //////////////////////////////

    ///////////////////////////////////////
    // Constructor & Destructor BEGIN: { //
    ///////////////////////////////////////
  private:
    _MACRO_CLASS_NAME_(std::string const name_file_bin)
      : NAME_FILE_BIN(name_file_bin), ARRAY(NAME_FILE_BIN), LIMIT(ARRAY()) {}

    ~_MACRO_CLASS_NAME_() { DESTROY(); }
    /////////////////////////////////////
    // Constructor & Destructor END. } //
    /////////////////////////////////////

    ////////////////////////////////
    // Dynamic allocator BEGIN: { //
    ////////////////////////////////
  private:
    static inline TYPE_SELF *NEW(std::string const name_file_bin) {
        TYPE_SELF *ret = new TYPE_SELF(name_file_bin);
        return ret;
    }
    //////////////////////////////
    // Dynamic allocator END. } //
    //////////////////////////////

    ////////////////////////////////
    // Exposed interface BEGIN: { //
    ////////////////////////////////
  public:
    static inline void work(std::string const name_file_bin) {
        auto ref = NEW(name_file_bin);
        ref->WORK();
        delete ref;
    }
    //////////////////////////////
    // Exposed interface END. } //
    //////////////////////////////
};

template <typename T> inline void SortFile(std::string const filename) {
    _MACRO_CLASS_NAME_<T>::work(filename);
}

#undef _MACRO_CLASS_NAME_
///////////////////////////////////
// Simple sorting of file END. } //
///////////////////////////////////

/////////////////////////////////////////////////
// Merging file without deduplication BEGIN: { //
/////////////////////////////////////////////////
#define _MACRO_CLASS_NAME_ MergeFileSlave

template <typename T> class _MACRO_CLASS_NAME_ {
    //////////////////////////
    // Definitions BEGIN: { //
    //////////////////////////
  public:
    using TYPE_SELF    = _MACRO_CLASS_NAME_;
    using TYPE_ELEMENT = T;
    using TYPE_READER  = FileArray<TYPE_ELEMENT const>;
    using TYPE_WRITER  = FileWriter<TYPE_ELEMENT>;
    ////////////////////////
    // Definitions END. } //
    ////////////////////////

    ////////////////////////////
    // Data elements BEGIN: { //
    ////////////////////////////
  private:
    std::string const NAME_FILE_BIN_INPUT_1;
    std::string const NAME_FILE_BIN_INPUT_2;
    std::string const NAME_FILE_BIN_OUTPUT;
    TYPE_READER       R1;
    TYPE_READER       R2;
    TYPE_WRITER       W;
    size_t const      L1;
    size_t const      L2;
    size_t            I1, I2;
    //////////////////////////
    // Data elements END. } //
    //////////////////////////

    ///////////////////////////////////
    // Main mering function BEGIN: { //
    ///////////////////////////////////
  private:
    inline void WORK() {
        while ((I1 < L1) && (I2 < L2)) {
            T const &V1 = R1(I1);
            T const &V2 = R2(I2);

            if (V1 < V2) {
                W(V1);
                I1++;
            } else {
                W(V2);
                I2++;
            }
        }

        while (I1 < L1) {
            W(R1(I1));
            I1++;
        }

        while (I2 < L2) {
            W(R2(I2));
            I2++;
        }
    }
    /////////////////////////////////
    // Main mering function END. } //
    /////////////////////////////////

    ///////////////////////////////////////
    // Constructor & Destructor BEGIN: { //
    ///////////////////////////////////////
  private:
    _MACRO_CLASS_NAME_(std::string const name_file_bin_input_1,
                       std::string const name_file_bin_input_2,
                       std::string const name_file_bin_output)
      : NAME_FILE_BIN_INPUT_1(name_file_bin_input_1),
        NAME_FILE_BIN_INPUT_2(name_file_bin_input_2),
        NAME_FILE_BIN_OUTPUT(name_file_bin_output), R1(NAME_FILE_BIN_INPUT_1),
        R2(NAME_FILE_BIN_INPUT_2), W(NAME_FILE_BIN_OUTPUT), L1(R1()), L2(R2()),
        I1(0), I2(0) {}
    /////////////////////////////////////
    // Constructor & Destructor END. } //
    /////////////////////////////////////

    //////////////////////////////////////////
    // Public interface to merging BEGIN: { //
    //////////////////////////////////////////
  public:
    static inline void work(std::string const name_file_bin_input_1,
                            std::string const name_file_bin_input_2,
                            std::string const name_file_bin_output) {

        TYPE_SELF *slave = new TYPE_SELF(
          name_file_bin_input_1, name_file_bin_input_2, name_file_bin_output);

        slave->WORK();

        delete slave;
    }
    ////////////////////////////////////////
    // Public interface to merging END. } //
    ////////////////////////////////////////
};

template <typename T>
inline void MergeFile(std::string const file1, std::string const file2,
                      std::string const fileo) {

    _MACRO_CLASS_NAME_<T>::work(file1, file2, fileo);
}

#undef _MACRO_CLASS_NAME_

template <typename T>
inline void MergeFile_old(std::string const file1, std::string const file2,
                          std::string const fileo) {

    FileArray<T>  reader1(file1);
    FileArray<T>  reader2(file2);
    FileWriter<T> writer(fileo);

    size_t const limit1 = reader1.size();
    size_t const limit2 = reader2.size();

    size_t i1 = 0;
    size_t i2 = 0;

    while ((i1 < limit1) && (i2 < limit2)) {
        T const &V1 = reader1(i1);
        T const &V2 = reader2(i2);

        if (V1 < V2) {
            writer(V1);
            i1++;
        } else {
            writer(V2);
            i2++;
        }
    }

    while (i1 < limit1) {
        writer(reader1(i1));
        i1++;
    }

    while (i2 < limit2) {
        writer(reader2(i2));
        i2++;
    }
}
///////////////////////////////////////////////
// Merging file without deduplication END. } //
///////////////////////////////////////////////

////////////////////////////////////////////
// Merge file with deduplication BEGIN: { //
////////////////////////////////////////////
#define _MACRO_CLASS_NAME_ MergeFileDeDupSlave

template <typename TD, typename TI = long> class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_DATA   = TD;
    using TYPE_INT    = TI;
    using TYPE_SELF   = _MACRO_CLASS_NAME_<TYPE_DATA, TYPE_INT>;
    using TYPE_READER = FileArray<TYPE_DATA>;
    using TYPE_WRITER = FileWriter<TYPE_DATA>;

  private:
    std::string const F1, F2, F0;
    TYPE_READER       R1, R2;
    TYPE_WRITER       W;
    size_t const      L1, L2;
    TYPE_DATA         P;
    size_t            I1, I2;

  private:
    inline void WRITE(TYPE_DATA const &in) {
        if (in != P) {
            W(in);
            P = in;
        }
    }

    inline void WRITE() {
        while ((I1 < L1) && (I2 < L2)) {
            TYPE_DATA const &V1 = R1(I1);
            TYPE_DATA const &V2 = R2(I2);

            if (V1 < V2) {
                WRITE(V1);
                I1++;
            } else {
                WRITE(V2);
                I2++;
            }
        }

        while (I1 < L1) {
            WRITE(R1(I1));
            I1++;
        }

        while (I2 < L2) {
            WRITE(R2(I2));
            I2++;
        }
    }

  private:
    _MACRO_CLASS_NAME_(std::string const f1, std::string const f2,
                       std::string const f0)
      : F1(f1), F2(f2), F0(f0), R1(F1), R2(F2), W(F0), L1(R1()), L2(R2()),
        I1(0), I2(0) {}

  public:
    static inline void work(std::string const f1, std::string const f2,
                            std::string const f0) {

        TYPE_SELF *slave = new TYPE_SELF(f1, f2, f0);
        slave->WRITE();
        delete slave;
    }
};

template <typename TD, typename TI = long>
inline void MergeFileDeDup(std::string const f1, std::string const f2,
                           std::string const f0) {
    _MACRO_CLASS_NAME_<TD, TI>::work(f1, f2, f0);
}

#undef _MACRO_CLASS_NAME_
//////////////////////////////////////////
// Merge file with deduplication END. } //
//////////////////////////////////////////

//////////////////////////////////////
// Important wrapper slave BEGIN: { //
//////////////////////////////////////
#define _MACRO_CLASS_NAME_ sort_and_merge_slave

template <typename T, bool DOSORT = false, bool DEDUP = true>
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF    = _MACRO_CLASS_NAME_;
    using TYPE_ELEMENT = T;

  public:
    static inline void SORT(std::string const name_file_bin_input) {
        if (DOSORT) { SortFile<T>(name_file_bin_input); }
    }

    static inline void MERGE(std::string const name_file_bin_input_1,
                             std::string const name_file_bin_input_2,
                             std::string const name_file_bin_output) {

        if (DEDUP) {
            MergeFileDeDup<TYPE_ELEMENT>(name_file_bin_input_1,
                                         name_file_bin_input_2,
                                         name_file_bin_output);
        } else {
            MergeFile<TYPE_ELEMENT>(name_file_bin_input_1,
                                    name_file_bin_input_2,
                                    name_file_bin_output);
        }
    }
};

#undef _MACRO_CLASS_NAME_
////////////////////////////////////
// Important wrapper slave END. } //
////////////////////////////////////

//////////////////////////////////////
// Class to sort and merge BEGIN: { //
//////////////////////////////////////
#define _MACRO_CLASS_NAME_ sort_and_merge
template <typename T> class _MACRO_CLASS_NAME_ {

    //////////////////////////
    // Definitions BEGIN: { //
    //////////////////////////
  public:
    using TYPE_SELF    = _MACRO_CLASS_NAME_;
    using TYPE_STRINGS = std::vector<std::string>;
    using TYPE_SLAVE   = T;
    using ATOMIC_INT   = Atomic_Counter<size_t>;
    ////////////////////////
    // Definitions END. } //
    ////////////////////////

    ////////////////////////////
    // Data elements BEGIN: { //
    ////////////////////////////
  private:
    TYPE_STRINGS const &INPUTS;  // List of input files.
    std::string const   OUTPUT;  // The output file.
    std::string const   DIRNAME; // Directory to hold temporary files.
    bool const          CLEANUP; // Need to cleanup ?
    size_t const        NTH;
    ATOMIC_INT          COUNTER;
    std::thread *       THREADS;
    TYPE_SLAVE *        SLAVE;
    //////////////////////////
    // Data elements END. } //
    //////////////////////////

    ////////////////////////////////////////////////////
    // Important file manipulation functions BEGIN: { //
    ////////////////////////////////////////////////////
  private:
    static inline void RM(std::string const filename) {
        /*int ret = */ unlink(
          /* const char *pathname = */
          filename.c_str());
    }

    static inline void RMDIR(std::string const filename) {
        /* int ret = */ rmdir(
          /* const char *pathname = */
          filename.c_str());
    }

    static inline void MV(std::string const src, std::string const dst) {
        RM(dst);

        int res = link(
          /* const char *oldpath = */ src.c_str(),
          /* const char *newpath = */ dst.c_str());

        if (res == 0) {
            RM(src);
        } else {
            find_executable   finder;
            std::string const mv_exe = finder("/mv");
            if (mv_exe.length() > 1) {
                ExternalStarter<true>::GET(mv_exe)("-vf")("--")(src)(dst);
            }
        }
    }
    //////////////////////////////////////////////////
    // Important file manipulation functions END. } //
    //////////////////////////////////////////////////

    //////////////////////////////////
    // Filename generation BEGIN: { //
    //////////////////////////////////
  private:
    inline std::string GET_FILE_NAME(size_t const i) const { return INPUTS[i]; }

    inline std::string GET_FILE_NAME(size_t const i1, size_t const i2) const {
        if (i1 == i2) {
            return GET_FILE_NAME(i1);
        } else {
            mkdir(DIRNAME.c_str(), 0755);
            char tmp[128];
            sprintf(tmp, "/bin.%zu-%zu.sorted", i1, i2);
            std::string ret = DIRNAME + std::string(tmp);
            return ret;
        }
    }
    ////////////////////////////////
    // Filename generation END. } //
    ////////////////////////////////

    ////////////////////////////////////
    // Clean temporary files BEGIN: { //
    ////////////////////////////////////
  private:
    inline void REMOVE_FILE(size_t const i1) {
        if (CLEANUP) { RM(GET_FILE_NAME(i1)); }
    }

    inline void REMOVE_FILE(size_t const i1, size_t const i2) {
        if (i1 == i2) {
            REMOVE_FILE(i1);
        } else {
            RM(GET_FILE_NAME(i1, i2));
        }
    }
    //////////////////////////////////
    // Clean temporary files END. } //
    //////////////////////////////////

    ////////////////////////////////
    // Sorting the files BEGIN: { //
    ////////////////////////////////
  private:
    inline void SORT(std::string const name_file_bin) {
        if (SLAVE != NULL) {
            SLAVE->SORT(name_file_bin);
        } else {
            TYPE_SLAVE::SORT(name_file_bin);
        }
    }

    inline void SORT(size_t const index) { SORT(GET_FILE_NAME(index)); }

    inline void MERGE(std::string const name_file_bin_input_1,
                      std::string const name_file_bin_input_2,
                      std::string const name_file_bin_output) {

        if (SLAVE != NULL) {
            SLAVE->MERGE(name_file_bin_input_1, name_file_bin_input_2,
                         name_file_bin_output);
        } else {
            TYPE_SLAVE::MERGE(name_file_bin_input_1, name_file_bin_input_2,
                              name_file_bin_output);
        }
    }

    inline void MERGE(size_t const i1, size_t const i2) {
        MERGE(GET_FILE_NAME(i1), GET_FILE_NAME(i2), GET_FILE_NAME(i1, i2));
    }

    inline void MERGE(size_t const i1, size_t const mid, size_t const i2) {
        MERGE(GET_FILE_NAME(i1, mid), GET_FILE_NAME(mid + 1, i2),
              GET_FILE_NAME(i1, i2));
    }
    //////////////////////////////
    // Sorting the files END. } //
    //////////////////////////////

    ////////////////////////////////////
    // Main merging function BEGIN: { //
    ////////////////////////////////////
  private:
    inline void MERGE_RANGE(size_t const i1, size_t const i2,
                            size_t const dp = 0) {

        size_t const diff = i2 - i1;

        switch (diff) {
            case 0:
                if (true) { SORT(i1); }
                return;

            case 1:
                if (true) {
                    size_t const ath = shifter(dp) >> 1;

                    if (ath <= NTH) {
                        ForkMe forker;
                        if (forker.InKid()) {
                            SORT(i1);
                        } else {
                            SORT(i2);
                        }
                    } else {
                        SORT(i1);
                        SORT(i2);
                    }

                    MERGE(i1, i2);

                    REMOVE_FILE(i1);
                    REMOVE_FILE(i2);
                }
                return;

            default:
                if (true) {
                    size_t const ath = shifter(dp) >> 1;
                    size_t const mid = (i1 + i2) / 2;

                    if (ath <= NTH) {
                        ForkMe forker;
                        if (forker.InKid()) {
                            MERGE_RANGE(i1, mid, dp + 1);
                        } else {
                            MERGE_RANGE(mid + 1, i2, dp + 1);
                        }
                    } else {
                        MERGE_RANGE(i1, mid, dp + 1);
                        MERGE_RANGE(mid + 1, i2, dp + 1);
                    }

                    MERGE(i1, mid, i2);

                    REMOVE_FILE(i1, mid);
                    REMOVE_FILE(mid + 1, i2);
                }
                return;
        }
    }

    inline void MERGE_RANGE() {
        MERGE_RANGE(0, INPUTS.size() - 1);
        std::string filename = GET_FILE_NAME(0, INPUTS.size() - 1);
        MV(filename, OUTPUT);
        RMDIR(DIRNAME);
    }
    //////////////////////////////////
    // Main merging function END. } //
    //////////////////////////////////

    ///////////////////////////////////////
    // Constructor & Destructor BEGIN: { //
    ///////////////////////////////////////
  private:
    _MACRO_CLASS_NAME_(TYPE_STRINGS const &inputs, std::string const output,
                       size_t nth, bool const cleanup)
      : INPUTS(inputs), OUTPUT(output), DIRNAME(OUTPUT + ".dir"),
        CLEANUP(cleanup), NTH(nth), THREADS(new std::thread[NTH]), SLAVE(NULL) {
    }

    ~_MACRO_CLASS_NAME_() {
        MERGE_RANGE();
        delete[] THREADS;
    }
    /////////////////////////////////////
    // Constructor & Destructor END. } //
    /////////////////////////////////////

    ///////////////////////////////////
    // Main working wrapper BEGIN: { //
    ///////////////////////////////////
  public:
    static inline void WORK(TYPE_STRINGS const &inputs,
                            std::string const output, size_t nth,
                            bool const cleanup, TYPE_SLAVE *slave) {

        TYPE_SELF tmp(
          /*TYPE_STRINGS const &inputs =*/inputs,
          /*std::string const output =*/output,
          /*size_t nth =*/nth,
          /*bool const cleanup =*/cleanup);

        tmp.SLAVE = slave;
    }

    static inline void WORK(TYPE_STRINGS const &inputs,
                            std::string const output, size_t nth,
                            bool const cleanup) {

        TYPE_SELF slave(
          /*TYPE_STRINGS const &inputs =*/inputs,
          /*std::string const output =*/output,
          /*size_t nth =*/nth,
          /*bool const cleanup =*/cleanup);
    }
    /////////////////////////////////
    // Main working wrapper END. } //
    /////////////////////////////////
};

template <typename T>
inline void multi_way_merge(std::vector<std::string> const &inputs,
                            std::string const output, size_t nth = 64,
                            bool const cleanup           = false,
                            bool const deduplicate       = true,
                            bool const sort_before_merge = false) {

    if (sort_before_merge) {
        if (deduplicate) {
            using TYPE_SLAVE = sort_and_merge_slave<T, true, true>;
            _MACRO_CLASS_NAME_<TYPE_SLAVE>::WORK(inputs, output, nth, cleanup);
        } else {
            using TYPE_SLAVE = sort_and_merge_slave<T, true, false>;
            _MACRO_CLASS_NAME_<TYPE_SLAVE>::WORK(inputs, output, nth, cleanup);
        }
    } else {
        if (deduplicate) {
            using TYPE_SLAVE = sort_and_merge_slave<T, false, true>;
            _MACRO_CLASS_NAME_<TYPE_SLAVE>::WORK(inputs, output, nth, cleanup);
        } else {
            using TYPE_SLAVE = sort_and_merge_slave<T, false, false>;
            _MACRO_CLASS_NAME_<TYPE_SLAVE>::WORK(inputs, output, nth, cleanup);
        }
    }
}

template <typename T>
inline void external_merge_sort(std::string const name_file_bin_input,
                                std::string const name_file_bin_output,
                                size_t const n_pieces, size_t const n_threads,
                                bool const deduplicate  = true,
                                bool const remove_input = false) {

    std::string const name_dir_output = name_file_bin_output + ".dir";

    mkdir(name_dir_output.c_str(), 0755);

    std::vector<std::string> names_file_bin_tmp(n_pieces);

    for (size_t i = 0; i < n_pieces; i++) {
        names_file_bin_tmp[i] =
          name_dir_output + "/out." + std::to_string(i) + ".bin";
    }

    SplitFile<T>(
      /*const std::string infilename =*/name_file_bin_input,
      /*const std::vector<std::string> &outnames =*/names_file_bin_tmp,
      /*const size_t NTH =*/n_threads);

    multi_way_merge<T>(
      /*std::vector<std::string> const &inputs =*/names_file_bin_tmp,
      /*std::string const output =*/name_file_bin_output,
      /*size_t nth =*/n_threads,
      /*bool const cleanup =*/true,
      /*bool const deduplicate =*/deduplicate,
      /*bool const sort_before_merge =*/true);

    rmdir(name_dir_output.c_str());

    if (remove_input) { unlink(name_file_bin_input.c_str()); }
}

#undef _MACRO_CLASS_NAME_
////////////////////////////////////
// Class to sort and merge END. } //
////////////////////////////////////

#endif
