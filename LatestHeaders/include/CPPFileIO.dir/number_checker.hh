#ifndef _HEADER_GUARD_number_checker_hh_
#define _HEADER_GUARD_number_checker_hh_

#include "./Basic.hh"

///////////////////////////////////
// For arbitrary numbers BEGIN:{ //
///////////////////////////////////

template <typename T>
inline char is_number_type(char const *in, T const length) {

#define _CASE_NUM_                                                             \
    case '0':                                                                  \
    case '1':                                                                  \
    case '2':                                                                  \
    case '3':                                                                  \
    case '4':                                                                  \
    case '5':                                                                  \
    case '6':                                                                  \
    case '7':                                                                  \
    case '8':                                                                  \
    case '9'

#define _CASE_E_                                                               \
    case 'e':                                                                  \
    case 'E'

#define _CASE_SIGN_                                                            \
    case '+':                                                                  \
    case '-'

#define _CASE_SPACE_ case ' '

#define _CASE_DOT_ case '.'

#define _CASE_DEFAULT_ default

    T current = 0;

    char ret = 1;

    if (current < length) {
        switch (in[current]) {

        _CASE_SPACE_:
            goto STATE_1;

        _CASE_SIGN_:
            goto STATE_2;

        _CASE_NUM_:
            goto STATE_4;

        _CASE_DOT_:
            goto STATE_5;

        _CASE_DEFAULT_:
            return 2;
        }
    } else {
        return 1;
    }

    if (true) /* Remove spurious spaces: */ {
    STATE_1:
        current++;
        if (current < length) {
            switch (in[current]) {
            _CASE_SPACE_:
                goto STATE_1;

            _CASE_SIGN_:
                goto STATE_2;

            _CASE_NUM_:
                goto STATE_4;

            _CASE_DOT_:
                goto STATE_5;

            _CASE_DEFAULT_:
                return 2;
            }
        } else {
            return 2;
        }
    }

    if (true) /* For sign: */ {
    STATE_2:

        switch (in[current]) {
            case '-': ret *= -1;
            case '+': break;
        }

        current++;

        if (current < length) {

            switch (in[current]) {

            _CASE_NUM_:
                goto STATE_4;

            _CASE_DOT_:
                goto STATE_5;

            _CASE_DEFAULT_:
                return 2;
            }

        } else {
            return 2;
        }
    }

    if (true) /* Integer part: */ {
    STATE_4:
        current++;
        if (current < length) {
            switch (in[current]) {

            _CASE_NUM_:
                goto STATE_4;

            _CASE_DOT_:
                goto STATE_5;

            _CASE_E_:
                goto STATE_7;

            _CASE_SPACE_:
                goto STATE_10;

            _CASE_DEFAULT_:
                return 2;
            }
        } else {
            return ret;
        }
    }

    if (true) /* got the point: */ {
    STATE_5:
        current++;
        if (current < length) {
            switch (in[current]) {
            _CASE_NUM_:
                goto STATE_6;

            _CASE_E_:
                goto STATE_7;

            _CASE_SPACE_:
                goto STATE_10;

            _CASE_DEFAULT_:
                return 2;
            }
        } else {
            return 0;
        }
    }

    if (true) /* Fraction part: */ {
    STATE_6:
        current++;
        if (current < length) {
            switch (in[current]) {
            _CASE_NUM_:
                goto STATE_6;

            _CASE_E_:
                goto STATE_7;

            _CASE_SPACE_:
                goto STATE_10;

            _CASE_DEFAULT_:
                return 2;
            }
        } else {
            return 0;
        }
    }

    if (true) /* Exponent part: */ {
    STATE_7:
        current++;
        if (current < length) {
            switch (in[current]) {
            _CASE_SIGN_:
                goto STATE_8;

            _CASE_NUM_:
                goto STATE_9;

            _CASE_DEFAULT_:
                return 2;
            }
        } else {
            return 0;
        }
    }

    if (true) /* exponent sign */ {
    STATE_8:
        current++;
        if (current < length) {
            switch (in[current]) {

            _CASE_NUM_:
                goto STATE_9;

            _CASE_DEFAULT_:
                return 2;
            }
        } else {
            return 2;
        }
    }

    if (true) /* exponent number */ {
    STATE_9:
        current++;
        if (current < length) {
            switch (in[current]) {
            _CASE_NUM_:
                goto STATE_9;

            _CASE_SPACE_:
                goto STATE_10;

            _CASE_DEFAULT_:
                return 2;
            }
        } else {
            return 0;
        }
    }

    if (true) /* space at the end */ {
    STATE_10:
        current++;
        if (current < length) {
            switch (in[current]) {
            _CASE_SPACE_:
                goto STATE_10;

            _CASE_DEFAULT_:
                return 2;
            }
        } else {
            return 0;
        }
    }

#undef _CASE_DEFAULT_
#undef _CASE_DOT_
#undef _CASE_SPACE_
#undef _CASE_SIGN_
#undef _CASE_E_
#undef _CASE_NUM_
}

inline char is_number_type(std::string const &in) {
    return is_number_type(in.c_str(), in.size());
}

inline char is_number_type(char const *in) {
    return is_number_type(in, strlen(in));
}

inline char number_type_and_size(char const *in) {
    if (strlen(in) == 0) { return 1; }

    char res = is_number_type(in);

    switch (res) {
        case 0: return 0;

        case -1:
            if (true) {

                TYPE_I8 constexpr max8   = 127;
                TYPE_I16 constexpr max16 = 32767;
                TYPE_I32 constexpr max32 = 2147483647;
                TYPE_I64 constexpr max64 = 9223372036854775807;

                long tmp = 0;
                sscanf(in, "%ld", &tmp);

                if ((-max8 <= tmp) && (tmp <= max8)) {
                    return -1;
                } else if ((-max16 <= tmp) && (tmp <= max16)) {
                    return -2;
                } else if ((-max32 <= tmp) && (tmp <= max32)) {
                    return -4;
                } else if ((-max64 <= tmp) && (tmp <= max64)) {
                    return -8;
                }
            }
            return 0;

        case 1:
            if (true) {
                TYPE_U8 constexpr max8   = 255U;
                TYPE_U16 constexpr max16 = 65535U;
                TYPE_U32 constexpr max32 = 4294967295U;
                TYPE_U64 constexpr max64 = 18446744073709551615U;

                size_t tmp = 0;
                sscanf(in, "%zu", &tmp);

                if (tmp <= max8) {
                    return 1;
                } else if (tmp <= max16) {
                    return 2;
                } else if (tmp <= max32) {
                    return 4;
                } else if (tmp <= max64) {
                    return 8;
                }
            }
            return 0;

        default: return 64;
    }

    return res;
}

inline char merge_codes(char const a, char const b) {
    if ((0 < a) && (a <= 8)) {
        if ((0 < b) && (b <= 8)) {
            return mymax(a, b);
        } else if ((-8 <= b) && (b < 0)) {
            return -mymax(char(-b), a);
        } else if (b == 0) {
            return 0;
        } else if (b == 64) {
            return 64;
        }
    } else if ((-8 <= a) && (a < 0)) {
        if ((0 < b) && (b <= 8)) {
            return -mymax(b, char(-a));
        } else if ((-8 <= b) && (b < 0)) {
            return mymin(a, b);
        } else if (b == 0) {
            return 0;
        } else if (b == 64) {
            return 64;
        }
    } else if (a == 0) {
        if (b == 64) {
            return 64;
        } else {
            return 0;
        }
    } else if (a == 64) {
        return 64;
    }
    return 64;
}
/////////////////////////////////
// For arbitrary numbers END.} //
/////////////////////////////////

/////////////////////////
// For doubles BEGIN:{ //
/////////////////////////

// ex: '  +  2543.234e+015  '

template <typename T> inline bool is_double(char const *in, T const length) {

#define _CASE_NUM_                                                             \
    case '0':                                                                  \
    case '1':                                                                  \
    case '2':                                                                  \
    case '3':                                                                  \
    case '4':                                                                  \
    case '5':                                                                  \
    case '6':                                                                  \
    case '7':                                                                  \
    case '8':                                                                  \
    case '9'

#define _CASE_E_                                                               \
    case 'e':                                                                  \
    case 'E'

#define _CASE_SIGN_                                                            \
    case '+':                                                                  \
    case '-'

#define _CASE_SPACE_ case ' '

#define _CASE_DOT_ case '.'

#define _CASE_DEFAULT_ default

    T current = 0;

    if (current < length) {
        switch (in[current]) {

        _CASE_SPACE_:
            goto STATE_1;

        _CASE_SIGN_:
            goto STATE_2;

        _CASE_NUM_:
            goto STATE_4;

        _CASE_DOT_:
            goto STATE_5;

        _CASE_DEFAULT_:
            return false;
        }
    } else {
        return false;
    }

    if (true) /* Remove spurious spaces: */ {
    STATE_1:
        current++;
        if (current < length) {
            switch (in[current]) {
            _CASE_SPACE_:
                goto STATE_1;

            _CASE_SIGN_:
                goto STATE_2;

            _CASE_NUM_:
                goto STATE_4;

            _CASE_DOT_:
                goto STATE_5;

            _CASE_DEFAULT_:
                return false;
            }
        } else {
            return false;
        }
    }

    if (true) /* For sign: */ {
    STATE_2:
        current++;
        if (current < length) {
            switch (in[current]) {
            _CASE_NUM_:
                goto STATE_4;

            _CASE_DOT_:
                goto STATE_5;

            _CASE_DEFAULT_:
                return false;
            }
        } else {
            return false;
        }
    }

    if (true) /* Integer part: */ {
    STATE_4:
        current++;
        if (current < length) {
            switch (in[current]) {

            _CASE_NUM_:
                goto STATE_4;

            _CASE_DOT_:
                goto STATE_5;

            _CASE_E_:
                goto STATE_7;

            _CASE_SPACE_:
                goto STATE_10;

            _CASE_DEFAULT_:
                return false;
            }
        } else {
            return true;
        }
    }

    if (true) /* got the point: */ {
    STATE_5:
        current++;
        if (current < length) {
            switch (in[current]) {
            _CASE_NUM_:
                goto STATE_6;

            _CASE_E_:
                goto STATE_7;

            _CASE_SPACE_:
                goto STATE_10;

            _CASE_DEFAULT_:
                return false;
            }
        } else {
            return true;
        }
    }

    if (true) /* Fraction part: */ {
    STATE_6:
        current++;
        if (current < length) {
            switch (in[current]) {
            _CASE_NUM_:
                goto STATE_6;

            _CASE_E_:
                goto STATE_7;

            _CASE_SPACE_:
                goto STATE_10;

            _CASE_DEFAULT_:
                return false;
            }
        } else {
            return true;
        }
    }

    if (true) /* Exponent part: */ {
    STATE_7:
        current++;
        if (current < length) {
            switch (in[current]) {
            _CASE_SIGN_:
                goto STATE_8;

            _CASE_NUM_:
                goto STATE_9;

            _CASE_DEFAULT_:
                return false;
            }
        } else {
            return false;
        }
    }

    if (true) /* exponent sign */ {
    STATE_8:
        current++;
        if (current < length) {
            switch (in[current]) {

            _CASE_NUM_:
                goto STATE_9;

            _CASE_DEFAULT_:
                return false;
            }
        } else {
            return false;
        }
    }

    if (true) /* exponent number */ {
    STATE_9:
        current++;
        if (current < length) {
            switch (in[current]) {
            _CASE_NUM_:
                goto STATE_9;

            _CASE_SPACE_:
                goto STATE_10;

            _CASE_DEFAULT_:
                return false;
            }
        } else {
            return true;
        }
    }

    if (true) /* space at the end */ {
    STATE_10:
        current++;
        if (current < length) {
            switch (in[current]) {
            _CASE_SPACE_:
                goto STATE_10;

            _CASE_DEFAULT_:
                return false;
            }
        } else {
            return true;
        }
    }

#undef _CASE_DEFAULT_
#undef _CASE_DOT_
#undef _CASE_SPACE_
#undef _CASE_SIGN_
#undef _CASE_E_
#undef _CASE_NUM_
}

inline bool is_double(std::string const &in) {
    return is_double(in.c_str(), in.size());
}

inline bool is_double(char const *in) { return is_double(in, strlen(in)); }

///////////////////////
// For doubles END.} //
///////////////////////

#endif
