#ifndef _HEADER_GUARD_CPPFileIO_dir_hashing_hh_
#define _HEADER_GUARD_CPPFileIO_dir_hashing_hh_

//////////////////////
// Headers BEGIN: { //
//////////////////////
#include "./Basic.hh"
#include "./redirect_fd.hh"
#include "./FileLines.hh"
#include "./find_executables.hh"
#include "./ExternalPrograms.hh"
////////////////////
// Headers END. } //
////////////////////

//////////////////////////////////////////
// main function for sha512sum BEGIN: { //
//////////////////////////////////////////

inline std::string SHA512SUM(std::string const name_file) {
    std::hash<std::string> hashes;
    auto                   hash    = hashes(name_file);
    auto                   seconds = time(NULL);
    auto                   pid     = getpid();

    std::string const outfilename =
      std::string("/tmp/SHA512SUM_") + std::to_string(seconds) + "_" +
      std::to_string(pid) + "_" + std::to_string(hash);

    if (true) {
        redirect_fd       redirector(outfilename, 1);
        find_executable   finder;
        std::string const sha512sum = finder("/sha512sum");
        if (true) { ExternalStarter<true>::GET(sha512sum)(name_file); }
    }

    tmp_file_lines<8> reader(outfilename);
    std::string       ret = reinterpret_cast<char *>(reader(0).get_start());
    ret.resize(128);
    return ret;
}

////////////////////////////////////////
// main function for sha512sum END. } //
////////////////////////////////////////

//////////////////////////////////////////
// main function for sha256sum BEGIN: { //
//////////////////////////////////////////

inline std::string SHA256SUM(std::string const name_file) {
    std::hash<std::string> hashes;
    auto                   hash    = hashes(name_file);
    auto                   seconds = time(NULL);
    auto                   pid     = getpid();

    std::string const outfilename =
      std::string("/tmp/SHA256SUM_") + std::to_string(seconds) + "_" +
      std::to_string(pid) + "_" + std::to_string(hash);

    if (true) {
        redirect_fd       redirector(outfilename, 1);
        find_executable   finder;
        std::string const sha512sum = finder("/sha256sum");
        if (true) { ExternalStarter<true>::GET(sha512sum)(name_file); }
    }

    tmp_file_lines<8> reader(outfilename);
    std::string       ret = reinterpret_cast<char *>(reader(0).get_start());
    ret.resize(64);
    return ret;
}

////////////////////////////////////////
// main function for sha256sum END. } //
////////////////////////////////////////

///////////////////////////////////////
// main function for md5sum BEGIN: { //
///////////////////////////////////////

inline std::string MD5SUM(std::string const name_file) {
    std::hash<std::string> hashes;
    auto                   hash    = hashes(name_file);
    auto                   seconds = time(NULL);
    auto                   pid     = getpid();

    std::string const outfilename =
      std::string("/tmp/MD5SUM_") + std::to_string(seconds) + "_" +
      std::to_string(pid) + "_" + std::to_string(hash);

    if (true) {
        redirect_fd       redirector(outfilename, 1);
        find_executable   finder;
        std::string const sha512sum = finder("/md5sum");
        if (true) { ExternalStarter<true>::GET(sha512sum)(name_file); }
    }

    tmp_file_lines<8> reader(outfilename);
    std::string       ret = reinterpret_cast<char *>(reader(0).get_start());
    ret.resize(32);
    return ret;
}

/////////////////////////////////////
// main function for md5sum END. } //
/////////////////////////////////////

#endif
