#ifndef _HEADER_GUARD_CPPFileIO_translate_csv_
#define _HEADER_GUARD_CPPFileIO_translate_csv_

/////////////////////
// Headers BEGIN:{ //
/////////////////////
#include "../Headers.hh"
#include "./Basic.hh"
#include "./FileFD.hh"
#include "./FileArray.hh"
#include "./FullFileReader.hh"
#include "./FileWriter.hh"
///////////////////
// Headers END.} //
///////////////////

template <typename TYPE_INT>
inline void translate_csv(std::string const infile, std::string const outfile,
                          char const seperator) {

    full_file_reader<char>     reader(infile);
    FileWriter<char, TYPE_INT> writer(outfile);

    TYPE_INT current = 0;

    if (current < reader()) {
        switch (reader(current)) {
            case ',': goto LABEL_COMMA;
            case '"': goto LABEL_INSIDE_QUOTES;
            case '\\': goto LABEL_ESCAPED;
            case '\n': goto LABEL_END_OF_LINE;
            default: goto LABEL_REGULAR;
        }
    } else {
        return;
    }

LABEL_REGULAR:
LABEL_END_OF_LINE:
    writer(reader(current));
LABEL_ANALYZE_NEXT_CHAR:
    current++;
    if (current < reader()) {
        switch (reader(current)) {
            case ',': goto LABEL_COMMA;
            case '"': goto LABEL_INSIDE_QUOTES;
            case '\\': goto LABEL_ESCAPED;
            case '\n': goto LABEL_END_OF_LINE;
            default: goto LABEL_REGULAR;
        }
    } else {
        return;
    }

LABEL_COMMA:
    writer(seperator);
    goto LABEL_ANALYZE_NEXT_CHAR;

LABEL_INSIDE_QUOTES:
    current++;
    if (current < reader()) {
        switch (reader(current)) {
            case '"': goto LABEL_ANALYZE_NEXT_CHAR;
            case '\n': goto LABEL_END_OF_LINE;
            default: writer(reader(current)); goto LABEL_INSIDE_QUOTES;
        }
    } else {
        return;
    }

LABEL_ESCAPE_INSIDE_QUOTES:
    current++;
    if (current < reader()) {
        writer(reader(current));
        goto LABEL_INSIDE_QUOTES;
    } else {
        return;
    }

LABEL_ESCAPED:
    current++;
    if (current < reader()) {
        writer(reader(current));
        goto LABEL_ANALYZE_NEXT_CHAR;
    } else {
        return;
    }
}

template <typename TYPE_INT>
inline void translate_csv_old(std::string const infile,
                              std::string const outfile, char const seperator) {

    full_file_reader<char>     reader(infile);
    FileWriter<char, TYPE_INT> writer(outfile);

    unsigned char constexpr REGULAR = 1;
    unsigned char constexpr ESCAPED = 2;
    unsigned char status            = REGULAR;

    for (size_t i = 0; i < reader(); i++) {

        char const &buf = reader(i);

        switch (status) {
            case REGULAR:
                switch (buf) {
                    case ',': writer(seperator); break;

                    case '"': status = ESCAPED; break;

                    default: writer(buf); break;
                }
                break;

            case ESCAPED:
                switch (buf) {
                    case '"': status = REGULAR; break;

                    default: writer(buf); break;
                }
                break;
        }
    }
}

template <typename TYPE_INT = TYPE_U64>
inline void csv_2_tsv(std::string const infile, std::string const outfile) {
    translate_csv<TYPE_INT>(infile, outfile, '\t');
}

template <typename TYPE_INT = TYPE_U64>
inline void csv_2_null(std::string const infile, std::string const outfile) {
    translate_csv<TYPE_INT>(infile, outfile, 0);
}

#endif
