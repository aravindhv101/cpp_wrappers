#ifndef _HEADER_GUARD_Headers_
#define _HEADER_GUARD_Headers_

#ifndef _GNU_SOURCE
	#define _GNU_SOURCE
#endif

#ifndef _XOPEN_SOURCE
	#define _XOPEN_SOURCE
#endif

#include <algorithm>
#include <bitset>
#include <cmath>
#include <complex>
#include <dirent.h>
#include <fcntl.h>
#include <functional>
#include <iomanip>
#include <iostream>
#include <locale>
#include <map>
#include <math.h>
#include <memory>
#include <mutex>
#include <queue>
#include <random>
#include <sched.h>
#include <set>
#include <sstream>
#include <stack>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <unordered_map>
#include <vector>
#include <thread>

#ifdef USECBLAS
	#include "cblas.h"
#endif

#endif
