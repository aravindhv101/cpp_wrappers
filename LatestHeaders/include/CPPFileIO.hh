#ifndef _HEADER_GUARD_CPPFileIO_
#define _HEADER_GUARD_CPPFileIO_

/////////////////////
// Includes BEGIN: //
/////////////////////
#include "./Headers.hh"
#include "./pcg-cpp.hh"
#include "./xxHash.hh"
#include "./TEMPLATES.hh"
///////////////////
// Includes END. //
///////////////////

namespace CPPFileIO {

//////////////////////////
// Main Includes BEGIN: //
//////////////////////////
#include "./CPPFileIO.dir/Basic.hh"
#include "./CPPFileIO.dir/find_stand_alone.hh"
#include "./CPPFileIO.dir/find_executables.hh"
#include "./CPPFileIO.dir/number_checker.hh"
#include "./CPPFileIO.dir/redirect_fd.hh"
#include "./CPPFileIO.dir/StaticHorsePoolSearch.hh"
#include "./CPPFileIO.dir/ToUpper.hh"
#include "./CPPFileIO.dir/Z_Curve_Digits.hh"
#include "./CPPFileIO.dir/BinMapper.hh"
#include "./CPPFileIO.dir/Atomic_Counter.hh"
#include "./CPPFileIO.dir/ExternalPrograms.hh"
#include "./CPPFileIO.dir/find.hh"
#include "./CPPFileIO.dir/hashing.hh"
#include "./CPPFileIO.dir/D1.hh"
#include "./CPPFileIO.dir/MyStr.hh"
#include "./CPPFileIO.dir/Polynomial.hh"
#include "./CPPFileIO.dir/interpolation_polynomial.hh"
#include "./CPPFileIO.dir/Correlation.hh"
#include "./CPPFileIO.dir/D2.hh"
#include "./CPPFileIO.dir/symmetric_matrix.hh"
#include "./CPPFileIO.dir/ParseTime.hh"
#include "./CPPFileIO.dir/DirChanger.hh"
#include "./CPPFileIO.dir/RandomNumberGenerator.hh"
#include "./CPPFileIO.dir/DirChanger.hh"
#include "./CPPFileIO.dir/FileLines.hh"
#include "./CPPFileIO.dir/FileFD.hh"
#include "./CPPFileIO.dir/FileArray.hh"
#include "./CPPFileIO.dir/FullFileReader.hh"
#include "./CPPFileIO.dir/hasher.hh"
#include "./CPPFileIO.dir/make_boundary.hh"
#include "./CPPFileIO.dir/small_file_appender.hh"
#include "./CPPFileIO.dir/FileWriter.hh"
#include "./CPPFileIO.dir/DynamicStore.hh"
#include "./CPPFileIO.dir/Shuffle.hh"
#include "./CPPFileIO.dir/FileSplitter.hh"
#include "./CPPFileIO.dir/AnalyzeLines.hh"
#include "./CPPFileIO.dir/Sorter.hh"
#include "./CPPFileIO.dir/FileLines.hh"
#include "./CPPFileIO.dir/LatexWriter.hh"
#include "./CPPFileIO.dir/FileDivider.hh"
#include "./CPPFileIO.dir/BufferLineReader.hh"
#include "./CPPFileIO.dir/FastTXT2BIN_NEW.hh"
#include "./CPPFileIO.dir/BinarySearch.hh"
#include "./CPPFileIO.dir/Inner_Join.hh"
#include "./CPPFileIO.dir/RadixSort.hh"
#include "./CPPFileIO.dir/translate_csv.hh"
#include "./CPPFileIO.dir/search_maker.hh"
#include "./CPPFileIO.dir/proc_reader.hh"
#include "./CPPFileIO.dir/parallel_run_file_lines.hh"
////////////////////////
// Main Includes END. //
////////////////////////

} // namespace CPPFileIO

#endif
