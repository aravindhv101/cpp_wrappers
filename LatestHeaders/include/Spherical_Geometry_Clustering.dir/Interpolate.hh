#ifndef _HEADER_GUARD_Spherical_Geometry_Clustering_dir_Interpolate_hh_
#define _HEADER_GUARD_Spherical_Geometry_Clustering_dir_Interpolate_hh_

#include "./PolarCoordinates.hh"

#define _MACRO_CLASS_NAME_ Interpolate
template <typename TF, typename TI> class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_FLOAT    = TF;
    using TYPE_INT      = TI;
    using TYPE_SELF     = _MACRO_CLASS_NAME_<TYPE_FLOAT, TYPE_INT>;
    using TYPE_ELEMENT  = GPS_with_time<TYPE_FLOAT, TYPE_INT>;
    using TYPE_ELEMENTS = std::vector<TYPE_ELEMENT>;
    using TYPE_BUFFER   = CPPFileIO::Dynamic1DArray<TYPE_ELEMENT, TYPE_INT>;
    using TYPE_FINDER   = CPPFileIO::BinarySearch<TYPE_BUFFER>;
    using TYPE_FIND_RES = typename TYPE_FINDER::TYPE_RETURN;

  private:
    TYPE_ELEMENTS ELEMENTS;
    bool          PREPARED;
    TYPE_BUFFER * BUFFER;

  private:
    inline void UN_PREPARE() {
        if (PREPARED) {
            delete BUFFER;
            PREPARED = false;
        }
    }

    inline bool PREPARE() {
        if (PREPARED) { return true; }

        if (ELEMENTS.size() < 2) { return false; }

        TYPE_ELEMENTS tmp(ELEMENTS.size());
        tmp = ELEMENTS;

        std::sort(tmp.begin(), tmp.end());

        ELEMENTS.clear();

        ELEMENTS.push_back(tmp[0]);
        for (size_t i = 1; i < tmp.size(); i++) {
            if (tmp[i] != tmp[i - 1]) { ELEMENTS.push_back(tmp[i]); }
        }

        BUFFER = new TYPE_BUFFER(&(ELEMENTS[0]), ELEMENTS.size());

        PREPARED = true;

        return true;
    }

    inline TYPE_FIND_RES FIND_INDEX(TYPE_FLOAT const t) {
        if (!PREPARE()) {
            TYPE_FIND_RES ret = {ELEMENTS.size() + 1, ELEMENTS.size() + 1, -1};
            return ret;
        }

        TYPE_FINDER FINDER(*BUFFER);

        struct comparer {

            inline char operator()(TYPE_ELEMENT const &a,
                                   TYPE_ELEMENT const &b) const {
                return (a > b) - (a < b);
            }

            inline char operator()(TYPE_ELEMENT const &a,
                                   TYPE_FLOAT const &  b) const {
                return (a.time > b) - (a.time < b);
            }

            inline char operator()(TYPE_FLOAT const &  a,
                                   TYPE_ELEMENT const &b) const {
                return (a > b.time) - (a < b.time);
            }

            inline char operator()(TYPE_FLOAT const &a,
                                   TYPE_FLOAT const &b) const {
                return (a > b) - (a < b);
            }

        } cmp;

        TYPE_FIND_RES const res = FINDER(t, cmp);

        return res;
    }

    inline TYPE_FLOAT INTERPOLATE(TYPE_FLOAT const x, TYPE_INT const len,
                                  TYPE_FLOAT const *Y, TYPE_FLOAT const *X) {

        std::vector<TYPE_FLOAT> U(len);
        std::vector<TYPE_FLOAT> V(len);

        for (TYPE_INT i = 0; i < len; i++) {

            U[i] = 1;
            V[i] = 1;

            for (TYPE_INT j = 0; j < len; j++)
                if (j != i) {

                    U[i] *= (X[i] - X[j]);
                    V[i] *= (x - X[j]);
                }
        }

        TYPE_FLOAT y = 0;

        for (TYPE_INT i = 0; i < len; ++i) { y += (Y[i] * V[i] / U[i]); }

        return y;
    }

    inline TYPE_FLOAT INTERPOLATE_WITH_GRAD(TYPE_FLOAT &y, TYPE_FLOAT &dy,
                                            TYPE_FLOAT const  x,
                                            TYPE_INT const    len,
                                            TYPE_FLOAT const *Y,
                                            TYPE_FLOAT const *X) {

        std::vector<TYPE_FLOAT> U(len);
        std::vector<TYPE_FLOAT> V(len);
        std::vector<TYPE_FLOAT> DV(len);

        for (TYPE_INT i = 0; i < len; i++) {

            U[i]  = 1;
            V[i]  = 1;
            DV[i] = 0;

            for (TYPE_INT j = 0; j < len; j++)
                if (j != i) {

                    U[i] *= (X[i] - X[j]);
                    V[i] *= (x - X[j]);

                    TYPE_FLOAT dv = 1;

                    for (TYPE_INT k = 0; k < len; k++)
                        if ((k != i) && (k != j)) { dv *= (x - X[k]); }

                    DV[i] += dv;
                }
        }

        y  = 0;
        dy = 0;

        for (TYPE_INT i = 0; i < len; ++i) {
            y += (Y[i] * V[i] / U[i]);
            dy += (Y[i] * DV[i] / U[i]);
        }

        return y;
    }

    inline TYPE_ELEMENT GET_VAL(TYPE_FLOAT const time, TYPE_INT const len = 2) {
        auto const res = FIND_INDEX(time);

        TYPE_ELEMENT ret;
        ret.time = time;

        if (true) {
            if (res.status == -1) {
                ret.invalidate();
                return ret;
            }
        } else {
            switch (res.status) {
                case -1: ret.invalidate(); return ret;

                case 0:
                    ret.latitude  = ELEMENTS[res.begin].latitude;
                    ret.longitude = ELEMENTS[res.begin].longitude;
                    ret.time      = time;
                    return ret;

                case 1:
                default: break;
            }
        }

        TYPE_INT begin = 0;
        if (len <= static_cast<TYPE_INT>(res.begin)) {
            begin = res.begin - len;
        }

        TYPE_INT end = res.end + len;
        if (static_cast<TYPE_INT>(ELEMENTS.size()) <= end) {
            end = ELEMENTS.size() - 1;
        }

        std::vector<TYPE_FLOAT> lat, lon, t;

        for (TYPE_INT i = begin; i <= end; i++) {
            lat.push_back(ELEMENTS[i].latitude);
            lon.push_back(ELEMENTS[i].longitude);
            t.push_back(ELEMENTS[i].time);
        }

        ret.latitude = INTERPOLATE(
          /* TYPE_FLOAT const x = */ time,
          /* TYPE_INT const len = */ lat.size(),
          /* TYPE_FLOAT const *Y = */ &(lat[0]),
          /* TYPE_FLOAT const *X = */ &(t[0]));

        ret.longitude = INTERPOLATE(
          /* TYPE_FLOAT const x = */ time,
          /* TYPE_INT const len = */ lon.size(),
          /* TYPE_FLOAT const *Y = */ &(lon[0]),
          /* TYPE_FLOAT const *X = */ &(t[0]));

        return ret;
    }

    inline TYPE_ELEMENT GET_VAL_GRAD(TYPE_FLOAT const time, TYPE_FLOAT &v,
                                     TYPE_INT const len = 2) {

        TYPE_ELEMENT ret;
        ret.time = time;
        v        = -9999;

        auto const res = FIND_INDEX(time);

        if (false) {
            switch (res.status) {
                case -1: ret.invalidate(); return ret;

                case 0:
                    ret.latitude  = ELEMENTS[res.begin].latitude;
                    ret.longitude = ELEMENTS[res.begin].longitude;
                    ret.time      = time;
                    return ret;

                case 1:
                default: break;
            }
        } else {
            if (res.status == -1) {
                ret.invalidate();
                return ret;
            }
        }

        TYPE_INT                begin = 0;
        TYPE_INT                end   = res.end + len;
        std::vector<TYPE_FLOAT> lat, lon, t;
        TYPE_FLOAT              d_lat, d_lon;

        if (true) /* Decide the boundaries */ {
            if (len <= static_cast<TYPE_INT>(res.begin)) {
                begin = res.begin - len;
            }
            if (static_cast<TYPE_INT>(ELEMENTS.size()) <= end) {
                end = ELEMENTS.size() - 1;
            }
        }

        if (true) /* Allocate the points: */ {
            for (TYPE_INT i = begin; i <= end; i++) {
                lat.push_back(ELEMENTS[i].latitude);
                lon.push_back(ELEMENTS[i].longitude);
                t.push_back(ELEMENTS[i].time);
            }
        }

        if (true) /* Perform the interpolation: */ {
            INTERPOLATE_WITH_GRAD(
              /* TYPE_FLOAT &y = */ ret.latitude,
              /* TYPE_FLOAT &dy = */ d_lat,
              /* TYPE_FLOAT const x = */ time,
              /* TYPE_INT const len = */ lat.size(),
              /* TYPE_FLOAT const *Y = */ &(lat[0]),
              /* TYPE_FLOAT const *X = */ &(t[0]));

            INTERPOLATE_WITH_GRAD(
              /* TYPE_FLOAT &y = */ ret.longitude,
              /* TYPE_FLOAT &dy = */ d_lon,
              /* TYPE_FLOAT const x = */ time,
              /* TYPE_INT const len = */ lon.size(),
              /* TYPE_FLOAT const *Y = */ &(lon[0]),
              /* TYPE_FLOAT const *X = */ &(t[0]));
        }

        if (true) {
            TYPE_FLOAT const phi   = (M_PI * ret.longitude) / 180.0;
            TYPE_FLOAT const theta = M_PI * (0.5 - (ret.latitude / 180.0));

            TYPE_FLOAT const d_phi   = (M_PI * d_lon) / 180.0;
            TYPE_FLOAT const d_theta = -(M_PI * d_lat) / 180.0;

            TYPE_FLOAT const c_t = cos(theta);
            TYPE_FLOAT const s_t = sin(theta);

            TYPE_FLOAT const c_p = cos(phi);
            TYPE_FLOAT const s_p = sin(phi);

            TYPE_FLOAT const Vx = TYPE_ELEMENT::EarthRadius() *
                                  ((c_t * c_p * d_theta) - (s_t * s_p * d_phi));

            TYPE_FLOAT const Vy = TYPE_ELEMENT::EarthRadius() *
                                  ((c_t * s_p * d_theta) + (s_t * c_p * d_phi));

            TYPE_FLOAT const Vz = -s_t * d_theta * TYPE_ELEMENT::EarthRadius();

            v = std::sqrt(std::pow(Vx, 2) + std::pow(Vy, 2) + std::pow(Vz, 2));
        }

        return ret;
    }

  public:
    inline void append(TYPE_ELEMENT const &in) {
        ELEMENTS.push_back(in);
        UN_PREPARE();
    }

    inline void append(TYPE_ELEMENT const &&in) {
        ELEMENTS.push_back(in);
        UN_PREPARE();
    }

    inline void append(TYPE_FLOAT const lat, TYPE_FLOAT const lon,
                       TYPE_FLOAT const t) {

        append(TYPE_ELEMENT(lat, lon, t));
    }

    inline bool prepare() { return PREPARE(); }

    inline TYPE_ELEMENT interpolate(TYPE_FLOAT const in) {
        return GET_VAL(in, 2);
    }

    inline TYPE_ELEMENT interpolate(TYPE_FLOAT const in, TYPE_INT const order) {
        return GET_VAL(in, order);
    }

    inline TYPE_ELEMENT interpolate(TYPE_FLOAT const in, TYPE_FLOAT &v,
                                    TYPE_INT const order) {

        return GET_VAL_GRAD(in, v, order);
    }

  public:
    inline void operator()(TYPE_ELEMENT const &in) { append(in); }

    inline void operator()(TYPE_ELEMENT const &&in) { append(in); }

    inline void operator()(TYPE_FLOAT const lat, TYPE_FLOAT const lon,
                           TYPE_FLOAT const t) {

        append(lat, lon, t);
    }

    inline bool operator()() { return PREPARE(); }

    inline TYPE_ELEMENT operator()(TYPE_FLOAT const in) {
        return interpolate(in, 2);
    }

    inline TYPE_ELEMENT operator()(TYPE_FLOAT const in, TYPE_INT const order) {
        return interpolate(in, order);
    }

    inline TYPE_ELEMENT operator()(TYPE_FLOAT const in, TYPE_FLOAT &v,
                                   TYPE_INT const order) {

        return interpolate(in, v, order);
    }

  public:
    _MACRO_CLASS_NAME_() : PREPARED(false) {}
    ~_MACRO_CLASS_NAME_() {}
};
#undef _MACRO_CLASS_NAME_

#endif
