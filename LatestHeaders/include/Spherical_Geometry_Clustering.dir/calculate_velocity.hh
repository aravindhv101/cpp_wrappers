#ifndef _HEADER_GUARD_Spherical_Geometry_Clustering_dir_calculate_velocity_hh_
#define _HEADER_GUARD_Spherical_Geometry_Clustering_dir_calculate_velocity_hh_

//////////////////////
// Headers BEGIN: { //
//////////////////////
#include "../CPPFileIO.hh"
#include "./PolarCoordinates.hh"
////////////////////
// Headers END. } //
////////////////////

//////////////////////////////////////////
// Class to calculate velocity BEGIN: { //
//////////////////////////////////////////
#define _MACRO_CLASS_NAME_ calculate_velocity
template <typename T> class _MACRO_CLASS_NAME_ {
    ////////////////////////////////////
    // Important definitions BEGIN: { //
    ////////////////////////////////////
  public:
    using TYPE_SELF       = _MACRO_CLASS_NAME_;
    using TYPE_FLOAT      = T;
    using TYPE_COORDINATE = D2GPS_Coordinates<TYPE_FLOAT, long>;
    //////////////////////////////////
    // Important definitions END. } //
    //////////////////////////////////

    //////////////////////////////////////
    // Important data elements BEGIN: { //
    //////////////////////////////////////
  public:
    static TYPE_FLOAT constexpr R = TYPE_COORDINATE::EarthRadius();

  public:
    TYPE_FLOAT const LAT, LON;
    TYPE_FLOAT const D_LAT, D_LON;
    TYPE_FLOAT const THETA, PHI;
    TYPE_FLOAT const D_THETA, D_PHI;
    TYPE_FLOAT const SIN_THETA, SIN_PHI;
    TYPE_FLOAT const COS_THETA, COS_PHI;
    TYPE_FLOAT const D_SIN_THETA, D_SIN_PHI;
    TYPE_FLOAT const D_COS_THETA, D_COS_PHI;
    TYPE_FLOAT const TCC, TCS, TSC, TSS;
    TYPE_FLOAT const D_TCC, D_TCS, D_TSC, D_TSS;
    TYPE_FLOAT const X, Y, Z;
    TYPE_FLOAT const D_X, D_Y, D_Z;
    TYPE_FLOAT const V;
    ////////////////////////////////////
    // Important data elements END. } //
    ////////////////////////////////////

    ///////////////////////////////////////
    // Constructor & Destructor BEGIN: { //
    ///////////////////////////////////////
  public:
    _MACRO_CLASS_NAME_(TYPE_FLOAT const lat, TYPE_FLOAT const lon,
                       TYPE_FLOAT const d_lat, TYPE_FLOAT const d_lon)
      : // Basic variables:
        LAT(lat), LON(lon), D_LAT(d_lat), D_LON(d_lon),

        ///////////////////////////////////////
        // Calculating polar angles BEGIN: { //
        ///////////////////////////////////////
        THETA((M_PI * (90 - LAT)) / 180.0), D_THETA((-M_PI * D_LAT) / 180.0),

        PHI((M_PI * LON) / 180.0), D_PHI((M_PI * D_LON) / 180.0),
        /////////////////////////////////////
        // Calculating polar angles END. } //
        /////////////////////////////////////

        ///////////////////////////////////
        // Trignometric rations BEGIN: { //
        ///////////////////////////////////
        SIN_THETA(sin(THETA)), D_SIN_THETA(COS_THETA * D_THETA),

        COS_THETA(cos(THETA)), D_COS_THETA(-SIN_THETA * D_THETA),

        SIN_PHI(sin(PHI)), D_SIN_PHI(COS_PHI * D_PHI),

        COS_PHI(cos(PHI)), D_COS_PHI(-SIN_PHI * D_PHI),
        /////////////////////////////////
        // Trignometric rations END. } //
        /////////////////////////////////

        ////////////////////////////////////
        // Intermediate products BEGIN: { //
        ////////////////////////////////////
        TCC(COS_THETA * COS_PHI),
        D_TCC((D_COS_THETA * COS_PHI) + (COS_THETA * D_COS_PHI)),

        TCS(COS_THETA * SIN_PHI),
        D_TCS((D_COS_THETA * SIN_PHI) + (COS_THETA * D_SIN_PHI)),

        TSC(SIN_THETA * COS_PHI),
        D_TSC((D_SIN_THETA * COS_PHI) + (SIN_THETA * D_COS_PHI)),

        TSS(SIN_THETA * SIN_PHI),
        D_TSS((D_SIN_THETA * SIN_PHI) + (SIN_THETA * D_SIN_PHI)),
        //////////////////////////////////
        // Intermediate products END. } //
        //////////////////////////////////

        ////////////////////////////////////////////
        // Final embeded tangent vectors BEGIN: { //
        ////////////////////////////////////////////
        X(R * TSC), D_X(R * D_TSC),

        Y(R * TSS), D_Y(R * D_TSS),

        Z(R * COS_THETA), D_Z(R * D_COS_THETA),
        //////////////////////////////////////////
        // Final embeded tangent vectors END. } //
        //////////////////////////////////////////

        // Final velocity:
        V(std::sqrt(std::pow(D_X, 2) + std::pow(D_Y, 2) + std::pow(D_Z, 2))) {}
    /////////////////////////////////////
    // Constructor & Destructor END. } //
    /////////////////////////////////////
};
#undef _MACRO_CLASS_NAME_
////////////////////////////////////////
// Class to calculate velocity END. } //
////////////////////////////////////////

///////////////////////////////////////////////////////////
// Class to calculate velocity and acceleration BEGIN: { //
///////////////////////////////////////////////////////////
#define _MACRO_CLASS_NAME_ calculate_acceleration
template <typename T> class _MACRO_CLASS_NAME_ {
    ////////////////////////////////////
    // Important definitions BEGIN: { //
    ////////////////////////////////////
  public:
    using TYPE_SELF       = _MACRO_CLASS_NAME_;
    using TYPE_FLOAT      = T;
    using TYPE_COORDINATE = D2GPS_Coordinates<TYPE_FLOAT, long>;
    //////////////////////////////////
    // Important definitions END. } //
    //////////////////////////////////

    //////////////////////////////////////
    // Important data elements BEGIN: { //
    //////////////////////////////////////
  public:
    static TYPE_FLOAT constexpr R = TYPE_COORDINATE::EarthRadius();

  public:
    TYPE_FLOAT const LAT, LON;
    TYPE_FLOAT const D_LAT, D_LON;
    TYPE_FLOAT const D2_LAT, D2_LON;
    TYPE_FLOAT const THETA, PHI;
    TYPE_FLOAT const D_THETA, D_PHI;
    TYPE_FLOAT const D2_THETA, D2_PHI;
    TYPE_FLOAT const SIN_THETA, SIN_PHI;
    TYPE_FLOAT const COS_THETA, COS_PHI;
    TYPE_FLOAT const D_SIN_THETA, D_SIN_PHI;
    TYPE_FLOAT const D_COS_THETA, D_COS_PHI;
    TYPE_FLOAT const D2_SIN_THETA, D2_SIN_PHI;
    TYPE_FLOAT const D2_COS_THETA, D2_COS_PHI;
    TYPE_FLOAT const TCC, TCS, TSC, TSS;
    TYPE_FLOAT const D_TCC, D_TCS, D_TSC, D_TSS;
    TYPE_FLOAT const D2_TCC, D2_TCS, D2_TSC, D2_TSS;
    TYPE_FLOAT const X, Y, Z;
    TYPE_FLOAT const D_X, D_Y, D_Z;
    TYPE_FLOAT const D2_X, D2_Y, D2_Z;
    TYPE_FLOAT const V, A;
    TYPE_FLOAT const unit_dot;
    ////////////////////////////////////
    // Important data elements END. } //
    ////////////////////////////////////

    ///////////////////////////////////////
    // Constructor & Destructor BEGIN: { //
    ///////////////////////////////////////
  public:
    _MACRO_CLASS_NAME_(TYPE_FLOAT const lat, TYPE_FLOAT const lon,
                       TYPE_FLOAT const d_lat, TYPE_FLOAT const d_lon,
                       TYPE_FLOAT const d2_lat, TYPE_FLOAT const d2_lon)
      : // Basic variables:
        LAT(lat), D_LAT(d_lat), D2_LAT(d2_lat), LON(lon), D_LON(d_lon),
        D2_LON(d2_lon),

        ///////////////////////////////////////
        // Calculating polar angles BEGIN: { //
        ///////////////////////////////////////
        THETA((M_PI * (90 - LAT)) / 180.0), D_THETA((-M_PI * D_LAT) / 180.0),
        D2_THETA((-M_PI * D2_LAT) / 180.0),

        PHI((M_PI * LON) / 180.0), D_PHI((M_PI * D_LON) / 180.0),
        D2_PHI((M_PI * D2_LON) / 180.0),
        /////////////////////////////////////
        // Calculating polar angles END. } //
        /////////////////////////////////////

        ///////////////////////////////////
        // Trignometric rations BEGIN: { //
        ///////////////////////////////////
        SIN_THETA(sin(THETA)), D_SIN_THETA(COS_THETA * D_THETA),
        D2_SIN_THETA((D_COS_THETA * D_THETA) + (COS_THETA * D2_THETA)),

        COS_THETA(cos(THETA)), D_COS_THETA(-SIN_THETA * D_THETA),
        D2_COS_THETA((-D_SIN_THETA * D_THETA) + (-SIN_THETA * D2_THETA)),

        SIN_PHI(sin(PHI)), D_SIN_PHI(COS_PHI * D_PHI),
        D2_SIN_PHI((D_COS_PHI * D_PHI) + (COS_PHI * D2_PHI)),

        COS_PHI(cos(PHI)), D_COS_PHI(-SIN_PHI * D_PHI),
        D2_COS_PHI((-D_SIN_PHI * D_PHI) + (-SIN_PHI * D2_PHI)),
        /////////////////////////////////
        // Trignometric rations END. } //
        /////////////////////////////////

        ////////////////////////////////////
        // Intermediate products BEGIN: { //
        ////////////////////////////////////
        TCC(COS_THETA * COS_PHI),
        D_TCC((D_COS_THETA * COS_PHI) + (COS_THETA * D_COS_PHI)),
        D2_TCC((D2_COS_THETA * COS_PHI) + (2 * D_COS_THETA * D_COS_PHI) +
               (COS_THETA * D2_COS_PHI)),

        TSC(SIN_THETA * COS_PHI),
        D_TSC((D_SIN_THETA * COS_PHI) + (SIN_THETA * D_COS_PHI)),
        D2_TSC((D2_SIN_THETA * COS_PHI) + (2 * D_SIN_THETA * D_COS_PHI) +
               (SIN_THETA * D2_COS_PHI)),

        TCS(COS_THETA * SIN_PHI),
        D_TCS((D_COS_THETA * SIN_PHI) + (COS_THETA * D_SIN_PHI)),
        D2_TCS((D2_COS_THETA * SIN_PHI) + (2 * D_COS_THETA * D_SIN_PHI) +
               (COS_THETA * D2_SIN_PHI)),

        TSS(SIN_THETA * SIN_PHI),
        D_TSS((D_SIN_THETA * SIN_PHI) + (SIN_THETA * D_SIN_PHI)),
        D2_TSS((D2_SIN_THETA * SIN_PHI) + (2 * D_SIN_THETA * D_SIN_PHI) +
               (SIN_THETA * D2_SIN_PHI)),
        //////////////////////////////////
        // Intermediate products END. } //
        //////////////////////////////////

        ////////////////////////////////////////////
        // Final embeded tangent vectors BEGIN: { //
        ////////////////////////////////////////////
        X(R * TSC), D_X(R * D_TSC), D2_X(R * D2_TSC),

        Y(R * TSS), D_Y(R * D_TSS), D2_Y(R * D2_TSS),

        Z(R * COS_THETA), D_Z(R * D_COS_THETA), D2_Z(R * D2_COS_THETA),
        //////////////////////////////////////////
        // Final embeded tangent vectors END. } //
        //////////////////////////////////////////

        ////////////////////////////////////////
        // Velocity and acceleration BEGIN: { //
        ////////////////////////////////////////
        V(std::sqrt(std::pow(D_X, 2) + std::pow(D_Y, 2) + std::pow(D_Z, 2))),
        A(std::sqrt(std::pow(D2_X, 2) + std::pow(D2_Y, 2) + std::pow(D2_Z, 2))),
        //////////////////////////////////////
        // Velocity and acceleration END. } //
        //////////////////////////////////////

        // Final angle calculation:
        unit_dot(((D_X * D2_X) + (D_Y * D2_Y) + (D_Z * D2_Z)) / (V * A)) {}

    /////////////////////////////////////
    // Constructor & Destructor END. } //
    /////////////////////////////////////
};
#undef _MACRO_CLASS_NAME_
/////////////////////////////////////////////////////////
// Class to calculate velocity and acceleration END. } //
/////////////////////////////////////////////////////////

#endif
