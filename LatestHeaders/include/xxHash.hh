#ifndef _HEADER_GUARD_xxHash_hh_
#define _HEADER_GUARD_xxHash_hh_

#define XXH_INLINE_ALL
#define XXH_STATIC_LINKING_ONLY
#define XXH_IMPLEMENTATION

#include "./xxHash.dir/xxhash.h"

#endif
