#ifndef _HEADER_GUARD_Z_CURVE_dir_header_hh_
#define _HEADER_GUARD_Z_CURVE_dir_header_hh_

#include "../CPPFileIO.hh"

//////////////////////////
// Definitions BEGIN: { //
//////////////////////////
using B1 = CPPFileIO::TYPE_U8;
using B2 = CPPFileIO::TYPE_U16;
using B4 = CPPFileIO::TYPE_U32;
using B8 = CPPFileIO::TYPE_U64;
////////////////////////
// Definitions END. } //
////////////////////////

#endif
