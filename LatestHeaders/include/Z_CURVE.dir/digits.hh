#ifndef _HEADER_GUARD_Z_CURVE_dir_digit_1_hh_
#define _HEADER_GUARD_Z_CURVE_dir_digit_1_hh_

#include "./header.hh"
#include "./eval_functions.hh"

////////////////////////////////////////////////
// Pre-computed data for 1 byte uint BEGIN: { //
////////////////////////////////////////////////
inline B2 get_digit_1_1(B1 const in) {
    switch (in) {
#include "./digits.dir/1/1.h"
        default: return 0;
    }
}

inline B2 get_digit_1_2(B1 const in) {
    switch (in) {
#include "./digits.dir/1/2.h"
        default: return 0;
    }
}

inline B2 get_digit_1_fast(B1 const in) { return get_digit_1_1(in); }
inline B2 get_digit_2_fast(B1 const in) { return get_digit_1_2(in); }

inline B2 get_z_curve(B1 const a1, B1 const a2) {
    return get_digit_1_fast(a1) | get_digit_2_fast(a2);
}
//////////////////////////////////////////////
// Pre-computed data for 1 byte uint END. } //
//////////////////////////////////////////////

////////////////////////////////////////////////
// Pre-computed data for 2 byte uint BEGIN: { //
////////////////////////////////////////////////
inline B4 get_digit_2_1(B2 const in) {
    switch (in) {
#include "./digits.dir/2/1.h"
        default: return 0;
    }
}

inline B4 get_digit_2_2(B2 const in) {
    switch (in) {
#include "./digits.dir/2/2.h"
        default: return 0;
    }
}

inline B4 get_digit_1_fast(B2 const in) { return get_digit_2_1(in); }
inline B4 get_digit_2_fast(B2 const in) { return get_digit_2_2(in); }

inline B4 get_z_curve(B2 const a1, B2 const a2) {
    return get_digit_1_fast(a1) | get_digit_2_fast(a2);
}
//////////////////////////////////////////////
// Pre-computed data for 2 byte uint END. } //
//////////////////////////////////////////////

//////////////////////////////////////////
// Fast evaluation for 4 bytes BEGIN: { //
//////////////////////////////////////////
inline B8 get_digit_4_1(B4 const in) {
    return (B8(get_digit_2_1(B2(in & 0xFFFF)))) |
           (B8(get_digit_2_1(B2(in >> 16))) << 32);
}

inline B8 get_digit_4_2(B4 const in) {
    return (B8(get_digit_2_2(B2(in & 0xFFFF)))) |
           (B8(get_digit_2_2(B2(in >> 16))) << 32);
}

inline B8 get_digit_1_fast(B4 const in) { return get_digit_4_1(in); }
inline B8 get_digit_2_fast(B4 const in) { return get_digit_4_2(in); }

inline B8 get_z_curve(B4 const a1, B4 const a2) {
    return get_digit_1_fast(a1) | get_digit_2_fast(a2);
}
////////////////////////////////////////
// Fast evaluation for 4 bytes END. } //
////////////////////////////////////////

#endif
