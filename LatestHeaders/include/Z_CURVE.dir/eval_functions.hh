#ifndef _HEADER_GUARD_Z_CURVE_dir_eval_functions_hh_
#define _HEADER_GUARD_Z_CURVE_dir_eval_functions_hh_

#include "./header.hh"

////////////////////////////////////////
// Slow evaluation functions BEGIN: { //
////////////////////////////////////////
#define prep_val(val, pos1, pos2) (((val >> pos1) & 1) << pos2)

#define _MACRO_DECLARE_(TYPE1, TYPE2)                                          \
                                                                               \
    static inline TYPE2 get_digit_1_slow(TYPE1 const in) {                     \
        TYPE2 ret = 0;                                                         \
        for (size_t i = 0; i < (sizeof(in) * 8); i++) {                        \
            size_t const val = (2 * i);                                        \
            ret |= prep_val(in, i, val);                                       \
        }                                                                      \
        return ret;                                                            \
    }                                                                          \
                                                                               \
    static inline TYPE2 get_digit_2_slow(TYPE1 const in) {                     \
        TYPE2 ret = 0;                                                         \
        for (size_t i = 0; i < (sizeof(in) * 8); i++) {                        \
            size_t val = (2 * i) + 1;                                          \
            ret |= prep_val(in, i, val);                                       \
        }                                                                      \
        return ret;                                                            \
    }

_MACRO_DECLARE_(B1, B2)
_MACRO_DECLARE_(B2, B4)
_MACRO_DECLARE_(B4, B8)

#undef _MACRO_DECLARE_
#undef prep_val
//////////////////////////////////////
// Slow evaluation functions END. } //
//////////////////////////////////////

//////////////////////////////////////////
// The main printing functions BEGIN: { //
//////////////////////////////////////////
template <typename TYPE_I, typename TYPE_O> static inline void show_digit_1() {
    TYPE_I a               = 0;
    size_t constexpr limit = 1 << (sizeof(a) * 8);
    TYPE_O b[limit];

    for (size_t i = 0; i < limit; i++) {
        a    = i;
        b[i] = get_digit_1_slow(a);
    }

    if (false) {
        for (size_t i = 0; i < limit; i++) {
            printf("%zu", b[i]);
            if (i < (limit - 1)) { printf(","); }
        }
    } else if (true) {
        for (size_t i = 0; i < limit; i++) {
            printf("case %zu: return %zu;\n", i, b[i]);
        }
    }
}

template <typename TYPE_I, typename TYPE_O> static inline void show_digit_2() {
    TYPE_I a               = 0;
    size_t constexpr limit = 1 << (sizeof(a) * 8);
    TYPE_O b[limit];

    for (size_t i = 0; i < limit; i++) {
        a    = i;
        b[i] = get_digit_2_slow(a);
    }

    if (false) {
        for (size_t i = 0; i < limit; i++) {
            printf("%zu", b[i]);
            if (i < (limit - 1)) { printf(","); }
        }
    } else if (true) {
        for (size_t i = 0; i < limit; i++) {
            printf("case %zu: return %zu;\n", i, b[i]);
        }
    }
}
////////////////////////////////////////
// The main printing functions END. } //
////////////////////////////////////////

#endif
