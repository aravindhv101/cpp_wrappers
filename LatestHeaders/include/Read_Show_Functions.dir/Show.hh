#ifndef _HEADER_GUARD_Read_Show_Functions_Show_
#define _HEADER_GUARD_Read_Show_Functions_Show_

////////////////////
// Headers BEGIN: //
////////////////////
#include "../StaticArray.hh"
//////////////////
// Headers END. //
//////////////////

inline void Show(char const *c, FILE *f = stdout) { fprintf(f, "%s", c); }

inline void Show(std::string c, FILE *f = stdout) { Show(c.c_str(), f); }

template <size_t n>
inline void Show(StaticArray::ND_ARRAY<n, char> const &in, FILE *f = stdout) {
    Show(in.GET_DATA(), f);
}

inline void Show(FILE *f = stdout) { fprintf(f, "\t"); }

inline void Show_Next(FILE *f = stdout) { fprintf(f, "\n"); }

inline void Show(char const in, FILE *f = stdout) { fprintf(f, "%c", in); }

inline void Show(unsigned char const in, FILE *f = stdout) {
    fprintf(f, "%d", static_cast<int>(in));
}

inline void Show(short const in, FILE *f = stdout) { fprintf(f, "%hd", in); }

inline void Show(unsigned short const in, FILE *f = stdout) {
    fprintf(f, "%hu", in);
}

inline void Show(int const in, FILE *f = stdout) { fprintf(f, "%d", in); }

inline void Show(unsigned int const in, FILE *f = stdout) {
    fprintf(f, "%u", in);
}

inline void Show(long const in, FILE *f = stdout) { fprintf(f, "%ld", in); }

inline void Show(unsigned long const in, FILE *f = stdout) {
    fprintf(f, "%lu", in);
}

inline void Show(float const in, FILE *f = stdout) { fprintf(f, "%f", in); }

inline void Show(double const in, FILE *f = stdout) { fprintf(f, "%lf", in); }

#endif
