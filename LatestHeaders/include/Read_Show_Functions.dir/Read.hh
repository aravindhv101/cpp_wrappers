#ifndef _HEADER_GUARD_Read_Show_Functions_Read
#define _HEADER_GUARD_Read_Show_Functions_Read

///////////////////
// Header BEGIN: //
///////////////////
#include "../CPPFileIO.hh"
#include "../StaticArray.hh"
/////////////////
// Header END. //
/////////////////

////////////////////
// String BEGIN:{ //
////////////////////
template <size_t n>
inline void Read(StaticArray::ND_ARRAY<n, char> &dest, std::string const &src) {
    std::memset(/* void *s = */ reinterpret_cast<void *>(dest.GET_DATA()),
                /* int c = */ 0, /* size_t n = */ dest.SIZE());

    size_t const limit = CPPFileIO::mymin(
      static_cast<size_t>(src.size()), static_cast<size_t>(dest.SIZE() - 1)); //

    memcpy(/* void *dest = */ reinterpret_cast<void *>(dest.GET_DATA()),
           /* const void *src = */ reinterpret_cast<void const *>(src.c_str()),
           /* size_t n = */ limit);
}

template <size_t n>
inline void Read(StaticArray::ND_ARRAY<n, char> &dest, char const *src) {
    std::memset(/* void *s = */ reinterpret_cast<void *>(dest.GET_DATA()),
                /* int c = */ 0, /* size_t n = */ dest.SIZE());

    size_t const limit = CPPFileIO::mymin(static_cast<size_t>(strlen(src)),
                                          static_cast<size_t>(dest.SIZE() - 1));

    memcpy(/* void *dest = */ reinterpret_cast<void *>(dest.GET_DATA()),
           /* const void *src = */ reinterpret_cast<void const *>(src),
           /* size_t n = */ limit);
}
//////////////////
// String END.} //
//////////////////

///////////////////
// Float BEGIN:{ //
///////////////////
inline void Read(float &dest, std::string const &src) {
    if (src.size() < 1) {
        dest = NAN;
    } else {
        sscanf(src.c_str(), "%f", &dest);
    }
}

inline void Read(float &dest, char const *src) {
    if (strlen(src) < 1) {
        dest = NAN;
    } else {
        sscanf(src, "%f", &dest);
    }
}
/////////////////
// Float END.} //
/////////////////

////////////////////
// double BEGIN:{ //
////////////////////
inline void Read(double &dest, std::string const &src) {
    if (src.size() < 1) {
        dest = NAN;
    } else {
        sscanf(src.c_str(), "%lf", &dest);
    }
}

inline void Read(double &dest, char const *src) {
    if (strlen(src) < 1) {
        dest = NAN;
    } else {
        sscanf(src, "%lf", &dest);
    }
}
//////////////////
// double END.} //
//////////////////

//////////////////
// char BEGIN:{ //
//////////////////
inline void Read(char &dest, std::string const &src) {
    if (src.size() < 1) {
        dest = 0;
    } else {
        dest = src[0];
    }
}

inline void Read(char &dest, char const *src) {
    if (strlen(src) < 1) {
        dest = 0;
    } else {
        dest = src[0];
    }
}
////////////////
// char END.} //
////////////////

///////////////////////////
// unsigned char BEGIN:{ //
///////////////////////////
inline void Read(unsigned char &dest, std::string const &src) {
    if (src.size() < 1) {
        dest = 0;
    } else {
        unsigned int tmp;
        sscanf(src.c_str(), "%u", &tmp);
        dest = static_cast<unsigned char>(tmp);
    }
}

inline void Read(unsigned char &dest, char const *src) {
    if (strlen(src) < 1) {
        dest = 0;
    } else {
        unsigned int tmp;
        sscanf(src, "%u", &tmp);
        dest = static_cast<unsigned char>(tmp);
    }
}
/////////////////////////
// unsigned char END.} //
/////////////////////////

////////////////////////////
// Short related BEGIN: { //
////////////////////////////
inline void Read(short &dest, std::string const &src) {
    if (src.size() < 1) {
        dest = 0;
    } else {
        sscanf(src.c_str(), "%hd", &dest);
    }
}

inline void Read(short &dest, char const *src) {
    if (strlen(src) < 1) {
        dest = 0;
    } else {
        sscanf(src, "%hd", &dest);
    }
}
//////////////////////////
// Short related END. } //
//////////////////////////

/////////////////////////////////////
// Unsigned short related BEGIN: { //
/////////////////////////////////////
inline void Read(unsigned short &dest, std::string const &src) {
    if (src.size() < 1) {
        dest = 0;
    } else {
        sscanf(src.c_str(), "%hu", &dest);
    }
}

inline void Read(unsigned short &dest, char const *src) {
    if (strlen(src) < 1) {
        dest = 0;
    } else {
        sscanf(src, "%hu", &dest);
    }
}
///////////////////////////////////
// Unsigned short related END. } //
///////////////////////////////////

//////////////////////////
// unsigned int BEGIN:{ //
//////////////////////////
inline void Read(unsigned int &dest, std::string const &src) {
    if (src.size() < 1) {
        dest = -999999;
    } else {
        sscanf(src.c_str(), "%u", &dest);
    }
}

inline void Read(unsigned int &dest, char const *src) {
    if (strlen(src) < 1) {
        dest = -999999;
    } else {
        sscanf(src, "%u", &dest);
    }
}
////////////////////////
// unsigned int END.} //
////////////////////////

/////////////////
// int BEGIN:{ //
/////////////////
inline void Read(int &dest, std::string const &src) {
    if (src.size() < 1) {
        dest = -999999;
    } else {
        sscanf(src.c_str(), "%d", &dest);
    }
}

inline void Read(int &dest, char const *src) {
    if (strlen(src) < 1) {
        dest = -999999;
    } else {
        sscanf(src, "%d", &dest);
    }
}
///////////////
// int END.} //
///////////////

//////////////////
// long BEGIN:{ //
//////////////////
inline void Read(long &dest, std::string const &src) {
    if (src.size() < 1) {
        dest = -999999;
    } else {
        sscanf(src.c_str(), "%ld", &dest);
    }
}

inline void Read(long &dest, char const *src) {
    if (strlen(src) < 1) {
        dest = -999999;
    } else {
        sscanf(src, "%ld", &dest);
    }
}
////////////////
// long END.} //
////////////////

///////////////////////////
// unsigned long BEGIN:{ //
///////////////////////////
inline void Read(unsigned long &dest, std::string const &src) {
    if (src.size() < 1) {
        dest = 0;
    } else {
        sscanf(src.c_str(), "%lu", &dest);
    }
}

inline void Read(unsigned long &dest, char const *src) {
    if (strlen(src) < 1) {
        dest = 0;
    } else {
        sscanf(src, "%lu", &dest);
    }
}
/////////////////////////
// unsigned long END.} //
/////////////////////////

#endif
