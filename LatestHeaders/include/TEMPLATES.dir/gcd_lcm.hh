#ifndef _HEADER_GUARD_TEMPLATES_dir_gcd_lcm_hh_
#define _HEADER_GUARD_TEMPLATES_dir_gcd_lcm_hh_

#include "./COMPARE.hh"
#include "./TYPE.hh"

#define _MACRO_TYPE_INT_ type_uint<SIZE_INT>

namespace INTERNAL_gcd {

#define _MACRO_CLASS_NAME_ GCD_LCM

template <uint8 SIZE_INT, _MACRO_TYPE_INT_ a, _MACRO_TYPE_INT_ b>
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_INT                    = _MACRO_TYPE_INT_;
    using TYPE_SELF                   = _MACRO_CLASS_NAME_<SIZE_INT, a, b>;
    static TYPE_INT constexpr MIN_V   = COMPARE::min_value<TYPE_INT, a, b>;
    static TYPE_INT constexpr MAX_V   = COMPARE::max_value<TYPE_INT, a, b>;
    static TYPE_INT constexpr NEW_MIN = MAX_V % MIN_V;
    static TYPE_INT constexpr NEW_MAX = MIN_V;
    static TYPE_INT constexpr VALUE_GCD =
      _MACRO_CLASS_NAME_<SIZE_INT, NEW_MIN, NEW_MAX>::VALUE_GCD;
    static TYPE_INT constexpr VALUE_LCM = MAX_V * (MIN_V / VALUE_GCD);
};

template <uint8 SIZE_INT, _MACRO_TYPE_INT_ a>
class _MACRO_CLASS_NAME_<SIZE_INT, a, 0> {
  public:
    using TYPE_INT                      = _MACRO_TYPE_INT_;
    static TYPE_INT constexpr VALUE_GCD = a;
};

template <uint8 SIZE_INT, _MACRO_TYPE_INT_ a>
class _MACRO_CLASS_NAME_<SIZE_INT, 0, a> {
  public:
    using TYPE_INT                      = _MACRO_TYPE_INT_;
    static TYPE_INT constexpr VALUE_GCD = a;
};

#undef _MACRO_CLASS_NAME_

} // namespace INTERNAL_gcd

template <uint8 SIZE_INT, _MACRO_TYPE_INT_ a, _MACRO_TYPE_INT_ b>
_MACRO_TYPE_INT_ constexpr gcd =
  INTERNAL_gcd::GCD_LCM<SIZE_INT, a, b>::VALUE_GCD;

template <uint8 SIZE_INT, _MACRO_TYPE_INT_ a, _MACRO_TYPE_INT_ b>
_MACRO_TYPE_INT_ constexpr lcm =
  INTERNAL_gcd::GCD_LCM<SIZE_INT, a, b>::VALUE_LCM;

#undef _MACRO_TYPE_INT_

#endif
