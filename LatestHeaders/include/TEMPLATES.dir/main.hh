#ifndef _HEADER_GUARD_TEMPLATES_dir_main_hh_
#define _HEADER_GUARD_TEMPLATES_dir_main_hh_

#include "./headers.hh"
#include "./COMPARE.hh"
#include "./TYPE.hh"

#define _MACRO_PAGE_SIZE_ 1 << 20

TYPE::type_uint<TYPE::min_int_size<_MACRO_PAGE_SIZE_>> constexpr PAGE_SIZE =
  _MACRO_PAGE_SIZE_;

#undef _MACRO_PAGE_SIZE_

#endif
