#ifndef _HEADER_GUARD_TEMPLATES_dir_TYPE_dir_basic_definitions_hh_
#define _HEADER_GUARD_TEMPLATES_dir_TYPE_dir_basic_definitions_hh_

using uint1  = unsigned char;
using uint2  = unsigned short;
using uint4  = unsigned int;
using uint8  = unsigned long;
using uint16 = unsigned long long;

using int1  = char;
using int2  = short;
using int4  = int;
using int8  = long;
using int16 = long long;

using float4  = float;
using float8  = double;
using float16 = long double;

uint1 constexpr max_uint1   = 0xff;
uint2 constexpr max_uint2   = 0xffff;
uint4 constexpr max_uint4   = 0xffffffffU;
uint8 constexpr max_uint8   = 0xffffffffffffffffUL;
uint16 constexpr max_uint16 = 0xffffffffffffffffUL;

uint1 constexpr max_int1   = (max_uint1 >> 1);
uint2 constexpr max_int2   = (max_uint2 >> 1);
uint4 constexpr max_int4   = (max_uint4 >> 1);
uint8 constexpr max_int8   = (max_uint8 >> 1);
uint16 constexpr max_int16 = (max_uint16 >> 1);

using uint_smallest = uint1;
using uint_biggest  = uint16;

using int_smallest = int1;
using int_biggest  = int16;

using float_smallest = float4;
using float_biggest  = float16;

uint_smallest constexpr max_uint_smallest = max_uint1;
uint_biggest constexpr max_uint_biggest   = max_uint16;

int_smallest constexpr max_int_smallest = max_int1;
int_biggest constexpr max_int_biggest   = max_int16;

#endif
