#ifndef _HEADER_GUARD_TEMPLATES_dir_TYPE_dir_main_hh_
#define _HEADER_GUARD_TEMPLATES_dir_TYPE_dir_main_hh_

#include "./header.hh"
#include "./basic_definitions.hh"
#include "./type_template.hh"
#include "./min_int_size.hh"
#include "./basic_functions.hh"
#include "./universal_types.hh"

#endif
