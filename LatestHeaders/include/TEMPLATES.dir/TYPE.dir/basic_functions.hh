#ifndef _HEADER_GUARD_TEMPLATES_dir_TYPE_dir_basic_functions_hh_
#define _HEADER_GUARD_TEMPLATES_dir_TYPE_dir_basic_functions_hh_

#include "./basic_definitions.hh"

#define _MACRO_MYMOD_(IN_TYPE)                                                 \
    inline IN_TYPE      abs(IN_TYPE const a) { return a; }                     \
    inline int_smallest sign(IN_TYPE const a) { return 1; }

_MACRO_MYMOD_(uint1)
_MACRO_MYMOD_(uint2)
_MACRO_MYMOD_(uint4)
_MACRO_MYMOD_(uint8)
_MACRO_MYMOD_(uint16)

#undef _MACRO_MYMOD_

#define _MACRO_MYMOD_(IN_TYPE)                                                 \
    inline IN_TYPE      abs(IN_TYPE const a) { return ((a < 0) ? -a : a); }    \
    inline int_smallest sign(IN_TYPE const a) { return ((a < 0) ? -1 : 1); }

_MACRO_MYMOD_(float4)
_MACRO_MYMOD_(float8)
_MACRO_MYMOD_(float16)

#undef _MACRO_MYMOD_

#define _MACRO_MYMOD_(IN_TYPE_RET, IN_TYPE)                                    \
    inline IN_TYPE_RET  abs(IN_TYPE const a) { return ((a < 0) ? -a : a); }    \
    inline int_smallest sign(IN_TYPE const a) { return ((a < 0) ? -1 : 1); }

_MACRO_MYMOD_(uint1, int1)
_MACRO_MYMOD_(uint2, int2)
_MACRO_MYMOD_(uint4, int4)
_MACRO_MYMOD_(uint8, int8)
_MACRO_MYMOD_(uint16, int16)

#undef _MACRO_MYMOD_

#define _MACRO_DEF_(in_type)                                                   \
    inline in_type neg(in_type const a) { return ((a > 0) ? -a : a); }

_MACRO_DEF_(int1)
_MACRO_DEF_(int2)
_MACRO_DEF_(int4)
_MACRO_DEF_(int8)
_MACRO_DEF_(int16)

_MACRO_DEF_(float4)
_MACRO_DEF_(float8)
_MACRO_DEF_(float16)

#undef _MACRO_DEF_

inline int1  neg(uint1 const a) { return -int1(a); }
inline int2  neg(uint2 const a) { return -int2(a); }
inline int4  neg(uint4 const a) { return -int4(a); }
inline int8  neg(uint8 const a) { return -int8(a); }
inline int16 neg(uint16 const a) { return -int16(a); }

#endif
