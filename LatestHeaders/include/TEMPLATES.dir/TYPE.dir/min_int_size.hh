#ifndef _HEADER_GUARD_TEMPLATES_dir_TYPE_dir_min_int_size_hh_
#define _HEADER_GUARD_TEMPLATES_dir_TYPE_dir_min_int_size_hh_

#include "./basic_definitions.hh"

namespace INTERNAL_min_int_size {

#define _MACRO_CLASS_NAME_ MIN_INT_SIZE

template <uint_biggest IN_INT> class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_<IN_INT>;
    static uint_smallest constexpr VALUE =
      1 + (_MACRO_CLASS_NAME_<(IN_INT >> 8)>::VALUE);
};

template <> class _MACRO_CLASS_NAME_<0> {
  public:
    using TYPE_SELF                      = _MACRO_CLASS_NAME_<0>;
    static uint_smallest constexpr VALUE = 0;
};

#undef _MACRO_CLASS_NAME_

} // namespace INTERNAL_min_int_size

template <uint_biggest IN_INT>
uint_smallest constexpr min_int_size =
  INTERNAL_min_int_size::MIN_INT_SIZE<IN_INT>::VALUE;

#endif
