#ifndef _HEADER_GUARD_TEMPLATES_dir_TYPE_dir_universal_types_hh_
#define _HEADER_GUARD_TEMPLATES_dir_TYPE_dir_universal_types_hh_

#include "./header.hh"
#include "./basic_definitions.hh"
#include "./type_template.hh"

namespace INTERNAL_universal_types {

#define _MACRO_CLASS_NAME_ TYPE_2_ID

template <typename T> class _MACRO_CLASS_NAME_ {
  public:
    static uint_smallest constexpr TYPE_ID = 0;
};

#define _MACRO_MAP_(TYPE, ID)                                                  \
    template <> class _MACRO_CLASS_NAME_<TYPE> {                               \
      public:                                                                  \
        static uint_smallest constexpr TYPE_ID = ID;                           \
    };

_MACRO_MAP_(uint1, 1)
_MACRO_MAP_(uint2, 1)
_MACRO_MAP_(uint4, 1)
_MACRO_MAP_(uint8, 1)
_MACRO_MAP_(uint16, 1)

_MACRO_MAP_(int1, 2)
_MACRO_MAP_(int2, 2)
_MACRO_MAP_(int4, 2)
_MACRO_MAP_(int8, 2)
_MACRO_MAP_(int16, 2)

_MACRO_MAP_(float4, 3)
_MACRO_MAP_(float8, 3)
_MACRO_MAP_(float16, 3)

#undef _MACRO_MAP_

#undef _MACRO_CLASS_NAME_

} // namespace INTERNAL_universal_types

template <typename T>
uint_smallest constexpr type_2_id =
  INTERNAL_universal_types::TYPE_2_ID<T>::TYPE_ID;

namespace INTERNAL_universal_types {

#define _MACRO_CLASS_NAME_ ID_2_TYPE

template <uint_smallest ID, uint_smallest SIZE> class _MACRO_CLASS_NAME_ {
  public:
    using TYPE = void;
};

template <uint_smallest SIZE> class _MACRO_CLASS_NAME_<1, SIZE> {
  public:
    using TYPE = type_uint<SIZE>;
};

template <uint_smallest SIZE> class _MACRO_CLASS_NAME_<2, SIZE> {
  public:
    using TYPE = type_int<SIZE>;
};

template <uint_smallest SIZE> class _MACRO_CLASS_NAME_<3, SIZE> {
  public:
    using TYPE = type_float<SIZE>;
};

#undef _MACRO_CLASS_NAME_

} // namespace INTERNAL_universal_types

template <uint_smallest ID, uint_smallest SIZE>
using id_2_type = typename INTERNAL_universal_types::ID_2_TYPE<ID, SIZE>::TYPE;

template <typename T1, typename T2>
using type_superset =
  id_2_type<COMPARE::max_value<uint_smallest, type_2_id<T1>, type_2_id<T2>>,
            COMPARE::max_value<uint_biggest, sizeof(T1), sizeof(T2)>>;

template <typename T1, typename T2>
inline type_superset<T1, T2> min(T1 const a, T2 const b) {
    return ((a < b) ? a : b);
}

template <typename T1, typename T2>
inline type_superset<T1, T2> max(T1 const a, T2 const b) {
    return ((a > b) ? a : b);
}

#endif
