#ifndef _HEADER_GUARD_TEMPLATES_dir_TYPE_dir_type_template_hh_
#define _HEADER_GUARD_TEMPLATES_dir_TYPE_dir_type_template_hh_

#include "./header.hh"
#include "./basic_definitions.hh"

namespace INTERNAL_type_template {

#define _MACRO_CLASS_NAME_ TYPE_TEMPLATE

template <uint_smallest SZ> class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_<SZ>;

    using TYPE_INT = COMPARE::compare_type<
      /*bool OC =*/(SZ > sizeof(int_biggest)), /*class TT =*/int_biggest,
      /*class FT =*/typename _MACRO_CLASS_NAME_<SZ + 1>::TYPE_INT>;

    using TYPE_UINT = COMPARE::compare_type<
      /*bool OC =*/(SZ > sizeof(uint_biggest)), /*class TT =*/uint_biggest,
      /*class FT =*/typename _MACRO_CLASS_NAME_<SZ + 1>::TYPE_UINT>;

    using TYPE_FLOAT = COMPARE::compare_type<
      /*bool OC =*/(SZ > sizeof(float_biggest)),
      /*class TT =*/float_biggest,
      /*class FT =*/typename _MACRO_CLASS_NAME_<SZ + 1>::TYPE_FLOAT>;

    static TYPE_UINT constexpr MAX_UINT =
      (SZ > sizeof(uint_biggest)) ? max_uint_biggest
                                  : _MACRO_CLASS_NAME_<SZ + 1>::MAX_UINT;

    static TYPE_UINT constexpr MAX_INT =
      (SZ > sizeof(int_biggest)) ? max_int_biggest
                                 : _MACRO_CLASS_NAME_<SZ + 1>::MAX_INT;
};

template <> class _MACRO_CLASS_NAME_<1> {
  public:
    using TYPE_SELF                     = _MACRO_CLASS_NAME_<1>;
    using TYPE_INT                      = int1;
    using TYPE_UINT                     = uint1;
    using TYPE_FLOAT                    = float4;
    static TYPE_UINT constexpr MAX_UINT = max_uint1;
    static TYPE_INT constexpr MAX_INT   = max_int1;
};

template <> class _MACRO_CLASS_NAME_<2> {
  public:
    using TYPE_SELF                     = _MACRO_CLASS_NAME_<2>;
    using TYPE_INT                      = int2;
    using TYPE_UINT                     = uint2;
    using TYPE_FLOAT                    = float4;
    static TYPE_UINT constexpr MAX_UINT = max_uint2;
    static TYPE_INT constexpr MAX_INT   = max_int2;
};

template <> class _MACRO_CLASS_NAME_<4> {
  public:
    using TYPE_SELF                     = _MACRO_CLASS_NAME_<4>;
    using TYPE_INT                      = int4;
    using TYPE_UINT                     = uint4;
    using TYPE_FLOAT                    = float4;
    static TYPE_UINT constexpr MAX_UINT = max_uint4;
    static TYPE_INT constexpr MAX_INT   = max_int4;
};

template <> class _MACRO_CLASS_NAME_<8> {
  public:
    using TYPE_SELF                     = _MACRO_CLASS_NAME_<8>;
    using TYPE_INT                      = int8;
    using TYPE_UINT                     = uint8;
    using TYPE_FLOAT                    = float8;
    static TYPE_UINT constexpr MAX_UINT = max_uint8;
    static TYPE_INT constexpr MAX_INT   = max_int8;
};

template <> class _MACRO_CLASS_NAME_<16> {
  public:
    using TYPE_SELF                     = _MACRO_CLASS_NAME_<16>;
    using TYPE_INT                      = int16;
    using TYPE_UINT                     = uint16;
    using TYPE_FLOAT                    = float16;
    static TYPE_UINT constexpr MAX_UINT = max_uint16;
    static TYPE_INT constexpr MAX_INT   = max_int16;
};

#undef _MACRO_CLASS_NAME_

} // namespace INTERNAL_type_template

template <uint_smallest I>
using type_int = typename INTERNAL_type_template::TYPE_TEMPLATE<I>::TYPE_INT;

template <uint_smallest I>
using type_uint = typename INTERNAL_type_template::TYPE_TEMPLATE<I>::TYPE_UINT;

template <uint_smallest I>
using type_float =
  typename INTERNAL_type_template::TYPE_TEMPLATE<I>::TYPE_FLOAT;

template <uint_smallest I>
type_uint<I> constexpr max_uint =
  INTERNAL_type_template::TYPE_TEMPLATE<I>::MAX_UINT;

template <uint_smallest I>
type_uint<I> constexpr max_int =
  INTERNAL_type_template::TYPE_TEMPLATE<I>::MAX_INT;

#endif
