#ifndef _HEADER_GUARD_TEMPLATES_dir_COMPARE_dir_compare_type_hh_
#define _HEADER_GUARD_TEMPLATES_dir_COMPARE_dir_compare_type_hh_

namespace INTERNAL_compare_type {

#define _MACRO_CLASS_NAME_ COMPARE_TYPE

template <bool OC, class TT, class FT> class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    using TYPE      = void;
};

template <class TT, class FT> class _MACRO_CLASS_NAME_<true, TT, FT> {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    using TYPE      = TT;
};

template <class TT, class FT> class _MACRO_CLASS_NAME_<false, TT, FT> {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_;
    using TYPE      = FT;
};

#undef _MACRO_CLASS_NAME_

} // namespace INTERNAL_compare_type

template <bool OC, class TT, class FT>
using compare_type =
  typename INTERNAL_compare_type::COMPARE_TYPE<OC, TT, FT>::TYPE;

#endif
