#ifndef _HEADER_GUARD_TEMPLATES_dir_COMPARE_dir_compare_values_hh_
#define _HEADER_GUARD_TEMPLATES_dir_COMPARE_dir_compare_values_hh_

namespace INTERNAL_compare_values {

#define _MACRO_CLASS_NAME_ COMPARE_VALUES

template <typename TYPE_VALUE, bool CONDITION, TYPE_VALUE VALUE_TRUE,
          TYPE_VALUE VALUE_FALSE>
class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF =
      _MACRO_CLASS_NAME_<TYPE_VALUE, CONDITION, VALUE_TRUE, VALUE_FALSE>;
};

template <typename TYPE_VALUE, TYPE_VALUE VALUE_TRUE, TYPE_VALUE VALUE_FALSE>
class _MACRO_CLASS_NAME_</*typename TYPE_VALUE =*/TYPE_VALUE,
                         /*bool CONDITION =*/true,
                         /*TYPE_VALUE VALUE_TRUE =*/VALUE_TRUE,
                         /*TYPE_VALUE VALUE_FALSE =*/VALUE_FALSE> {
  public:
    static TYPE_VALUE constexpr VALUE = VALUE_TRUE;
};

template <typename TYPE_VALUE, TYPE_VALUE VALUE_TRUE, TYPE_VALUE VALUE_FALSE>
class _MACRO_CLASS_NAME_</*typename TYPE_VALUE =*/TYPE_VALUE,
                         /*bool CONDITION =*/false,
                         /*TYPE_VALUE VALUE_TRUE =*/VALUE_TRUE,
                         /*TYPE_VALUE VALUE_FALSE =*/VALUE_FALSE> {
  public:
    static TYPE_VALUE constexpr VALUE = VALUE_FALSE;
};

#undef _MACRO_CLASS_NAME_

} // namespace INTERNAL_compare_values

template <typename TYPE_VALUE, bool CONDITION, TYPE_VALUE VALUE_TRUE,
          TYPE_VALUE VALUE_FALSE>
TYPE_VALUE constexpr compare_value =
  INTERNAL_compare_values::COMPARE_VALUES<TYPE_VALUE, CONDITION, VALUE_TRUE,
                                          VALUE_FALSE>::VALUE;

template <typename TYPE_VALUE, TYPE_VALUE VALUE_TRUE, TYPE_VALUE VALUE_FALSE>
TYPE_VALUE constexpr min_value =
  compare_value<TYPE_VALUE, (VALUE_TRUE < VALUE_FALSE), VALUE_TRUE,
                VALUE_FALSE>;

template <typename TYPE_VALUE, TYPE_VALUE VALUE_TRUE, TYPE_VALUE VALUE_FALSE>
TYPE_VALUE constexpr max_value =
  compare_value<TYPE_VALUE, (VALUE_TRUE > VALUE_FALSE), VALUE_TRUE,
                VALUE_FALSE>;

#endif
