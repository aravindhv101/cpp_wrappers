#ifndef _HEADER_GUARD_TEMPLATES_dir_types_hh_
#define _HEADER_GUARD_TEMPLATES_dir_types_hh_

#include "./COMPARE.hh"

using int1 = char;
using int2 = short;
using int4 = int;
using int8 = long;

using float4 = float;
using float8 = double;

using uint1 = unsigned char;
using uint2 = unsigned short;
using uint4 = unsigned int;
using uint8 = unsigned long;

uint1 constexpr max_uint1 = 0xff;
uint2 constexpr max_uint2 = 0xffff;
uint4 constexpr max_uint4 = 0xffffffffU;
uint8 constexpr max_uint8 = 0xffffffffffffffffUL;

uint1 constexpr max_int1 = (max_uint1 >> 1);
uint2 constexpr max_int2 = (max_uint2 >> 1);
uint4 constexpr max_int4 = (max_uint4 >> 1);
uint8 constexpr max_int8 = (max_uint8 >> 1);

#define _MACRO_MYMOD_(IN_TYPE)                                                 \
    inline IN_TYPE abs(IN_TYPE const a) { return a; }                          \
    inline int1    sign(IN_TYPE const a) { return 1; }

_MACRO_MYMOD_(uint1)
_MACRO_MYMOD_(uint2)
_MACRO_MYMOD_(uint4)
_MACRO_MYMOD_(uint8)

#undef _MACRO_MYMOD_

#define _MACRO_MYMOD_(IN_TYPE)                                                 \
    inline IN_TYPE abs(IN_TYPE const a) { return ((a < 0) ? -a : a); }         \
    inline int1    sign(IN_TYPE const a) { return ((a < 0) ? -1 : 1); }

_MACRO_MYMOD_(float4)
_MACRO_MYMOD_(float8)

#undef _MACRO_MYMOD_

#define _MACRO_MYMOD_(IN_TYPE_RET, IN_TYPE)                                    \
    inline IN_TYPE_RET abs(IN_TYPE const a) { return ((a < 0) ? -a : a); }     \
    inline int1        sign(IN_TYPE const a) { return ((a < 0) ? -1 : 1); }

_MACRO_MYMOD_(uint1, int1)
_MACRO_MYMOD_(uint2, int2)
_MACRO_MYMOD_(uint4, int4)
_MACRO_MYMOD_(uint8, int8)

#undef _MACRO_MYMOD_

namespace INTERNAL_types {

#define _MACRO_CLASS_NAME_ MIN_INT_SIZE

template <uint8 IN_INT> class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_<IN_INT>;
    static uint1 constexpr VALUE =
      1 + (_MACRO_CLASS_NAME_<(IN_INT >> 8)>::VALUE);
};

template <> class _MACRO_CLASS_NAME_<0> {
  public:
    using TYPE_SELF              = _MACRO_CLASS_NAME_<0>;
    static uint1 constexpr VALUE = 0;
};

#undef _MACRO_CLASS_NAME_

} // namespace INTERNAL_types

template <uint8 IN_INT>
uint1 constexpr min_int_size = INTERNAL_types::MIN_INT_SIZE<IN_INT>::VALUE;

namespace INTERNAL_types {

#define _MACRO_CLASS_NAME_ TYPE_2_ID

template <typename T> class _MACRO_CLASS_NAME_ {
  public:
    static uint1 constexpr TYPE_ID = 0;
};

#define _MACRO_MAP_(TYPE, ID)                                                  \
    template <> class _MACRO_CLASS_NAME_<TYPE> {                               \
      public:                                                                  \
        static uint1 constexpr TYPE_ID = ID;                                   \
    };

_MACRO_MAP_(uint1, 1)
_MACRO_MAP_(uint2, 1)
_MACRO_MAP_(uint4, 1)
_MACRO_MAP_(uint8, 1)

_MACRO_MAP_(int1, 2)
_MACRO_MAP_(int2, 2)
_MACRO_MAP_(int4, 2)
_MACRO_MAP_(int8, 2)

_MACRO_MAP_(float4, 3)
_MACRO_MAP_(float8, 3)

#undef _MACRO_MAP_

#undef _MACRO_CLASS_NAME_

} // namespace INTERNAL_types

template <typename T>
uint1 constexpr type_2_id = INTERNAL_types::TYPE_2_ID<T>::TYPE_ID;

namespace INTERNAL_types {

#define _MACRO_CLASS_NAME_ TYPE_TEMPLATE

template <uint1 SZ> class _MACRO_CLASS_NAME_ {
  public:
    using TYPE_SELF = _MACRO_CLASS_NAME_<SZ>;

    using TYPE_INT = COMPARE::compare_type<
      /*bool OC =*/(SZ > 8), /*class TT =*/int8,
      /*class FT =*/typename _MACRO_CLASS_NAME_<SZ + 1>::TYPE_INT>;

    using TYPE_UINT = COMPARE::compare_type<
      /*bool OC =*/(SZ > 8), /*class TT =*/uint8,
      /*class FT =*/typename _MACRO_CLASS_NAME_<SZ + 1>::TYPE_UINT>;

    using TYPE_FLOAT = COMPARE::compare_type<
      /*bool OC =*/(SZ > 8), /*class TT =*/float8,
      /*class FT =*/typename _MACRO_CLASS_NAME_<SZ + 1>::TYPE_FLOAT>;

    static TYPE_UINT constexpr MAX_UINT =
      (SZ > 8) ? max_uint8 : _MACRO_CLASS_NAME_<SZ + 1>::MAX_UINT;

    static TYPE_UINT constexpr MAX_INT =
      (SZ > 8) ? max_int8 : _MACRO_CLASS_NAME_<SZ + 1>::MAX_INT;
};

template <> class _MACRO_CLASS_NAME_<1> {
  public:
    using TYPE_SELF                     = _MACRO_CLASS_NAME_<1>;
    using TYPE_INT                      = int1;
    using TYPE_UINT                     = uint1;
    using TYPE_FLOAT                    = float4;
    static TYPE_UINT constexpr MAX_UINT = max_uint1;
    static TYPE_INT constexpr MAX_INT   = max_int1;
};

template <> class _MACRO_CLASS_NAME_<2> {
  public:
    using TYPE_SELF                     = _MACRO_CLASS_NAME_<2>;
    using TYPE_INT                      = int2;
    using TYPE_UINT                     = uint2;
    using TYPE_FLOAT                    = float4;
    static TYPE_UINT constexpr MAX_UINT = max_uint2;
    static TYPE_INT constexpr MAX_INT   = max_int2;
};

template <> class _MACRO_CLASS_NAME_<4> {
  public:
    using TYPE_SELF                     = _MACRO_CLASS_NAME_<4>;
    using TYPE_INT                      = int4;
    using TYPE_UINT                     = uint4;
    using TYPE_FLOAT                    = float4;
    static TYPE_UINT constexpr MAX_UINT = max_uint4;
    static TYPE_INT constexpr MAX_INT   = max_int4;
};

template <> class _MACRO_CLASS_NAME_<8> {
  public:
    using TYPE_SELF                     = _MACRO_CLASS_NAME_<8>;
    using TYPE_INT                      = int8;
    using TYPE_UINT                     = uint8;
    using TYPE_FLOAT                    = float8;
    static TYPE_UINT constexpr MAX_UINT = max_uint8;
    static TYPE_INT constexpr MAX_INT   = max_int8;
};

#undef _MACRO_CLASS_NAME_

} // namespace INTERNAL_types

template <uint1 I>
using type_int = typename INTERNAL_types::TYPE_TEMPLATE<I>::TYPE_INT;

template <uint1 I>
using type_uint = typename INTERNAL_types::TYPE_TEMPLATE<I>::TYPE_UINT;

template <uint1 I>
using type_float = typename INTERNAL_types::TYPE_TEMPLATE<I>::TYPE_FLOAT;

template <uint1 I>
type_uint<I> constexpr max_uint = INTERNAL_types::TYPE_TEMPLATE<I>::MAX_UINT;

template <uint1 I>
type_uint<I> constexpr max_int = INTERNAL_types::TYPE_TEMPLATE<I>::MAX_INT;

namespace INTERNAL_types {

#define _MACRO_CLASS_NAME_ ID_2_TYPE

template <uint1 ID, uint1 SIZE> class _MACRO_CLASS_NAME_ {
  public:
    using TYPE = void;
};

template <uint1 SIZE> class _MACRO_CLASS_NAME_<1, SIZE> {
  public:
    using TYPE = type_uint<SIZE>;
};

template <uint1 SIZE> class _MACRO_CLASS_NAME_<2, SIZE> {
  public:
    using TYPE = type_int<SIZE>;
};

template <uint1 SIZE> class _MACRO_CLASS_NAME_<3, SIZE> {
  public:
    using TYPE = type_float<SIZE>;
};

#undef _MACRO_CLASS_NAME_

} // namespace INTERNAL_types

template <uint1 ID, uint1 SIZE>
using id_2_type = typename INTERNAL_types::ID_2_TYPE<ID, SIZE>::TYPE;

template <typename T1, typename T2>
using type_superset =
  id_2_type<COMPARE::max_value<uint1, type_2_id<T1>, type_2_id<T2>>,
            COMPARE::max_value<uint8, sizeof(T1), sizeof(T2)>>;

template <typename T1, typename T2>
inline type_superset<T1, T2> min(T1 const a, T2 const b) {
    return ((a < b) ? a : b);
}

template <typename T1, typename T2>
inline type_superset<T1, T2> max(T1 const a, T2 const b) {
    return ((a > b) ? a : b);
}

#endif
