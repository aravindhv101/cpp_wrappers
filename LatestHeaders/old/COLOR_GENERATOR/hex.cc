#include "stdio.h"

class hexret {

private:

	char f[2] ;

	static inline char
	Convert4_i2h(
		unsigned char const
			in
	){
		unsigned char const f1 = 15     ;
		char constexpr vars[17] = "0123456789abcdef" ;
		return vars[in&f1] ;
	}

	template <typename T>
	static inline T
	Shifter (
		T const
			in
	) {
		return in >> 4 ;
	}

	inline char *
	Convert8_i2h(
		unsigned char const
			in
	) {
		unsigned char const f1 = 15	;
		f[1]=Convert4_i2h((in));
		f[0]=Convert4_i2h((Shifter(in)));
		return f ;
	}

public:

	hexret(
		unsigned char const
			in
	) {
		Convert8_i2h(in);
	}

	inline char *
	operator () (
		unsigned char const
			in
	) {
		return
			Convert8_i2h(in)
		; //
	}

	inline char *
	operator () () {
		return f ;
	}

	inline char
	operator [] (
		size_t const
			i
	)  const {
		return f[i] ;
	}

	hexret(){}
	~hexret(){}
} ;

class Colors {

private:

	double			f[3]	;
	unsigned char	c[3]	;
	char			name[8]	;

	inline void
	D2C () {
		c[0] = static_cast<unsigned char>(f[0]*255) ;
		c[1] = static_cast<unsigned char>(f[1]*255) ;
		c[2] = static_cast<unsigned char>(f[2]*255) ;
	}

	inline char *
	GetName () {
		name[0] = '#' ;
		name[7] = 0   ;
		hexret tmp0(c[0]) ;
		hexret tmp1(c[1]) ;
		hexret tmp2(c[2]) ;
		name[1] = tmp0[0] ;
		name[2] = tmp0[1] ;
		name[3] = tmp1[0] ;
		name[4] = tmp1[1] ;
		name[5] = tmp2[0] ;
		name[6] = tmp2[1] ;
		return name ;
	}

public:

	inline char const *
	operator () (
		double const a0 ,
        double const a1 ,
        double const a2
	) {
		f[0] = a0 ;
		f[1] = a1 ;
		f[2] = a2 ;
		D2C();
		GetName();
		return name ;
	}

	inline char const *
	operator () () {
		return name ;
	}

	Colors(
		double const a0 ,
		double const a1 ,
		double const a2
	) {
		f[0] = a0 ;
		f[1] = a1 ;
		f[2] = a2 ;
		D2C();
		GetName();
	}

	Colors(){}

	~Colors(){}

} ;

inline void
Printer_D (
	double const a0 ,
	double const a1 ,
	double const a2
) {
	Colors tmp;
	printf("#define \"%s\"\n",tmp((1.0*a0),(1.0*a1),(1.0*a2)));
}

inline void
Printer_B (
	double const a0 ,
	double const a1 ,
	double const a2
) {
	Colors tmp;
	printf("#define \"%s\"\n",tmp(1.0-(1.0*a0),1.0-(1.0*a1),1.0-(1.0*a2)));
}

int main () {

	Printer_D(1.0,0.0,0.0);
	Printer_D(0.8,0.6,0.0);
	Printer_D(0.6,0.8,0.0);

	Printer_D(0.0,1.0,0.0);
	Printer_D(0.0,0.8,0.6);
	Printer_D(0.6,0.6,0.8);

	Printer_D(0.0,0.0,1.0);
	Printer_D(0.6,0.0,0.8);
	Printer_D(0.8,0.0,0.6);

	Printer_B(1.0,0.0,0.0);
	Printer_B(0.8,0.6,0.0);
	Printer_B(0.6,0.8,0.0);

	Printer_B(0.0,1.0,0.0);
	Printer_B(0.0,0.8,0.6);
	Printer_B(0.6,0.6,0.8);

	Printer_B(0.0,0.0,1.0);
	Printer_B(0.6,0.0,0.8);
	Printer_B(0.8,0.0,0.6);

	return 0 ;
}
