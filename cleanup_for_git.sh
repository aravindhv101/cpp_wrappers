#!/bin/bash
cd "$('dirname' '--' "${0}")"
'find' './' '-type' 'd' | 'grep' '-v' '\./\.git$' | 'grep' '\.git$' | 'sed' 's@^@"rm" "-vfr" "--" "@g;s@$@"@g'
'find' './' '-type' 'f' | 'grep' '~$\|#$' | 'sed' 's@^@"rm" "-vf" "--" "@g;s@$@"@g'
exit '0'
